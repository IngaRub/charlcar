#ifndef VEHICLEMODEL_H
#define VEHICLEMODEL_H

#include <QObject>

#include "actions/action.h"
#include "concepts/acceleration.h"
#include "concepts/position.h"
#include "concepts/velocity.h"
#include "desires/desire.h"
#include "desires/goal.h"
#include "utils.h"

#include <deque>
#include <memory>
#include <map>
#include <vector>

namespace Charlcar {

class Route;

struct VehicleState {
    Position pos;
    Velocity v;
    Acceleration a;
    Direction ornt;
    VehicleState();
    VehicleState(Position vPos, Velocity vV, Acceleration vA, Direction vOrnt);
    QString str() const;
};

class VehicleModel : public QObject
{
    Q_OBJECT

public:
    using ActionPtrRef = Action::ActionPtrRef;
    using ActionPtrCRef = Action::ActionPtrCRef;
    using DesirePtr = std::unique_ptr<Desire>;
    using State = VehicleState;
    using GoalCItTy = std::deque<Goal>::const_iterator;

public:
    typedef int SizeTy;
    static constexpr unsigned ACC_MAX_DEFAULT = 5;
    static constexpr unsigned TURN_SPEED_MIN = 1;
    static constexpr unsigned TURN_SPEED_MAX = 10;

public:
    explicit VehicleModel(IDTy id, SizeTy size_l, SizeTy size_w, QObject *parent = 0);
    ~VehicleModel();

    IDTy id() const;

    State state() const;
    void setState(const State &state);

    Position pos() const;
    void setPos(Position pos);

    Velocity vel() const;
    void setVel(Velocity velocity);

    Acceleration acc() const;
    void setAcc(Acceleration acc);

    SizeTy sizeL() const;
    SizeTy sizeW() const;

    VectorValue::Value desiredSpeed() const;
    void setDesiredSpeed(VectorValue::Value val);

    Direction ornt() const;
    void setOrnt(Direction ornt);

    void assignRoute(const Route &route);

    void accelerateSimulate(State &vs, Acceleration acc) const;
    void moveForwardSimulate(State &vs, TimeTy interval) const;
    void turnSimulate(State &vs, Direction direction) const;
    void waitSimulate(State &vs) const;

    void accelerateExecute(Acceleration acc);
    void moveForwardExecute(TimeTy interval);
    void turnExecute(Direction direction);
    void waitExecute();

    std::vector<Action *> plan(VehicleState &vs, GoalCItTy &gIt,
                               TimeTy interval) const;
    void makeStep();

public:
    unsigned accMax() const;
    void setAccMax(const unsigned &acc_max);

    const std::deque<Goal>& goals() const;
    void setGoals(const std::deque<Goal> &goals);

    const Action *getPlannedAction() const;
    FuzzyPosition getNeighbourArea(
            const VehicleState &vs, TimeTy interval) const;

    Maybe<Goal> getGoal() const;
    GoalCItTy getGoalBegin() const;
    GoalCItTy getGoalEnd() const;
    void completeGoal();

    bool checkIfGoalCompleted();
    bool checkIfGoalCompleted(const VehicleState &vs, GoalCItTy gIt) const;

    void printDebug();

signals:

public slots:

private:
    IDTy m_id;
    State m_state;
    SizeTy m_size_l;
    SizeTy m_size_w;
    unsigned m_acc_max = ACC_MAX_DEFAULT;
    VectorValue::Value m_desired_speed;
    std::vector<DesirePtr> m_desires;
    std::deque<Goal> m_goals;
    Action *m_planned_action = nullptr;
};

} // namespace Charlcar

#endif // VEHICLEMODEL_H
