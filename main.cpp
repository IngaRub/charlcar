#include "ui/mainwindow.h"
#include <QApplication>
#include <QDebug>

#include "constants.h"
#include "gridmodel.h"
#include "simulationexecutor.h"
#include "trafficlights.h"
#include "utils.h"

#include "ui/mainmenudialog.h"

using namespace Charlcar;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    MainWindow w;

    MainMenuDialog main_menu_dialog;
    QObject::connect(&main_menu_dialog, SIGNAL(accepted()), &w, SLOT(show()));
    main_menu_dialog.exec();

    if (!main_menu_dialog.result()) { return 0; }

    return a.exec();
}
