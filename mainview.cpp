#include "mainview.h"

#include "gridmodel.h"
#include "renderarea.h"
#include "simulationexecutor.h"

#include <QGridLayout>

namespace Charlcar {

MainView::MainView(QWidget *parent) : QWidget(parent)
{
    m_renderArea = new RenderArea(this);
    connect(&SimulationExecutor::instance(), SIGNAL(changedData()),
            m_renderArea, SLOT(setData()));
    connect(&SimulationExecutor::instance(), SIGNAL(madeStep()),
            m_renderArea, SLOT(update()));

    QGridLayout *gridLayout = new QGridLayout;
    gridLayout->addWidget(m_renderArea, 1, 1);
    setLayout(gridLayout);
}

MainView::~MainView()
{

}

} // namespace Charlcar
