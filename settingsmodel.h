#ifndef SETTINGSMODEL_H
#define SETTINGSMODEL_H

#include <QObject>

namespace Charlcar {

class SettingsModel : public QObject
{
    Q_OBJECT
public:
    explicit SettingsModel(QObject *parent = 0);
    ~SettingsModel();

signals:

public slots:
};

} // namespace Charlcar

#endif // SETTINGSMODEL_H
