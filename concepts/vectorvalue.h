#ifndef VECTORVALUE_H
#define VECTORVALUE_H

#include "utils.h"

#include <cassert>
#include <QDebug>
#include <tuple>
#include <utility>

namespace Charlcar {

class VectorValue
{

public:
    using CoordTy = int;
    using ValueTy = std::pair<int, int>;
    using Value = unsigned;

public:
    explicit VectorValue(CoordTy xV = 0, CoordTy yV = 0);
    VectorValue(Value value, Direction dir);
    ~VectorValue();

    operator bool() const;
    ValueTy xy() const;
    CoordTy x() const;
    CoordTy y() const;

    Value value() const;
    Direction direction() const;

    bool isPerpendicularTo(const VectorValue &other) const;

    static VectorValue add(
        const VectorValue &vec1, const VectorValue &vec2);
    void addTo(const VectorValue &vec);

    static VectorValue sub(
        const VectorValue &vec1, const VectorValue &vec2);
    void subFrom(const VectorValue &vec);

    static VectorValue mul(int m, const VectorValue &vec);
    void mulBy(int m);

    inline friend bool operator<(const VectorValue &vec1, const VectorValue &vec2) {
        return vec1.value() < vec2.value();
    }
    inline friend bool operator>(const VectorValue &vec1, const VectorValue &vec2) {
        return vec2 < vec1;
    }
    inline friend bool operator>=(const VectorValue &vec1, const VectorValue &vec2) {
        return !(vec1 < vec2);
    }
    inline friend bool operator<=(const VectorValue &vec1, const VectorValue &vec2) {
        return !(vec2 < vec1);
    }
    inline friend bool operator==(const VectorValue &vec1, const VectorValue &vec2) {
        return vec1.x() == vec2.x() && vec1.y() == vec2.y();
    }
    inline friend bool operator!=(const VectorValue &vec1, const VectorValue &vec2) {
        return !(vec1 == vec2);
    }

    void setXY(const ValueTy &V);
    void setX(CoordTy xV);
    void setY(CoordTy yV);

    void setValueUp(Value diff, Direction dir);
    void setValueDown(Value diff, Direction dir);

    QString str() const;

protected:
    void assert1dim() const;
    void addValue(Value diff, Direction dir);

protected:
    CoordTy m_x = 0;
    CoordTy m_y = 0;

};

} // namespace Charlcar

#endif // VECTORVALUE_H
