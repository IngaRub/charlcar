#include "vectorvalue.h"

namespace Charlcar {

VectorValue::VectorValue(CoordTy xV, CoordTy yV) : m_x(xV), m_y(yV)
{
    assert1dim();
}

VectorValue::VectorValue(Value value, Direction dir)
{
    switch (dir) {
    case Direction::NORTH:
        m_y = -value; break;
    case Direction::SOUTH:
        m_y = value; break;
    case Direction::EAST:
        m_x = value; break;
    case Direction::WEST:
        m_x = -value; break;
    case Direction::INVALID:
    default:
        assert(!value);
    }
}

void VectorValue::assert1dim() const
{
    if (m_x) {
        assert(!m_y && "2D motion not supported");
    }
    if (m_y) {
        assert(!m_x && "2D motion not supported");
    }
}

VectorValue::~VectorValue()
{

}

VectorValue::operator bool() const
{
    return value();
}

VectorValue::CoordTy VectorValue::x() const
{
    return m_x;
}

VectorValue::CoordTy VectorValue::y() const
{
    return m_y;
}

VectorValue::ValueTy VectorValue::xy() const
{
    return std::make_pair(m_x, m_y);
}

void VectorValue::setXY(const ValueTy &V)
{
    std::tie(m_x, m_y) = V;
    assert1dim();
}

void VectorValue::setX(CoordTy xV)
{
    m_x = xV;
    assert1dim();
}

void VectorValue::setY(CoordTy yV)
{
    m_y = yV;
    assert1dim();
}

void VectorValue::setValueUp(Value diff, Direction dir)
{
    addValue(diff, dir);
}

void VectorValue::setValueDown(Value diff, Direction dir)
{
    addValue(-diff, dir);
}

VectorValue::Value VectorValue::value() const
{
    switch (direction()) {
    case Direction::NORTH:
        return -m_y;
    case Direction::SOUTH:
        return m_y;
    case Direction::EAST:
        return m_x;
    case Direction::WEST:
        return -m_x;
    case Direction::INVALID:
    default:
        return 0;
    }
}

Direction VectorValue::direction() const
{
    if (m_x > 0) return Direction::EAST;
    if (m_x < 0) return Direction::WEST;
    if (m_y > 0) return Direction::SOUTH;
    if (m_y < 0) return Direction::NORTH;
    return Direction::INVALID;
}

bool VectorValue::isPerpendicularTo(const VectorValue &other) const
{
    return (x() && other.y()) || (y() && other.x());
}

VectorValue VectorValue::add(
    const VectorValue &vec1, const VectorValue &vec2)
{
    VectorValue result(vec1.x(), vec1.y());
    result.addTo(vec2);
    return result;
}

void VectorValue::addTo(const VectorValue &vec)
{
    m_x += vec.x();
    m_y += vec.y();
}

VectorValue VectorValue::sub(
    const VectorValue &vec1, const VectorValue &vec2)
{
    VectorValue result(vec1.x(), vec1.y());
    result.subFrom(vec2);
    return result;
}

void VectorValue::subFrom(const VectorValue &vec)
{
    m_x -= vec.x();
    m_y -= vec.y();
}

VectorValue VectorValue::mul(int m, const VectorValue &vec)
{
    VectorValue result(vec.x(), vec.y());
    result.mulBy(m);
    return result;
}

void VectorValue::mulBy(int m)
{
    m_x *= m;
    m_y *= m;
}

void VectorValue::addValue(Value diff, Direction dir)
{
    switch (dir) {
    case Direction::NORTH:
        m_y -= diff; break;
    case Direction::SOUTH:
        m_y += diff; break;
    case Direction::EAST:
        m_x += diff; break;
    case Direction::WEST:
        m_x -= diff; break;
    default:
        assert("Invalid direction");
    }
    assert1dim();
}

QString VectorValue::str() const
{
    return QString("[%1, %2]").arg(m_x).arg(m_y);
}

} // namespace Charlcar
