#ifndef FUZZYVALUE_H
#define FUZZYVALUE_H

#include <QDebug>

#include <iostream>

namespace Charlcar {

template <class InnerValueTy> class FuzzyValue;

template <class InnerValueTy>
QDebug operator<< (QDebug out, const FuzzyValue<InnerValueTy> &fv);

template <class InnerValueTy>
bool operator< (const FuzzyValue<InnerValueTy> &fv, const InnerValueTy &iv);

template <class InnerValueTy>
bool operator<= (const FuzzyValue<InnerValueTy> &fv, const InnerValueTy &iv);

template <class InnerValueTy>
bool operator> (const FuzzyValue<InnerValueTy> &fv, const InnerValueTy &iv);

template <class InnerValueTy>
bool operator>= (const FuzzyValue<InnerValueTy> &fv, const InnerValueTy &iv);

template <class InnerValueTy> class FuzzyValue
{
public:
    FuzzyValue(InnerValueTy min, InnerValueTy max);
    explicit FuzzyValue(InnerValueTy value);
    ~FuzzyValue();

    bool includes(InnerValueTy value) const;
    bool intersects(const FuzzyValue &fv) const;

    InnerValueTy getAvg() const;
    InnerValueTy getMin() const;
    InnerValueTy getMax() const;

    friend QDebug operator<< <InnerValueTy>(QDebug out, const FuzzyValue &fv);
    friend bool operator< <InnerValueTy>(const FuzzyValue &fv, const InnerValueTy &iv);
    friend bool operator<= <InnerValueTy>(const FuzzyValue &fv, const InnerValueTy &iv);
    friend bool operator> <InnerValueTy>(const FuzzyValue &fv, const InnerValueTy &iv);
    friend bool operator>= <InnerValueTy>(const FuzzyValue &fv, const InnerValueTy &iv);

private:
    InnerValueTy m_min;
    InnerValueTy m_max;
};

} // namespace Charlcar

#endif // FUZZYVALUE_H
