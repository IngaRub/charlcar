#ifndef CONDITION_H
#define CONDITION_H

#include <functional>

namespace Charlcar {

class VehicleModel;

class Condition
{
public:
    using CondFuncTy = std::function<bool(const VehicleModel &)>;

    explicit Condition(CondFuncTy funcV) : m_function(funcV) {}

    bool check(const VehicleModel &vehicle) const {
        return m_function(vehicle);
    }

private:
    CondFuncTy m_function;
};

} // namespace Charlcar

#endif // CONDITION_H

