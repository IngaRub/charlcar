#include "acceleration.h"

#include "simulationexecutor.h"
#include "velocity.h"
#include "utils.h"

namespace Charlcar {

Acceleration::Acceleration() : VectorValue()
{
}

Acceleration::Acceleration(
    const Velocity &v1, const Velocity &v2, TimeTy interval)
{
    assert(interval);

    Velocity diff(VectorValue::sub(v2, v1));
    setX(diff.x() / SEC(interval));
    setY(diff.y() / SEC(interval));
}

Acceleration::Acceleration(const VectorValue::Value &v1,
                           const VectorValue::Value &v2,
                           Direction direction,
                           TimeTy interval)
    : VectorValue((v2 - v1) / SEC(interval), direction) {

}

Acceleration::~Acceleration()
{

}

}
