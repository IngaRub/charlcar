#ifndef MAYBE_H
#define MAYBE_H

#include <memory>

namespace Charlcar {

template <class InnerTy> class Maybe
{

public:
    Maybe();
    Maybe(const Maybe &m);
    Maybe& operator=(const Maybe &other);
    explicit Maybe(const InnerTy &value);
    ~Maybe();

    operator bool() const;
    void set(const InnerTy &value);
    InnerTy get() const;

private:
    std::unique_ptr<InnerTy> m_value;
};

} // namespace Charlcar

#endif // MAYBE_H
