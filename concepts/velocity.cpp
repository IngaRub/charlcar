#include "velocity.h"

#include "acceleration.h"
#include "constants.h"

namespace Charlcar {

Velocity::Velocity()
{
    m_x = 0;
    m_y = 0;
}

Velocity::Velocity(const VectorValue &vv)
{
    m_x = vv.x();
    m_y = vv.y();
}

Velocity::~Velocity()
{

}

void Velocity::applyAcceleration(const Acceleration &acc, int interval)
{
    m_x += acc.x() * SEC(interval);
    m_y += acc.y() * SEC(interval);
    assert1dim();
}

} // namespace Charlcar
