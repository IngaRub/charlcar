#include "position.h"

#include <QPointF>
#include <cmath>

namespace Charlcar {

Position::CoordTy min(Position::CoordTy a, Position::CoordTy b) {
    return a < b ? a : b;
}

Position::CoordTy max(Position::CoordTy a, Position::CoordTy b) {
    return a > b ? a : b;
}

Position::Position(CoordTy xV, CoordTy yV) : m_x(xV), m_y(yV)
{

}

Position::Position(QPoint p) : m_x(p.x()), m_y(p.y())
{

}

Position::~Position()
{

}

Position::CoordTy Position::x() const {
    return m_x;
}

void Position::setX(CoordTy xV) {
    m_x = xV;
}

Position::CoordTy Position::y() const {
    return m_y;
}

void Position::setY(CoordTy yV) {
    m_y = yV;
}

QPoint Position::point() const
{
    return QPoint(x(), y());
}

QPoint Position::scaledPoint(double factor) const
{
    return QPoint(x() * factor, y() * factor);
}

void Position::scale(double factor)
{
   m_x *= factor;
   m_y *= factor;
}

QString Position::str() const
{
    return QString("[%1, %2]").arg(m_x).arg(m_y);
}

QDebug operator<<(QDebug out, const Position &p)
{
    out << "x: " << p.m_x;
    out << "y: " << p.m_y;
    return out;
}

FuzzyPosition::FuzzyPosition(const Position &pos)
    : m_x(pos.x()), m_y(pos.y())
{

}

FuzzyPosition::FuzzyPosition(const QRect &rect)
    : m_x(rect.topLeft().x(), rect.topLeft().x() + rect.width()),
      m_y(rect.topLeft().y(), rect.topLeft().y() + rect.height())
{

}

FuzzyPosition::FuzzyPosition(
        const Position::CoordTy &x, const Position::CoordTy &y)
    : m_x(x), m_y(y)
{

}

FuzzyPosition::FuzzyPosition(const Position::CoordTy &minX, const Position::CoordTy &minY,
                             const Position::CoordTy &maxX, const Position::CoordTy &maxY)
    : m_x(minX, maxX), m_y(minY, maxY)
{

}

FuzzyPosition::FuzzyPosition(
        const FuzzyCoordTy &xV, const FuzzyCoordTy &yV)
    : m_x(xV), m_y(yV)
{

}

FuzzyPosition::~FuzzyPosition()
{

}

FuzzyPosition::FuzzyCoordTy FuzzyPosition::x() const {
    return m_x;
}

void FuzzyPosition::setX(FuzzyCoordTy xV) {
    m_x = xV;
}

FuzzyPosition::FuzzyCoordTy FuzzyPosition::y() const {
    return m_y;
}

void FuzzyPosition::setY(FuzzyCoordTy yV) {
    m_y = yV;
}

QRect FuzzyPosition::rect() const
{
    return QRect(x().getMin(), y().getMin(),
                 x().getMax() - x().getMin(),
                 y().getMax() - y().getMin());
}

Position FuzzyPosition::getTopLeft() const
{
    return Position(x().getMin(), y().getMin());
}

Position FuzzyPosition::getTopRight() const
{
    return Position(x().getMax(), y().getMin());
}

Position FuzzyPosition::getBottomLeft() const
{
    return Position(x().getMin(), y().getMax());
}

Position FuzzyPosition::getBottomRight() const
{
    return Position(x().getMax(), y().getMax());
}

Position FuzzyPosition::getCentrePos() const
{
   return Position(m_x.getAvg(), m_y.getAvg());
}

Position FuzzyPosition::getCentrePosAt(Direction dir) const
{
    Position p;
    if (isHorizontalDir(dir)) {
        if (dir == Direction::EAST) {
            p.setX(m_x.getMin());
        } else {
            p.setX(m_x.getMax());
        }
        p.setY(m_y.getAvg());
    } else {
        if (dir == Direction::NORTH) {
            p.setY(m_y.getMin());
        } else {
            p.setY(m_y.getMax());
        }
        p.setX(m_x.getAvg());
    }
    return p;
}

bool FuzzyPosition::isOnWay(const FuzzyPosition &other, Direction dir) const
{
    bool result = false;
    if (x().intersects(other.x())) {
        result |= (y().getMax() < other.y().getMin()) && dir == Direction::NORTH;
        result |= (y().getMin() > other.y().getMax()) && dir == Direction::SOUTH;
        return result;
    } else if (y().intersects(other.y())) {
        result |= (x().getMax() < other.x().getMin()) && dir == Direction::WEST;
        result |= (x().getMin() > other.x().getMax()) && dir == Direction::EAST;
    }
    return result;
}

bool FuzzyPosition::intersects(const FuzzyPosition &other) const
{
    return x().intersects(other.x()) && y().intersects(other.y());
}

FuzzyPosition FuzzyPosition::getIntersection(const FuzzyPosition &other) const
{
    if (!intersects(other)) {
        return FuzzyPosition();
    }
    return FuzzyPosition(max(x().getMin(), other.x().getMin()),
                         max(y().getMin(), other.y().getMin()),
                         min(x().getMax(), other.x().getMax()),
                         min(y().getMax(), other.y().getMax()));
}

FuzzyPosition FuzzyPosition::getUnion(const FuzzyPosition &other) const
{
    return FuzzyPosition(min(x().getMin(), other.x().getMin()),
                         min(y().getMin(), other.y().getMin()),
                         max(x().getMax(), other.x().getMax()),
                         max(y().getMax(), other.y().getMax()));
}

FuzzyPosition::DiffTy FuzzyPosition::getDiffX(const FuzzyPosition &other) const
{
    if (other.x().includes(x().getMin())) { return 0; }
    if (other.x().includes(x().getMax())) { return 0; }
    return other.x().getMin() - x().getMax();
}

FuzzyPosition::DiffTy FuzzyPosition::getDiffY(const FuzzyPosition &other) const
{
    if (other.y().includes(y().getMin())) { return 0; }
    if (other.y().includes(y().getMax())) { return 0; }
    return other.y().getMin() - y().getMax();
}

bool FuzzyPosition::includes(const FuzzyPosition &other) const
{
    return x().getMin() <= other.x().getMin()
           && y().getMin() <= other.y().getMin()
           && x().getMax() >= other.x().getMax()
           && y().getMax() >= other.y().getMax();
}

QDebug operator<<(QDebug out, const FuzzyPosition &p)
{
    out << "x: " << p.x();
    out << "y: " << p.y();
    return out;
}

Direction getAlongDirection(const Position &pos1, const Position &pos2)
{
    if (pos1.x() != pos2.x()) {
        assert(pos1.y() == pos2.y()); // LIMIT: manhattan grid
        if (pos1.x() < pos2.x()) {
            return Direction::EAST;
        }
        return Direction::WEST;
    }
    if (pos1.y() != pos2.y()) {
        assert(pos1.x() == pos2.x()); // LIMIT: manhattan grid
        if (pos1.y() < pos2.y()) {
            return Direction::SOUTH;
        }
        return Direction::NORTH;
    }
    return Direction::INVALID;
}

Maybe<Position::DistTy> getDistanceAlong(const Position &pos1, const Position &pos2, Direction dir)
{
    Maybe<Position::DistTy> result;
    int dist;
    switch (dir) {
    case (Direction::EAST):
        dist = pos2.x() - pos1.x();
        break;
    case (Direction::WEST):
        dist = pos1.x() - pos2.x();
        break;
    case (Direction::SOUTH):
        dist = pos2.y() - pos1.y();
        break;
    case (Direction::NORTH):
        dist = pos1.y() - pos2.y();
        break;
    default: break;
    }
    if (dist >= 0) {
        result.set(dist);
    }
    return result;
}

Maybe<Position::DistTy> getDistance(const Position &pos1, const Position &pos2)
{
    Direction dir = getAlongDirection(pos1, pos2);
    return getDistanceAlong(pos1, pos2, dir);
}

Direction getAlongDirection(const FuzzyPosition &pos1, const FuzzyPosition &pos2)
{
    if (!pos1.x().intersects(pos2.x())) {
        assert(pos1.y().intersects(pos2.y())); // LIMIT: manhattan grid
        if (pos1.x().getMax() < pos2.x().getMin()) {
            return Direction::EAST;
        }
        return Direction::WEST;
    }
    if (!pos1.y().intersects(pos2.y())) {
        assert(pos1.x().intersects(pos2.x())); // LIMIT: manhattan grid
        if (pos1.y().getMax() < pos2.y().getMin()) {
            return Direction::SOUTH;
        }
        return Direction::NORTH;
    }
    return Direction::INVALID;
}

Maybe<FuzzyPosition::DistTy> getDistanceAlong(
        const FuzzyPosition &pos1, const FuzzyPosition &pos2, Direction dir)
{
    Maybe<FuzzyPosition::DistTy> result;
    int dist;
    switch (dir) {
    case (Direction::EAST):
        dist = pos2.x().getMin() - pos1.x().getMax();
        break;
    case (Direction::WEST):
        dist = pos1.x().getMin() - pos2.x().getMax();
        break;
    case (Direction::SOUTH):
        dist = pos2.y().getMin() - pos1.y().getMax();
        break;
    case (Direction::NORTH):
        dist = pos1.y().getMin() - pos2.y().getMax();
        break;
    default: break;
    }
    if (dist >= 0) {
        result.set(dist);
    }
    return result;
}

Maybe<FuzzyPosition::DistTy> getDistance(const FuzzyPosition &pos1, const FuzzyPosition &pos2)
{
    Direction dir = getAlongDirection(pos1, pos2);
    return getDistanceAlong(pos1, pos2, dir);
}

} // namespace Charlcar
