#ifndef VELOCITY_H
#define VELOCITY_H

#include "utils.h"
#include "vectorvalue.h"

namespace Charlcar {

class Acceleration;

class Velocity : public VectorValue
{

public:

    using VectorValue::VectorValue;
    Velocity();
    Velocity(const VectorValue &vv);

    ~Velocity();

    void applyAcceleration(const Acceleration &acc, int interval);
};

} // namespace Charlcar

#endif // VELOCITY_H
