#ifndef ACCELERATION_H
#define ACCELERATION_H

#include "vectorvalue.h"

namespace Charlcar  {

class Velocity;

class Acceleration : public VectorValue
{
public:
    using VectorValue::VectorValue;
    Acceleration();
    Acceleration(const Velocity &v1, const Velocity &v2, TimeTy interval);
    Acceleration(const VectorValue::Value &v1,
                 const VectorValue::Value &v2,
                 Direction direction,
                 TimeTy interval);
    ~Acceleration();
};

}

#endif // ACCELERATION_H
