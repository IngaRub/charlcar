#ifndef POSITION_H
#define POSITION_H

#include "fuzzyvalue.h"
#include "utils.h"

#include <QPointF>
#include <QRect>
#include <vector>

namespace Charlcar {

class Position
{
public:
    using CoordTy = int;
    using DiffTy = int;
    using DistTy = unsigned;

public:
    explicit Position(CoordTy xV = 0, CoordTy yV = 0);
    Position(QPoint p);
    ~Position();

    CoordTy x() const;
    void setX(CoordTy xV);

    CoordTy y() const;
    void setY(CoordTy yV);

    QPoint point() const;
    QPoint scaledPoint(double factor) const;

    void scale(double factor);
    QString str() const;

    friend QDebug operator<< (QDebug out, const Position &p);

private:
    CoordTy m_x;
    CoordTy m_y;
};

class FuzzyPosition
{
public:
    using FuzzyCoordTy = FuzzyValue<Position::CoordTy>;
    using DiffTy = int;
    using DistTy = unsigned;

public:
    FuzzyPosition(const Position &pos);
    FuzzyPosition(const QRect &rect);
    FuzzyPosition(const Position::CoordTy &xV, const Position::CoordTy &yV);
    FuzzyPosition(const Position::CoordTy &minX = 0, const Position::CoordTy &minY = 0,
                  const Position::CoordTy &maxX = 0, const Position::CoordTy &maxY = 0);
    FuzzyPosition(const FuzzyCoordTy &xV, const FuzzyCoordTy &yV);
    ~FuzzyPosition();

    FuzzyCoordTy x() const;
    void setX(FuzzyCoordTy xV);

    FuzzyCoordTy y() const;
    void setY(FuzzyCoordTy yV);

    QRect rect() const;

    Position getTopLeft() const;
    Position getTopRight() const;
    Position getBottomLeft() const;
    Position getBottomRight() const;

    /**
     * Return the centre of the rectangle
     * that designates this fuzzy position
     */
    Position getCentrePos() const;

    /**
     * Return the centre of the rectangle's edge
     * at the <dir> side
     */
    Position getCentrePosAt(Direction dir) const;

    /**
     * Return <true> if:
     * an object at position <other>, heading stright towards <dir>
     * is going to collide with an object at this position
     */
    bool isOnWay(const FuzzyPosition &other, Direction dir) const;

    bool intersects(const FuzzyPosition &other) const;
    FuzzyPosition getIntersection(const FuzzyPosition &other) const;

    /**
     * Get the minimal area that includes both this and other area
     */
    FuzzyPosition getUnion(const FuzzyPosition &other) const;

    DiffTy getDiffX(const FuzzyPosition &other) const;
    DiffTy getDiffY(const FuzzyPosition &other) const;

    bool includes(const FuzzyPosition &other) const;

    friend QDebug operator<< (QDebug out, const FuzzyPosition &p);

private:
    FuzzyCoordTy m_x;
    FuzzyCoordTy m_y;
};

extern Maybe<Position::DistTy> getDistance(const Position &pos1, const Position &pos2);
extern Maybe<FuzzyPosition::DistTy> getDistance(const FuzzyPosition &pos1, const FuzzyPosition &pos2);

extern Maybe<Position::DistTy> getDistanceAlong(
        const Position &pos1, const Position &pos2, Direction dir);
extern Maybe<FuzzyPosition::DistTy> getDistanceAlong(
        const FuzzyPosition &pos1, const FuzzyPosition &pos2, Direction dir);

extern Direction getAlongDirection(const Position &pos1, const Position &pos2);
extern Direction getAlongDirection(const FuzzyPosition &pos1, const FuzzyPosition &pos2);

} // namespace Charlcar

#endif // POSITION_H
