#include "maybe.h"

#include "concepts/fuzzyvalue.h"
#include "concepts/position.h"
#include "concepts/vectorvalue.h"
#include "concepts/velocity.h"
#include "desires/goal.h"
#include "utils.h"

namespace Charlcar {

template<class InnerTy>
Maybe<InnerTy>::Maybe()
{

}

template<class InnerTy>
Maybe<InnerTy>::Maybe(const Maybe<InnerTy> &m)
{
    if (m) {
        m_value = std::unique_ptr<InnerTy>(new InnerTy(m.get()));
    } else {
        m_value = std::unique_ptr<InnerTy>(nullptr);
    }
}

template<class InnerTy>
Maybe<InnerTy>& Maybe<InnerTy>::operator=(const Maybe<InnerTy> &m)
{
    if (m) {
        m_value = std::unique_ptr<InnerTy>(new InnerTy(m.get()));
    } else {
        m_value = std::unique_ptr<InnerTy>(nullptr);
    }
    return *this;
}

template<class InnerTy>
Maybe<InnerTy>::Maybe(const InnerTy &value)
{
    m_value = std::unique_ptr<InnerTy>(new InnerTy(value));
}

template<class InnerTy>
Maybe<InnerTy>::~Maybe()
{

}

template<class InnerTy>
void Maybe<InnerTy>::set(const InnerTy &value)
{
    m_value = std::unique_ptr<InnerTy>(new InnerTy(value));
}

template<class InnerTy>
InnerTy Maybe<InnerTy>::get() const
{
    assert(m_value);
    return *m_value;
}

template<class InnerTy>
Maybe<InnerTy>::operator bool() const
{
    return m_value.get();
}

template class Maybe<Direction>;
template class Maybe<double>;
template class Maybe<FuzzyValue<Position::CoordTy>>;
template class Maybe<FuzzyValue<VectorValue::Value>>;
template class Maybe<FuzzyPosition>;
template class Maybe<Goal>;
template class Maybe<Position::DistTy>;
template class Maybe<TimeTy>;
template class Maybe<Velocity>;
template class Maybe<QRect>;

} // namespace Charlcar
