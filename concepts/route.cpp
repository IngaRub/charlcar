#include "route.h"

#include "actions/turnaction.h"
#include "constants.h"
#include "simulationexecutor.h"

namespace Charlcar {

using SE = SimulationExecutor;

Route::Route()
{

}

Route::~Route()
{

}

void Route::pushPosition(const FuzzyCoordTy &posx,
                         const FuzzyCoordTy &posy)
{
    pushPosition(FuzzyPosition(posx, posy));
}

void Route::pushPosition(const FuzzyPosition &pos)
{
    m_positions.push_back(pos);
}

void Route::pushLaneX(SimulationData &data, IDTy idx, FuzzyCoordTy posx)
{
    unsigned coord = data.grid()->getLane(idx).coord();
    FuzzyCoordTy fy(coord * LANE_SIZE, (coord + 1) * LANE_SIZE);
    m_positions.emplace_back(posx, fy);
}

void Route::pushLaneY(SimulationData &data, IDTy idy, FuzzyCoordTy posy)
{
    unsigned coord = data.grid()->getLane(idy).coord();
    FuzzyCoordTy fx(coord * LANE_SIZE, (coord + 1) * LANE_SIZE);
    m_positions.emplace_back(fx, posy);
}

void Route::pushLaneXY(SimulationData &data, IDTy idx, IDTy idy)
{
    const auto &laneX = data.grid()->getLane(idx);
    const auto &laneY = data.grid()->getLane(idy);
    m_positions.emplace_back(laneX.getIntersection(laneY));
}

FuzzyPosition Route::popPosition()
{
    FuzzyPosition pos = m_positions.front();
    m_positions.pop_front();
    return pos;
}

const FuzzyPosition &Route::getLastPosition() const
{
    return m_positions.back();
}

const FuzzyPosition &Route::getFirstPosition() const
{
    return m_positions.front();
}

const Route::RouteTy & Route::get() const
{
    return m_positions;
}

QDebug operator<<(QDebug out, const Route &route)
{
    for (const auto &pos : route.get()) {
        out << "(" << pos.x() << ", " << pos.y() << ")";
    }
    return out;
}

} // namespace Charlcar
