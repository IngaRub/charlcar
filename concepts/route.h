#ifndef ROUTE_H
#define ROUTE_H

#include "actions/action.h"
#include "concepts/position.h"
#include "desires/goal.h"
#include "gridmodel.h"
#include "simulationdata.h"

#include <deque>
#include <iostream>
#include <vector>

namespace Charlcar {

using FuzzyCoordTy = FuzzyPosition::FuzzyCoordTy;

class Route
{
public:
    using RouteTy = std::deque<FuzzyPosition>;

public:
    Route();
    ~Route();
    void pushPosition(const FuzzyCoordTy &posx,
                      const FuzzyCoordTy &posy);
    void pushPosition(const FuzzyPosition &pos);

    void pushLaneX(SimulationData &data, IDTy idx, FuzzyCoordTy posx);
    void pushLaneY(SimulationData &data, IDTy idy, FuzzyCoordTy posy);
    void pushLaneXY(SimulationData &data, IDTy idx, IDTy idy);

    FuzzyPosition popPosition();
    const FuzzyPosition &getLastPosition() const;
    const FuzzyPosition &getFirstPosition() const;
    const RouteTy &get() const;

    friend QDebug operator<< (std::ostream &out, const Route &route);

private:
    RouteTy m_positions;
};

} // namespace Charlcar

#endif // ROUTE_H
