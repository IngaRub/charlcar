#ifndef RANGE_H
#define RANGE_H

namespace Charlcar {

template <typename Iterator>
class range {
public:
    range(Iterator first, Iterator last) : m_f (first), m_l (last) {}
    Iterator begin() const { return m_f; }
    Iterator end() const { return m_l; }

private:
    Iterator m_f;
    Iterator m_l;
};

template <typename Iterator>
inline range<Iterator> make_range(Iterator f, Iterator l) {
    return range<Iterator>(f, l);
}

} // namespace Charlcar

#endif // RANGE_H

