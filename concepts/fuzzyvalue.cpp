#include "fuzzyvalue.h"

#include "concepts/position.h"
#include "concepts/vectorvalue.h"

namespace Charlcar {

template <class InnerValueTy>
FuzzyValue<InnerValueTy>::FuzzyValue(InnerValueTy min, InnerValueTy max)
    : m_min(min), m_max(max)
{
    if (min > max) {
        m_min = max;
        m_max = min;
    }
}

template <class InnerValueTy>
FuzzyValue<InnerValueTy>::FuzzyValue(InnerValueTy value)
    : m_min(value), m_max(value)
{

}

template <class InnerValueTy>
FuzzyValue<InnerValueTy>::~FuzzyValue()
{

}

template <class InnerValueTy>
bool FuzzyValue<InnerValueTy>::includes(InnerValueTy value) const
{
    return value >= m_min && value <= m_max;
}

template <class InnerValueTy>
bool FuzzyValue<InnerValueTy>::intersects(
        const FuzzyValue<InnerValueTy> &value) const
{
    return !(getMax() < value.getMin() || value.getMax() < getMin());
}

template <class InnerValueTy>
InnerValueTy FuzzyValue<InnerValueTy>::getAvg() const
{
    return (m_min + m_max) / 2;
}

template <class InnerValueTy>
InnerValueTy FuzzyValue<InnerValueTy>::getMin() const
{
    return m_min;
}

template <class InnerValueTy>
InnerValueTy FuzzyValue<InnerValueTy>::getMax() const
{
    return m_max;
}

template <class InnerValueTy>
QDebug operator<<(QDebug out, const FuzzyValue<InnerValueTy> &fv)
{
    out << "FUZZY[" << fv.getMin() << ", " << fv.getMax() << "]";
    return out;
}

template <class InnerValueTy>
bool operator<(const FuzzyValue<InnerValueTy> &fv, const InnerValueTy &iv)
{
    return fv.getMax() < iv;
}

template <class InnerValueTy>
bool operator<=(const FuzzyValue<InnerValueTy> &fv, const InnerValueTy &iv)
{
    return fv.getMax() <= iv;
}

template <class InnerValueTy>
bool operator>(const FuzzyValue<InnerValueTy> &fv, const InnerValueTy &iv)
{
    return fv.getMin() > iv;
}

template <class InnerValueTy>
bool operator>=(const FuzzyValue<InnerValueTy> &fv, const InnerValueTy &iv)
{
    return fv.getMin() >= iv;
}

using CoordTy = Position::CoordTy;
using VecValue = VectorValue::Value;

template class FuzzyValue<CoordTy>;
template class FuzzyValue<VecValue>;

using FuzzyCoordTy = FuzzyValue<CoordTy>;
using FuzzyVecValue = FuzzyValue<VecValue>;

template QDebug operator<< <CoordTy>(QDebug out, const FuzzyCoordTy &fv);
template QDebug operator<< <VecValue>(QDebug out, const FuzzyVecValue &fv);

template bool operator< <CoordTy>(const FuzzyCoordTy &fv, const CoordTy &p);
template bool operator<= <CoordTy>(const FuzzyCoordTy &fv, const CoordTy &p);
template bool operator> <CoordTy>(const FuzzyCoordTy &fv, const CoordTy &p);
template bool operator>= <CoordTy>(const FuzzyCoordTy &fv, const CoordTy &p);

template bool operator< <VecValue>(const FuzzyVecValue &fv, const VecValue &p);
template bool operator<= <VecValue>(const FuzzyVecValue &fv, const VecValue &p);
template bool operator> <VecValue>(const FuzzyVecValue &fv, const VecValue &p);
template bool operator>= <VecValue>(const FuzzyVecValue &fv, const VecValue &p);
} // namespace Charlcar
