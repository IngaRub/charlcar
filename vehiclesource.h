#ifndef VEHICLESOURCE_H
#define VEHICLESOURCE_H

#include "concepts/position.h"
#include "utils.h"

#include <QObject>
#include <QTimer>

namespace Charlcar {

class VehicleSource : public QObject
{
    Q_OBJECT
public:
    explicit VehicleSource(IDTy id, Position pos,
                           int interval, QObject *parent = 0);
    ~VehicleSource();

private:
    IDTy m_id;
    Position m_pos;
    int m_interval;
    QTimer *m_timer = nullptr;

signals:

public slots:
    void createVehicle();
};

} // namespace Charlcar

#endif // VEHICLESOURCE_H
