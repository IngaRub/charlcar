#include "kinematics.h"

#include "constants.h"
#include "utils.h"

#include <cmath>

namespace Charlcar {

Kinematics::Kinematics()
{

}

Kinematics::~Kinematics()
{

}

int Kinematics::roundUp(double val)
{
    if (val < 0) {
        val = -val;
        return -std::ceil(val);
    }
    return std::ceil(val);
}

int Kinematics::computeAccVal(int delta_z, int v0z, int vkz)
{
    if (!delta_z) {
        assert(v0z == vkz);
        return 0;
    }
    double diff_squares = (vkz * vkz) - (v0z * v0z);
    double result = diff_squares / (2 * (double) delta_z);
    return roundUp(result);
}

Acceleration Kinematics::computeAccOverDist(
        const Velocity &v0, const Velocity &vk,
        const FuzzyPosition &from, const FuzzyPosition &to)
{
    Velocity diffVel = Velocity::sub(vk, v0);
    Direction dir = diffVel.direction();

    // equal speeds
    if (dir == Direction::INVALID) { return Acceleration(); }

    int delta_x = from.getDiffX(to);
    int delta_y = from.getDiffY(to);

    return Acceleration(computeAccVal(delta_x, v0.x(), vk.x()),
                        computeAccVal(delta_y, v0.y(), vk.y()));
}

Acceleration Kinematics::computeAccOverSpeed(
        const Velocity &v0, const Velocity &vk, TimeTy interval)
{
    Velocity diffVel = Velocity::sub(vk, v0);
    Direction dir = diffVel.direction();

    // equal speeds
    if (dir == Direction::INVALID) { return Acceleration(); }

    if (interval == 0) { return Acceleration(INF, dir); }

    return Acceleration(v0, vk, interval);
}

Acceleration Kinematics::computeAccOverTime(
        const Velocity &v0, TimeTy t,
        const FuzzyPosition &from, const FuzzyPosition &to)
{
    // distDiff = vt + at^2/2
    double tSek = (double) t / SECOND;
    double delta_x = from.getDiffX(to);
    double delta_y = from.getDiffY(to);

    int acc_x = roundUp(2 * (delta_x - (v0.x() * tSek)) / (tSek * tSek));
    int acc_y = roundUp(2 * (delta_y - (v0.y() * tSek)) / (tSek * tSek));

    return Acceleration(acc_x, acc_y);
}

Maybe<TimeTy> Kinematics::computeTime(const Velocity &v,
                                      const FuzzyPosition &from, const FuzzyPosition &to) {
    double result = 0;
    if (v.x()) {
        result = (double) from.getDiffX(to) / v.x();
        result *= (int) SECOND;
    }
    if (v.y()) {
        assert(!result);
        result = (double) from.getDiffY(to) / v.y();
        result *= (int) SECOND;
    }
    return Maybe<TimeTy>(roundUp(result));
}

Maybe<TimeTy> Kinematics::computeTime(const Acceleration &a, const Velocity &v,
                                      const FuzzyPosition &from, const FuzzyPosition &to)
{
    double result = -1;
    if (a.x()) {
        assert(!v.y());
        double delta_x = -from.getDiffX(to);
        Maybe<double> t = solveQuadraticEq((double) a.x() / 2, v.x(), delta_x);
        if (t) { result = t.get(); }
    } else if (a.y()) {
        assert(!v.x());
        double delta_y = -from.getDiffY(to);
        Maybe<double> t = solveQuadraticEq((double) a.y() / 2, v.y(), delta_y);
        if (t) { result = t.get(); }
    } else {
        return computeTime(v, from, to);
    }
    if (result <= 0) { return Maybe<TimeTy>(); }
    return Maybe<TimeTy>(roundUp(result * (int) SECOND));
}

Maybe<FuzzyValue<TimeTy>> Kinematics::computeTime(const Acceleration &a, const Velocity &v,
                                      const FuzzyValue<Velocity::Value> &speed)
{
    int velValDiffMin, velValDiffMax;
    if (speed.includes(v.value())) {
        return Maybe<FuzzyValue<TimeTy>>(FuzzyValue<TimeTy>(0));
    }
    if (speed.getMin() > v.value()) {
        velValDiffMin = speed.getMin() - v.value();
        velValDiffMax = speed.getMax() - v.value();
    } else {
        velValDiffMin = v.value() - speed.getMax();
        velValDiffMax = v.value() - speed.getMin();
    }
    if (!velValDiffMin && !velValDiffMax) {
        return Maybe<FuzzyValue<TimeTy>>(FuzzyValue<TimeTy>(0));
    }
    if (!a.x() && !a.y()) {
        return Maybe<FuzzyValue<TimeTy>>();
    }
    if ((v.x() && a.y()) || (v.y() && a.x())) {
        return Maybe<FuzzyValue<TimeTy>>();
    }
    TimeTy tMax = (velValDiffMax / a.value()) * (int) SECOND;
    TimeTy tMin = (velValDiffMin / a.value()) * (int) SECOND;
    return Maybe<FuzzyValue<TimeTy>>(FuzzyValue<TimeTy>(tMin, tMax));
}

Position Kinematics::computePosition(const Position &pos, const Velocity &v,
                                     const Acceleration &a, TimeTy t)
{
    double time = (double) t / SECOND;
    double distX = 0, distY = 0;

    distX = (double) v.x() * time + ((double) a.x() * time * time / 2);
    distY = (double) v.y() * time + ((double) a.y() * time * time / 2);

    Position result = pos;
    result.setX(pos.x() + roundUp(distX));
    result.setY(pos.y() + roundUp(distY));
    return result;
}

Position Kinematics::computePosition(const Position &pos, const Velocity &v0,
                                     const Velocity &vk, const Acceleration &a)
{
    assert(!(v0.isPerpendicularTo(vk)));
    assert(!(v0.isPerpendicularTo(a)));
    Position result = pos;
    if (a.x()) {
        double dist = ((vk.x() * vk.x()) - (v0.x() * v0.x())) / (2 * a.x());
        result.setX(pos.x() + roundUp(dist));
        return result;
    }
    assert(a.y());
    double dist = ((vk.y() * vk.y()) - (v0.y() * v0.y())) / (2 * a.y());
    result.setY(pos.y() + roundUp(dist));

    return result;
}
} // namespace Charlcar
