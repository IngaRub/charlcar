#include "renderarea.h"

#include "gridmodel.h"
#include "simulationexecutor.h"

#include <QDebug>
#include <QPainter>
#include <QRect>

#include <algorithm>
#include <cassert>

#define LOG(text) _LOG(DBG_GR, text)
#define LOG_LINE _LOG_LINE(DBG_GR)
#define LOG_BEGIN _LOG_BEGIN(DBG_GR)
#define LOG_END _LOG_END(DBG_GR)

namespace Charlcar {

using SizeTy = VehicleModel::SizeTy;
using SE = SimulationExecutor;

static const int ARROW_HEAD_W = 5;
static const int ARROW_HEAD_L = 7;

RenderArea::RenderArea(QWidget *parent) : QWidget(parent)
{
    setBackgroundRole(QPalette::Base);
    setAutoFillBackground(true);
}

RenderArea::~RenderArea()
{

}

QSize RenderArea::minimumSizeHint() const
{
    return QSize(WIDTH, HEIGHT);
}

QSize RenderArea::sizeHint() const
{
    return QSize(WIDTH, HEIGHT);
}

const SimulationData *RenderArea::getData() const
{
    return m_data;
}

void RenderArea::setData() {
    setData(&SE::instance().data());
}

void RenderArea::setData(const SimulationData *data)
{
    assert(data);
    m_data = data;

    connect(data->grid(), SIGNAL(changed()), this, SLOT(update()));

    m_rows = data->grid()->getRows();
    m_columns = data->grid()->getColumns();

    unsigned tileWidth = WIDTH / m_columns;
    unsigned tileHeight = HEIGHT / m_rows;
    m_lane_size = std::min(tileWidth, tileHeight);
    update();
}

void RenderArea::paintEvent(QPaintEvent */* event */)
{
    paintGrid();
    paintTrafficLights();
    paintVehicles();
}

void RenderArea::paintGrid()
{
    assert(m_data);

    QPainter painter(this);
    painter.setPen(QPen(Qt::NoPen));

    QRect rect(0, 0, WIDTH, HEIGHT);
    painter.drawRect(rect);
    painter.fillRect(rect, Qt::darkGray);

    for (const auto &lane : m_data->grid()->lanes()) {
        paintLane(*lane);
    }
    for (const auto &lane : m_data->grid()->lanes()) {
        paintDirection(*lane);
    }
}

void RenderArea::paintLane(const LaneModel &lane)
{
    QPainter painter(this);
    painter.setPen(QPen(Qt::NoPen));

    const auto &rect = toDisplay(lane.area()).rect();
    painter.drawRect(rect);
    painter.fillRect(rect, Qt::lightGray);
}

void RenderArea::paintDirection(const LaneModel &lane)
{
    QPainter painter(this);
    painter.setPen(Qt::darkGray);

    QPainterPath path;
    path.moveTo(0, 0);
    path.lineTo(m_lane_size,
                 m_lane_size / 2);
    path.lineTo(0, m_lane_size);

    if (isHorizontalDir(lane.dirTo())) {
        auto start = lane.area().getTopLeft().point();
        painter.translate(start.x() * getScaleFactor(), start.y() * getScaleFactor());
        int offsetx = m_lane_size;
        if (lane.dirTo() == Direction::WEST) {
            painter.translate((int) m_lane_size, (int) m_lane_size);
            painter.rotate(180);
            offsetx = -m_lane_size;
        }
        for (int startx = start.x(); startx < lane.area().getBottomRight().x(); startx += LANE_SIZE) {
            painter.drawPath(path);
            painter.translate(offsetx, 0);
        }
    }

    if (isVerticalDir(lane.dirTo())) {
        auto start = lane.area().getTopRight().point();
        painter.translate(start.x() * getScaleFactor(), start.y() * getScaleFactor());
        int offsetx = m_lane_size;
        if (lane.dirTo() == Direction::NORTH) {
            painter.translate(-(int) m_lane_size, (int) m_lane_size);
            painter.rotate(270);
            offsetx = -m_lane_size;
        } else if (lane.dirTo() == Direction::SOUTH) {
            painter.rotate(90);
        }
        for (int starty = start.y(); starty < lane.area().getBottomRight().y(); starty += LANE_SIZE) {
            painter.drawPath(path);
            painter.translate(offsetx, 0);
        }
    }
}

void RenderArea::paintVehicles()
{
    QPainter painter(this);
    QPen pen = painter.pen();
    pen.setWidth(2);
    painter.setPen(pen);
    double scaleFactor = getScaleFactor();
    for (auto *V : m_data->vehicles()) {
        Position::CoordTy posX = V->pos().x() * scaleFactor;
        Position::CoordTy posY = V->pos().y() * scaleFactor;
        SizeTy sizeX = V->sizeL() * scaleFactor, sizeY = V->sizeW() * scaleFactor;
        if (V->ornt() == Direction::NORTH || V->ornt() == Direction::SOUTH) {
            sizeX = V->sizeW() * scaleFactor;
            sizeY = V->sizeL() * scaleFactor;
            posX -= sizeX / 2;
            if (V->ornt() == Direction::NORTH) {
                posY -= sizeY;
            }
        } else {
            posY -= sizeY / 2;
            if (V->ornt() == Direction::EAST)
                posX -= sizeX;
        }
        QRect rect(posX, posY, sizeX, sizeY);
        painter.drawRect(rect);
        painter.fillRect(rect, Qt::darkRed);
    }
}

void RenderArea::paintTrafficLights()
{
    for (auto TL : m_data->trafficLights()) {
        auto tlpos = TL->pos();
        QRect rect(tlpos.getTopLeft().scaledPoint(getScaleFactor()),
                   tlpos.getBottomRight().scaledPoint(getScaleFactor()));
        QPainter painter(this);
        painter.setPen(QPen(Qt::NoPen));
        painter.drawRect(rect);
        painter.fillRect(rect, QColor(40, 40, 40));
        for (auto A : TL->currentPhase().allowed()) {
            auto from = A.first;
            auto to = A.second;
            paintTrafficLightsPass(from, to, TL);
        }
    }
}

void RenderArea::paintTrafficLightsPass(IDTy from, IDTy to, const TrafficLights *TL)
{
    Position tl_centre = toDisplay(TL->pos().getCentrePos());
    QPainter painter(this);

    QPainterPath from_path;
    FuzzyPosition start = toDisplay(TL->getPosFrom(from));

    from_path.moveTo(start.getBottomRight().x(), start.getBottomRight().y());
    from_path.lineTo(start.getTopLeft().x(), start.getTopLeft().y());
    from_path.lineTo(tl_centre.x(), tl_centre.y());
    from_path.lineTo(start.getBottomRight().x(), start.getBottomRight().y());

    painter.setPen(Qt::lightGray);
    painter.drawPath(from_path);
    painter.fillPath(from_path, Qt::lightGray);

    QPainterPath to_path;
    FuzzyPosition end = toDisplay(TL->getPosTo(to));

    to_path.moveTo(end.getTopLeft().x(), end.getTopLeft().y());
    to_path.lineTo(end.getBottomRight().x(), end.getBottomRight().y());
    to_path.lineTo(tl_centre.x(), tl_centre.y());
    to_path.lineTo(end.getTopLeft().x(), end.getTopLeft().y());

    painter.setPen(Qt::lightGray);
    painter.drawPath(to_path);
    painter.fillPath(to_path, Qt::lightGray);
}

double RenderArea::getScaleFactor() const
{
    return (m_lane_size * 1.0) / LANE_SIZE;
}

unsigned RenderArea::toDisplay(unsigned dist) const
{
    return dist * getScaleFactor();
}

Position RenderArea::toDisplay(const Position &pos) const
{
    return Position(toDisplay(pos.x()), toDisplay(pos.y()));
}

FuzzyPosition RenderArea::toDisplay(const FuzzyPosition &pos) const
{
    return FuzzyPosition(toDisplay(pos.x().getMin()),
                         toDisplay(pos.y().getMin()),
                         toDisplay(pos.x().getMax()),
                         toDisplay(pos.y().getMax()));
}

unsigned RenderArea::fromDisplay(unsigned dist) const
{
    return dist / getScaleFactor();
}

Position RenderArea::fromDisplay(const Position &pos) const
{
    return Position(fromDisplay(pos.x()), fromDisplay(pos.y()));
}

FuzzyPosition RenderArea::fromDisplay(const FuzzyPosition &pos) const
{
    return FuzzyPosition(fromDisplay(pos.x().getMin()),
                         fromDisplay(pos.y().getMin()),
                         fromDisplay(pos.x().getMax()),
                         fromDisplay(pos.y().getMax()));
}

unsigned RenderArea::getDisplayLaneSize() const
{
    return m_lane_size;
}

} // namespace Charlcar
