#include "parameters.h"

namespace Charlcar {

Parameters::Parameters(QObject *parent) : QObject(parent)
{

}

Parameters::~Parameters()
{

}


TimeTy Parameters::stepInterval() const
{
    return m_step_interval;
}

void Parameters::setStepInterval(const TimeTy &step_interval)
{
    m_step_interval = step_interval;
}

VectorValue::Value Parameters::desiredSpeed() const
{
    return m_desired_speed;
}

void Parameters::setDesiredSpeed(const VectorValue::Value &desired_speed)
{
    m_desired_speed = desired_speed;
}

TimeTy Parameters::waitPhase() const
{
    return m_wait_phase;
}

void Parameters::setWaitPhase(const TimeTy &wait_phase)
{
    m_wait_phase = wait_phase;
}

} // namespace Charlcar
