#ifndef DRIVEFASTDESIRE_H
#define DRIVEFASTDESIRE_H

#include "concepts/position.h"
#include "desire.h"

namespace Charlcar {

class DriveFastDesire : public Desire
{

public:
    using Desire::Desire;
    ~DriveFastDesire();
    Action * planAction(const VehicleState &vs,
                        const GoalCItTy &, TimeTy interval,
                        Action *originalAction) override;

};

} // namespace Charlcar

#endif // DRIVEFASTDESIRE_H
