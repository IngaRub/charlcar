#ifndef GOAL_H
#define GOAL_H

#include "concepts/fuzzyvalue.h"
#include "concepts/maybe.h"
#include "concepts/position.h"
#include "concepts/vectorvalue.h"
#include "concepts/velocity.h"

namespace Charlcar {

class VehicleModel;
class VehicleState;

class Goal
{

public:
    using FuzzyCoordTy = FuzzyPosition::FuzzyCoordTy;
    using FuzzyVecValue = FuzzyValue<VectorValue::Value>;

    Goal(const VehicleModel &vm, FuzzyCoordTy x, FuzzyCoordTy y);
    ~Goal();

    const FuzzyCoordTy & x() const;
    void setX(const FuzzyCoordTy &x);

    const FuzzyCoordTy & y() const;
    void setY(const FuzzyCoordTy &y);

    FuzzyPosition pos() const;

    const Maybe<FuzzyVecValue> & speed() const;
    void setSpeed(const FuzzyVecValue &speed);

    const Maybe<Direction> & velocityDirection() const;
    void setVelocityDirection(const Direction &velocity_direction);

    friend QDebug operator<< (QDebug out, const Goal &goal);

public:
    Maybe<Position::DistTy> spaceDistance() const;
    Maybe<Position::DistTy> spaceDistance(const VehicleState &vs) const;

    Maybe<TimeTy> timeDistance() const;
    Maybe<TimeTy> timeDistance(const VehicleState &vs) const;

    bool isAchieved() const;
    bool isAchieved(const VehicleState &vs) const;

    bool approvesWay() const;
    bool approvesWay(const VehicleState &vs) const;

    bool approvesPosition() const;
    bool approvesPosition(const VehicleState &vs) const;
    bool approvesPosition(const Position &v) const;

    bool approvesVelocity() const;
    bool approvesVelocity(const VehicleState &vs) const;
    bool approvesVelocity(const Velocity &v) const;

    bool approvesX() const;
    bool approvesX(const VehicleState &vs) const;
    bool approvesX(const Position::CoordTy x) const;

    bool approvesY() const;
    bool approvesY(const VehicleState &vs) const;
    bool approvesY(const Position::CoordTy yV) const;

    bool approvesSpeed() const;
    bool approvesSpeed(const VehicleState &vs) const;
    bool approvesSpeed(const Velocity &vel) const;

    bool approvesDirection() const;
    bool approvesDirection(const VehicleState &vs) const;
    bool approvesDirection(const Velocity &vel) const;

 private:
    const VehicleModel &m_vehicle;
    FuzzyCoordTy m_x;
    FuzzyCoordTy m_y;
    Maybe<FuzzyVecValue> m_speed;
    Maybe<Direction> m_velocity_direction;
};

} // namespace Charlcar

#endif // GOAL_H
