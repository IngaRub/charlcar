#include "desire.h"

#include "actions/action.h"
#include "simulationexecutor.h"
#include "vehiclemodel.h"

namespace Charlcar {

Desire::Desire(const VehicleModel &vm) : m_vehicle(vm)
{

}

Desire::~Desire()
{

}

Action *Desire::planAction(Action *originalAction)
{
    VehicleState vs = m_vehicle.state();
    return planAction(vs, m_vehicle.getGoalBegin(),
                      simInterval(), originalAction);
}

void Desire::startPrediction()
{
    m_is_predicting = true;
}

void Desire::endPrediction()
{
    m_is_predicting = false;
}

bool Desire::isPredicting() const
{
    return m_is_predicting;
}

} // namespace Charlcar
