#ifndef DONOTCRASHDESIRE_H
#define DONOTCRASHDESIRE_H

#include "desire.h"

namespace Charlcar {

class Goal;
class TrafficLights;
class VehicleState;

class DoNotCrashDesire : public Desire
{

public:
    using Desire::Desire;
    using GoalCItTy = std::deque<Goal>::const_iterator;

    ~DoNotCrashDesire();
    Action * planAction(const VehicleState &vs, const GoalCItTy &gIt,
                        TimeTy interval, Action *originalAction) override;

private:
};

} // namespace Charlcar



#endif // DONOTCRASHDESIRE_H
