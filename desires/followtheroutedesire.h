#ifndef FOLLOWTHEROUTEDESIRE_H
#define FOLLOWTHEROUTEDESIRE_H

#include "concepts/acceleration.h"
#include "concepts/maybe.h"
#include "concepts/position.h"
#include "concepts/velocity.h"
#include "desire.h"
#include "desires/goal.h"

#include <deque>

namespace Charlcar {

class VehicleState;

class FollowTheRouteDesire : public Desire
{

public:
    using Desire::Desire;
    using GoalCItTy = std::deque<Goal>::const_iterator;

    ~FollowTheRouteDesire();
    Action * planAction(const VehicleState &vs, const GoalCItTy &gIt,
                        TimeTy interval, Action *original_action) override;

private:
    bool isActionAcceptable(const VehicleState &vs, GoalCItTy gIt, Action *action) const;

    Action *planSubAction(const VehicleState &vs, GoalCItTy &g, TimeTy time_left);

    bool shouldTurn(const VehicleState &vs, const Goal &g) const;

    /**
     * @brief shouldSlowDown
     * Check if slowing down in this step will result in obtaining the goal speed
     * at the goal position and is possible
     * @param state current state before slowing down
     * @param g current goal to achieve
     * @param time_left time left in this step for further actions
     * @return true if this is the right moment to slow down
     */
    bool shouldAccelerate(VehicleState vs, const Goal &g, TimeTy time_left) const;

private:

    static Acceleration::Value computeRequiredAcc(const Velocity::Value &v0V, const Velocity::Value &vkV, Position::DistTy distance);

    static Acceleration computeRequiredAcc(const Velocity &v0, const Velocity &vk);

    static Acceleration computeRequiredAcc(const Velocity &v0, const Velocity &vk, Position::DistTy distance);

    static Acceleration computeRequiredAcc(const VehicleState &state, const Goal &g);
};

} // namespace Charlcar

#endif // FOLLOWTHEROUTEDESIRE_H
