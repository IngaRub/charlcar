#ifndef OBEYTHERULES_H
#define OBEYTHERULES_H

#include "desire.h"
#include "concepts/maybe.h"
#include "utils.h"

#include <deque>

namespace Charlcar {

class Goal;
class TrafficLights;
class VehicleState;

class ObeyTheRulesDesire : public Desire
{

public:
    using Desire::Desire;
    using GoalCItTy = std::deque<Goal>::const_iterator;

    ~ObeyTheRulesDesire();
    Action *planAction(const VehicleState &vs, const GoalCItTy &gIt,
                       TimeTy interval, Action *originalAction) override;

private:
    Action *crossTrafficLights(TimeTy interval, const Action *originalAction,
            const TrafficLights &closest_tl1,
            const VehicleState &vs1, GoalCItTy gIt,
            TimeTy start, TimeTy duration);
    Action *approachTrafficLights(TimeTy interval,
            const TrafficLights &closest_tl1,
            const VehicleState &vs1, const VehicleState &vs2,
            GoalCItTy gIt, TimeTy start, TimeTy duration);
    IDTy getVehicleLane(const VehicleState &vs) const;
    Maybe<IDTy> getClosestTrafficLights(
            const VehicleState &vs, const Goal &g) const;
    Maybe<IDTy> getTurnLane(const TrafficLights &tl, GoalCItTy gIt) const;
    Action *stopAndWaitForTrafficLights(
            const VehicleState &vs, const TrafficLights &closest_tl, TimeTy interval) const;
    Action *slowDownToCrossTrafficLights(const VehicleState &vs, GoalCItTy gIt,
                                         const VehicleState &vs3, const TrafficLights &closest_tl1_id,
                                         TimeTy slow_down_interval, TimeTy step_interval);
};

} // namespace Charlcar

#endif // OBEYTHERULES_H
