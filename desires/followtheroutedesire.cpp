#include "followtheroutedesire.h"

#include "actions/accelerateaction.h"
#include "actions/compositeaction.h"
#include "actions/moveforwardaction.h"
#include "actions/turnaction.h"
#include "constants.h"
#include "kinematics.h"
#include "simulationexecutor.h"
#include "vehiclemodel.h"

#include <algorithm>

#define LOG(text) _LOG(DBG_FTRD, text)
#define LOG_LINE _LOG_LINE(DBG_FTRD)
#define LOG_BEGIN _LOG_BEGIN(DBG_FTRD)
#define LOG_END _LOG_END(DBG_FTRD)

#define LOG_ID(text) do { \
    LOG("[" + QString::number(m_vehicle.id()) + "]"); \
    LOG(text); \
} while(0)

namespace Charlcar {

FollowTheRouteDesire::~FollowTheRouteDesire()
{

}

Action *FollowTheRouteDesire::planAction(const VehicleState &vs, const GoalCItTy &gIt,
                                         TimeTy interval, Action *original_action)
{
    CompositeAction *current_action = new CompositeAction();

    if (isActionAcceptable(vs, gIt, original_action)) {
        LOG_ID("Action from desire drive fast - accepted");
        current_action->addAction(original_action);
    } else {
        LOG_ID("Action from desire drive fast - NOT accepted");
    }

    TimeTy time_left_for_action = interval - current_action->interval();

    Action *subaction = nullptr;

    auto goals_it = gIt;
    auto goals_end = m_vehicle.getGoalEnd();

    /* Plan subactions */
    do {
        if (goals_it == goals_end) { break; }

        VehicleState simulated_state = vs;
        current_action->simulate(m_vehicle, simulated_state);

        // try to plan another action for this step
        subaction = planSubAction(simulated_state, goals_it, time_left_for_action);

        // if there is an action that can be executed within this step - add it
        if (subaction) {
            current_action->addAction(subaction);
            time_left_for_action -= subaction->interval();
        }
    } while (subaction);

    return current_action;
}

bool FollowTheRouteDesire::isActionAcceptable(const VehicleState &vs, GoalCItTy gIt,
                                              Action *action) const
{
    if (gIt == m_vehicle.getGoalEnd()) { return true; }

    Goal g = *gIt;

    VehicleState simulated_state = vs;
    action->simulate(m_vehicle, simulated_state);
    /* Is it the right way? */
    if (!g.approvesWay(simulated_state)) { return false; }

    /* Is it the right speed? */
    // no requirements for speed or value is already ok
    if (g.approvesSpeed(simulated_state)) { return true; }

    // Is required acceleration possible?
    Acceleration required_acc = computeRequiredAcc(simulated_state, g);

    return required_acc.value() <= m_vehicle.accMax();
}

Action *FollowTheRouteDesire::planSubAction(const VehicleState &vs,
                                            GoalCItTy &goals_it, TimeTy time_left)
/* vs is a simulated state of the vehicle (current_action for this step is applied)
 * time_left is time left in this step for a potential subaction
 */
{
    const Goal g = *goals_it;
    LOG_ID("Subaction, Current Goal: " << g);
    LOG_ID("Subaction, Current State: " + vs.str());

    Action *result_action = nullptr;

    do {
    if (shouldTurn(vs, g)) {
        LOG(">> SHOULD TURN <<\n");
        result_action = new TurnAction(g.velocityDirection().get());
        ++goals_it;
        break;
    }

    // if it should not have turned then:
    assert(g.approvesWay(vs));

    // if speed is inappropriate then accelerate
    if (shouldAccelerate(vs, g, time_left)) {
      LOG_ID(">> SHOULD ACCELERATE <<\n");
      Acceleration required_acc = computeRequiredAcc(vs, g);
      if (m_vehicle.accMax() < required_acc.value()) {
        required_acc = Acceleration(m_vehicle.accMax(), required_acc.direction());
      }
      if (vs.a != required_acc) {
        result_action = new AccelerateAction(required_acc);
        break;
      }
    }

    if (time_left) {
      // just move forward
      LOG_ID(">> SHOULD MOVE FORWARD <<\n");
      result_action = new MoveForwardAction();

      Maybe<TimeTy> mtdist = g.timeDistance(vs);
      if (!mtdist) { // the goal is currently unreachable
        mtdist.set(time_left);
      }
      TimeTy tdist = mtdist.get();

      result_action->setInterval(std::min(tdist, time_left));

      VehicleState sim_vs = vs;
      result_action->simulate(m_vehicle, sim_vs, goals_it);
      break;
    }

    LOG_ID(">> SHOULD TAKE NO ACTION <<\n");
    } while (0);

    return result_action;
}

bool FollowTheRouteDesire::shouldTurn(const VehicleState &vs, const Goal &g) const
{
    bool result = true;

    // cannot turn when speed equals 0
    result &= vs.v.direction() != Direction::INVALID;

    // the goal is to turn
    result &= !g.approvesDirection(vs.v);

    // we are in expected state
    result &= g.approvesPosition(vs);
    result &= g.approvesSpeed(vs);

    return result;
}

bool FollowTheRouteDesire::shouldAccelerate(VehicleState vs, const Goal &g, TimeTy time_left) const
{
    if (!g.speed()) { return false; } // no requirements for speed
    if (!g.approvesWay(vs)) { return false; } // something else is wrong
    LOG_ID(vs.str());
    LOG_ID(g);
    Maybe<TimeTy> mtdist = g.timeDistance(vs);
    if (!mtdist) {
        mtdist.set(time_left);
    }
    TimeTy tdist = mtdist.get();

    // Pretend we move forward as long as possible in this step
    m_vehicle.moveForwardSimulate(vs, std::min(tdist, time_left));

    // the speed is right
    if (g.approvesSpeed(vs)) { return false; }

    // the position is right but the speed not
    if (g.approvesPosition(vs)) { return true; }

    // Would it be possible to apply needed acceleration?
    auto req_acc = computeRequiredAcc(vs, g);

    return req_acc.value() > m_vehicle.accMax();
}

Acceleration FollowTheRouteDesire::computeRequiredAcc(const VehicleState &state, const Goal &g)
{
    if (!g.speed()) { return Acceleration(); }
    if (g.approvesSpeed(state)) { return Acceleration(); }

    // velocity to achieve
    Velocity::Value desired_vel_val = g.speed().get().getAvg();
    Direction desired_vel_dir = g.velocityDirection() ?
                g.velocityDirection().get() : state.v.direction();
    Velocity desired_vel = Velocity(desired_vel_val, desired_vel_dir);

    if (g.approvesPosition(state)) {
        return Kinematics::computeAccOverSpeed(state.v, desired_vel, 0);
    }
    // required acceleration
    auto distance = g.spaceDistance(state);
    assert(distance);

    return Kinematics::computeAccOverDist(state.v, desired_vel, state.pos, g.pos());
}

} // namespace Charlcar
