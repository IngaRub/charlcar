#ifndef DESIRE_H
#define DESIRE_H

#include "utils.h"
#include "goal.h"

#include <deque>

namespace Charlcar {

class Action;
class VehicleModel;
class VehicleState;

class Desire
{
public:
    using GoalCItTy = std::deque<Goal>::const_iterator;

public:
    Desire(const VehicleModel &vm);
    virtual ~Desire();
    Action * planAction(Action *originalAction);
    virtual Action * planAction(const VehicleState &vs, const GoalCItTy &gIt,
                                TimeTy interval, Action *originalAction) = 0;

    void startPrediction();
    void endPrediction();
    bool isPredicting() const;

protected:
    const VehicleModel &m_vehicle;
    bool m_is_predicting = false;
};

} // namespace Charlcar

#endif // DESIRE_H
