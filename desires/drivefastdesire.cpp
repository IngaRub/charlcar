#include "drivefastdesire.h"

#include "actions/accelerateaction.h"
#include "vehiclemodel.h"
#include "simulationexecutor.h"
#include "utils.h"

#define LOG(text) _LOG(DBG_DFD, text)
#define LOG_LINE _LOG_LINE(DBG_DFD)
#define LOG_BEGIN _LOG_BEGIN(DBG_DFD)
#define LOG_END _LOG_END(DBG_DFD)

#define LOG_ID(text) do { \
    LOG("[" + QString::number(m_vehicle.id()) + "]"); \
    LOG(text); \
} while(0)

namespace Charlcar {

DriveFastDesire::~DriveFastDesire()
{

}

Action *DriveFastDesire::planAction(const VehicleState &vs, const GoalCItTy &,
                                    TimeTy interval, Action *originalAction)
{
    Direction dir = vs.v.direction();
    if (dir == Direction::INVALID) {
        dir = vs.a.direction();
    }
    if (dir == Direction::INVALID) {
        dir = vs.ornt;
    }
    if (dir == Direction::INVALID) {
        return originalAction;
    }

    VectorValue::Value desired_speed =
        m_vehicle.desiredSpeed();
    VectorValue::Value current_speed =
        vs.v.value();

    assert(current_speed <= desired_speed + m_vehicle.accMax()); // TODO
    if (current_speed == desired_speed) {
        if (vs.a.value() == 0) {
            return originalAction;
        } else {
            return new AccelerateAction(Acceleration());
        }
    }
    LOG_ID("WANTS TO SPEED UP\n");
    Acceleration acc(Velocity(current_speed, dir),
                     Velocity(desired_speed, dir),
                     interval);
    if (acc.value() > m_vehicle.ACC_MAX_DEFAULT) {
        acc = Acceleration(m_vehicle.ACC_MAX_DEFAULT, dir);
    }
    if (!acc.value()) {
        return originalAction;
    }

    free(originalAction);
    LOG_ID("RETURN Accelerate Action: " << acc.value() << "\n");
    return new AccelerateAction(acc);
}

} // namespace Charlcar
