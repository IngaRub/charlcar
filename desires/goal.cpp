#include "goal.h"

#include "constants.h"
#include "kinematics.h"
#include "vehiclemodel.h"

namespace Charlcar {

Goal::Goal(const VehicleModel &vm, FuzzyCoordTy x, FuzzyCoordTy y) : m_vehicle(vm), m_x(x), m_y(y)
{

}

Goal::~Goal()
{

}

FuzzyPosition Goal::pos() const
{
    return FuzzyPosition(x(), y());
}

const FuzzyValue<Position::CoordTy> & Goal::x() const
{
    return m_x;
}

void Goal::setX(const FuzzyValue<Position::CoordTy> &x)
{
    m_x = x;
}

const FuzzyValue<Position::CoordTy> & Goal::y() const
{
    return m_y;
}

void Goal::setY(const FuzzyValue<Position::CoordTy> &y)
{
    m_y = y;
}

const Maybe<FuzzyValue<VectorValue::Value> > & Goal::speed() const
{
    return m_speed;
}

void Goal::setSpeed(const FuzzyValue<VectorValue::Value> &speed)
{
    m_speed.set(speed);
}

const Maybe<Direction>& Goal::velocityDirection() const
{
    return m_velocity_direction;
}

void Goal::setVelocityDirection(const Direction &velocity_direction)
{
    m_velocity_direction.set(velocity_direction);
}

Maybe<Position::DistTy> Goal::spaceDistance(const VehicleState &vs) const
{
    if (!approvesWay(vs)) {
        return Maybe<Position::DistTy>();
    }
    if (approvesPosition(vs)) {
        return Maybe<Position::DistTy>(0);
    }
    return getDistance(vs.pos, pos());
}

Maybe<TimeTy> Goal::timeDistance(const VehicleState &vs) const
{
    /* Makes sense if the time distance is smaller than a simulation step.
     * Within a simulation step it is impossible to achieve speed greater than desired. */
    if (!approvesWay(vs)) { return Maybe<TimeTy>(); }
    if (!approvesX() || !approvesY()) {
        return Kinematics::computeTime(vs.a, vs.v, vs.pos, pos());
    }

    if (!approvesSpeed()) {
        auto tInt = Kinematics::computeTime(vs.a, vs.v, speed().get());
        if (!tInt || tInt.get().getMin() < 0) { return Maybe<TimeTy>(); }
        return Maybe<TimeTy>(tInt.get().getMin()); // TODO, later we'll deal with intervals
    }

    return Maybe<TimeTy>();
}

bool Goal::isAchieved() const
{
    return isAchieved(m_vehicle.state());
}

bool Goal::isAchieved(const VehicleState &vs) const
{
    return approvesPosition(vs) && approvesVelocity(vs);
}

bool Goal::approvesWay() const
{
    return approvesWay(m_vehicle.state());
}

bool Goal::approvesWay(const VehicleState &vs) const
{
    Position pos = vs.pos;
    Velocity vel = vs.v;
    bool result = true;

    Direction dir = vel.direction();
    if (dir == Direction::INVALID) {
        dir = vs.ornt;
    }

    if (x() > pos.x()) {
        result &= dir == Direction::EAST;
    } else if (x() < pos.x()){
        result &= dir == Direction::WEST;
    } else {
        result &= approvesX(vs);
    }
    result |= !vs.v.value();

    if (result) {
        if (y() > pos.y()) {
            result &= dir == Direction::SOUTH;
        } else if (y() < pos.y()) {
            result &= dir == Direction::NORTH;
        } else {
            result &= approvesY(vs);
        }
        result |= !vs.v.value();
    }
    return result;
}

bool Goal::approvesPosition() const
{
    return approvesPosition(m_vehicle.state());
}

bool Goal::approvesPosition(const VehicleState &vs) const
{
    return approvesPosition(vs.pos);
}

bool Goal::approvesPosition(const Position &pos) const
{
    return approvesX(pos.x()) && approvesY(pos.y());
}

bool Goal::approvesVelocity() const
{
    return approvesVelocity(m_vehicle.state());
}

bool Goal::approvesVelocity(const VehicleState &vs) const
{
    return approvesVelocity(vs.v);
}

bool Goal::approvesVelocity(const Velocity &vel) const
{
    return approvesSpeed(vel) && approvesDirection(vel);
}

bool Goal::approvesX() const
{
    return approvesX(m_vehicle.state());
}

bool Goal::approvesX(const VehicleState &vs) const
{
    return approvesX(vs.pos.x());
}

bool Goal::approvesX(const Position::CoordTy xV) const
{
    return x().includes(xV);
}

bool Goal::approvesY() const
{
    return approvesY(m_vehicle.state());
}

bool Goal::approvesY(const VehicleState &vs) const
{
    return approvesY(vs.pos.y());
}

bool Goal::approvesY(const Position::CoordTy yV) const
{
    return y().includes(yV);
}

bool Goal::approvesSpeed() const
{
    return approvesSpeed(m_vehicle.state());
}

bool Goal::approvesSpeed(const VehicleState &vs) const
{
    return approvesSpeed(vs.v);
}

bool Goal::approvesSpeed(const Velocity &vel) const
{
    return !speed() || speed().get().includes(vel.value());
}

bool Goal::approvesDirection() const
{
    return approvesDirection(m_vehicle.state());
}

bool Goal::approvesDirection(const VehicleState &vs) const
{
    return approvesDirection(vs.v);
}

bool Goal::approvesDirection(const Velocity &vel) const
{
    return !velocityDirection() ||
           velocityDirection().get() == vel.direction();
}

QDebug operator<<(QDebug out, const Goal &goal)
{
    out << "GOAL {";
    out << "pos: " << goal.pos() << "; ";
    if (goal.speed()) {
        out << "speed: " << goal.speed().get() << "; ";
    }
    if (goal.velocityDirection()) {
        out << "direction: "
            << dirToQStr(goal.velocityDirection().get()) << "; ";
    }
    out << "}";
    return out;
}

} // namespace Charlcar
