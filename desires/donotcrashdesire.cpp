#include "donotcrashdesire.h"

#include "simulationexecutor.h"
#include "vehiclemodel.h"
#include "utils.h"

#define LOG(text) _LOG(DBG_DNCD, text)
#define LOG_LINE _LOG_LINE(DBG_DNCD)
#define LOG_BEGIN _LOG_BEGIN(DBG_DNCD)
#define LOG_END _LOG_END(DBG_DNCD)

#define LOG_ID(text) do { \
    LOG("[" + QString::number(m_vehicle.id()) + "]"); \
    LOG(text); \
} while(0)

namespace Charlcar {

using SE = SimulationExecutor;

DoNotCrashDesire::~DoNotCrashDesire()
{

}

Action *DoNotCrashDesire::planAction(const VehicleState &vs,
                                     const DoNotCrashDesire::GoalCItTy &gIt,
                                     TimeTy interval, Action *originalAction)
{
    FuzzyPosition area = m_vehicle.getNeighbourArea(vs, interval);
    LOG_ID("Position:");
    LOG(vs.pos);
    LOG_ID("Neighbourhood area:");
    LOG(area);
    std::vector<const VehicleModel *> neighbours = SE::instance().getNeighbours(
                area, getOppositeDirection(vs.ornt));
    for (const VehicleModel *n : neighbours) {
        LOG_ID(n->id());
    }
    return originalAction;
}

} // namespace Charlcar

