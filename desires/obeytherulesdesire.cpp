#include "obeytherulesdesire.h"

#include "actions/accelerateaction.h"
#include "actions/compositeaction.h"
#include "actions/moveforwardaction.h"
#include "actions/waitaction.h"
#include "kinematics.h"
#include "simulationexecutor.h"
#include "trafficlights.h"
#include "utils.h"

#define LOG(text) _LOG(DBG_OBTR, text)
#define LOG_LINE _LOG_LINE(DBG_OBTR)
#define LOG_BEGIN _LOG_BEGIN(DBG_OBTR)
#define LOG_END _LOG_END(DBG_OBTR)

#define LOG_ID(text) do { \
    LOG("[" + QString::number(m_vehicle.id()) + "]"); \
    LOG(text); \
} while(0)

namespace Charlcar {

using SE = SimulationExecutor;

ObeyTheRulesDesire::~ObeyTheRulesDesire()
{

}

Action *ObeyTheRulesDesire::planAction(const VehicleState &vs, const GoalCItTy &gIt,
                                    TimeTy interval, Action *originalAction)
{
    if (gIt == m_vehicle.getGoalEnd()) {
        LOG_ID("Achieved all goals.\n");
        // TODO
        return originalAction;
    }

    GoalCItTy next_goal = gIt;
    VehicleState vs1 = vs; // before action
    VehicleState vs2 = vs; // after action

    auto closest_tl1_id = getClosestTrafficLights(vs, *gIt);
    if (!closest_tl1_id) {
        return originalAction;
    }
    const auto &closest_tl1 =
            SE::instance().data().getTrafficLights(closest_tl1_id.get());

    LOG_ID("Closest TL1: " + QString::number(closest_tl1_id.get()));
    LOG_ID(vs1.str());
    LOG_ID(*gIt);
    if (next_goal != m_vehicle.getGoalEnd()) {
        LOG_ID(*next_goal);
    }

    // simulate the state
    originalAction->simulate(m_vehicle, vs2, next_goal);

    Maybe<IDTy> closest_tl2_id;
    if (next_goal != m_vehicle.getGoalEnd()) {
        closest_tl2_id = getClosestTrafficLights(vs2, *next_goal);
    }

    auto from_lane = getVehicleLane(vs1);
    auto to_lane = from_lane;

    auto to_lane_m = getTurnLane(closest_tl1, gIt);
    if (to_lane_m) {
        to_lane = to_lane_m.get();
    }

    LOG_ID("From: " + QString::number(from_lane) + " to: " + QString::number(to_lane));

    TimeTy start, duration;
    assert(closest_tl1.getWhenAllowed(from_lane, to_lane, start, duration));

    LOG_ID("Time to cross tl: " + QString::number(start) + " + " + QString::number(duration));

    Action *action = nullptr;

    if (!closest_tl2_id ||
            closest_tl1_id.get() != closest_tl2_id.get()) {
        action = crossTrafficLights(interval, originalAction, closest_tl1,
                           vs1, gIt, start, duration);
    } else {
        assert(gIt == next_goal);
        action = approachTrafficLights(interval, closest_tl1,
                                               vs1, vs2, gIt,
                                               start, duration);
    }

    if (action) {
        free(originalAction);
        return action;
    }

    return originalAction;
}

Action *ObeyTheRulesDesire::crossTrafficLights(TimeTy interval, const Action *originalAction,
                                            const TrafficLights &closest_tl1,
                                            const VehicleState &vs1, ObeyTheRulesDesire::GoalCItTy gIt,
                                            TimeTy start, TimeTy duration)
{
    LOG_ID("Crossing TL of id " + QString::number(closest_tl1.id()));

    if (start > interval) {

        return stopAndWaitForTrafficLights(vs1, closest_tl1, interval);

    } else {

        // 3: state in time interval = start
        VehicleState vs3 = vs1;
        GoalCItTy g3 = gIt;
        originalAction->simulateFor(m_vehicle, vs3, g3, start);
        Maybe<IDTy> closest_tl3_id;
        if (g3 != m_vehicle.getGoalEnd()) {
            closest_tl3_id = getClosestTrafficLights(vs3, *g3);
        }

        if (start + duration < interval) {

            // 4: state in time interval = start
            VehicleState vs4 = vs1;
            GoalCItTy g4 = gIt;
            originalAction->simulateFor(m_vehicle, vs4, g4, start + duration);
            Maybe<IDTy> closest_tl4_id;
            if (g4 != m_vehicle.getGoalEnd()) {
                closest_tl4_id = getClosestTrafficLights(vs4, *g4);
            }

            if (closest_tl4_id &&
                    closest_tl1.id() == closest_tl4_id.get()) {
                // at t=(start+duration) the vehicle is still before TL
                return stopAndWaitForTrafficLights(vs1, closest_tl1, interval);
            } else {
                // at t=(start+duration) the vehicle has crossed the TL
                if (closest_tl3_id &&
                        closest_tl1.id() == closest_tl3_id.get()) {
                    // OK: at t=start the vehicle is still before TL
                } else {
                    // at t=start the vehicle has already crossed the TL
                    return slowDownToCrossTrafficLights(vs1, gIt, vs3, closest_tl1, start, interval);
                }
            }
        } else { // start + duration > interval

            if (closest_tl3_id &&
                    closest_tl1.id() == closest_tl3_id.get()) {
                // OK: at start the vehicle is still before TL
                // and is supposed to cross TL in this step - before start + duration
            } else {
                // slow down and predict
                return slowDownToCrossTrafficLights(vs1, gIt, vs3, closest_tl1, start, interval);
            }
        }
    }
    return nullptr;
}

Action *ObeyTheRulesDesire::slowDownToCrossTrafficLights(const VehicleState &vs1, GoalCItTy gIt,
                                                      const VehicleState &vs3, const TrafficLights &closest_tl1,
                                                      TimeTy slow_down_interval,
                                                      TimeTy step_interval
                                                     )
{
    LOG_BEGIN;

    Acceleration acc = Kinematics::computeAccOverTime(
                vs1.v, slow_down_interval, vs1.pos, closest_tl1.pos());
    Velocity v = vs1.v;
    v.applyAcceleration(acc, slow_down_interval);
    if (v > vs3.v) {
        acc = Kinematics::computeAccOverSpeed(vs1.v, vs3.v, slow_down_interval);
    }
    if (acc.value() > m_vehicle.accMax()) {
        assert(false && "ALERT - traffic lights ahead!");
    }

    CompositeAction *result_action = new CompositeAction();
    auto acc_action = new AccelerateAction(acc);
    result_action->addAction(acc_action);
    auto move_action = new MoveForwardAction();
    move_action->setInterval(std::min(slow_down_interval, step_interval));
    result_action->addAction(move_action);

    if (slow_down_interval >= step_interval) {
        return result_action;
    }

    VehicleState vs5 = vs1;
    GoalCItTy g5 = gIt;
    result_action->simulate(m_vehicle, vs5, g5);

    startPrediction();
    std::vector<Action *> predicted_actions = m_vehicle.plan(
                vs5, g5, step_interval - slow_down_interval);
    endPrediction();

    for (auto action : predicted_actions) {
        result_action->addAction(action);
    }

    return result_action;
}


Action *ObeyTheRulesDesire::stopAndWaitForTrafficLights(const VehicleState &vs,
                                                     const TrafficLights &closest_tl,
                                                     TimeTy interval
                                                     ) const
{
    LOG_BEGIN;

    Acceleration acc = Kinematics::computeAccOverDist(
                vs.v, Velocity(0, 0), vs.pos, closest_tl.pos());
    if (acc.value() > m_vehicle.accMax()) {
        LOG_ID("ALERT - traffic lights ahead! Cannot stop on time.");
    }
    LOG_ID("Acc: " + QString::number(acc.value()));
    LOG_ID("Vel: " + QString::number(vs.v.value()));
    auto mt = Kinematics::computeTime(acc, vs.v, FuzzyValue<Velocity::Value>(0, 0));
    assert(mt);
    LOG_ID("Mt: " + QString::number(mt.get().getMin()));
    CompositeAction *action = new CompositeAction();
    auto acc_action = new AccelerateAction(acc);
    TimeTy time_to_move = mt.get().getMin() < interval ? mt.get().getMin() : interval;
    auto move_action = new MoveForwardAction();
    move_action->setInterval(time_to_move);
    TimeTy time_to_wait = interval - time_to_move;
    auto wait_action = new WaitAction();
    wait_action->setInterval(time_to_wait);
    action->addAction(acc_action);
    action->addAction(move_action);
    action->addAction(wait_action);
    return action;
}

Action *ObeyTheRulesDesire::approachTrafficLights(TimeTy interval,
                                               const TrafficLights &closest_tl1,
                                               const VehicleState &vs1, const VehicleState &vs2,
                                               ObeyTheRulesDesire::GoalCItTy gIt,
                                               TimeTy start, TimeTy duration)
{
    LOG_ID("STATE: " + vs1.str());
    Action *acc_action = nullptr;

    // 3: state in time interval = start
    VehicleState vs3 = vs1;
    auto g3 = gIt;

    startPrediction();
    std::vector<Action *> predicted_actions3 = m_vehicle.plan(vs3, g3, start);
    LOG_ID("State after start: " + vs3.str());

    // 4: state in time interval = start + duration
    VehicleState vs4 = vs3;
    auto g4 = g3;
    std::vector<Action *> predicted_actions4 = m_vehicle.plan(vs4, g4, duration);
    LOG_ID("State after (start + duration): " + vs4.str());
    endPrediction();

    Maybe<IDTy> closest_tl4_id;
    if (g4 != m_vehicle.getGoalEnd()) {
        closest_tl4_id = getClosestTrafficLights(vs4, *g4);
    }

    if (closest_tl4_id &&
            closest_tl4_id.get() == closest_tl1.id()) {
        LOG_ID("I cannot make it to get on time to TL " + QString::number(closest_tl1.id()));
        Acceleration acc1 = Kinematics::computeAccOverDist(
                    vs1.v, Velocity(0, 0), vs1.pos, closest_tl1.pos());
        Acceleration acc2 = Kinematics::computeAccOverDist(
                    vs2.v, Velocity(0, 0), vs2.pos, closest_tl1.pos());
        if (acc1.value() > m_vehicle.accMax()) {
            assert(false && "ALERT - traffic lights ahead!");
        }
        if (acc2.value() > m_vehicle.accMax()) {
            LOG_ID("I'm slowing down with acc: " + QString::number(acc1.value()));
            acc_action = new AccelerateAction(acc1);
        }
    } else {
        LOG_ID("I'll cross traffic lights...");

        Maybe<IDTy> closest_tl3_id;
        if (g3 != m_vehicle.getGoalEnd()) {
            closest_tl3_id = getClosestTrafficLights(vs3, *g3);
        }

        if (closest_tl3_id &&
                closest_tl3_id.get() == closest_tl1.id()) {
            LOG_ID("...just on time.");
        } else {
            LOG_ID("...too early. I should slow down.");
            if (closest_tl3_id) {
                LOG_ID("TL in start time: " + QString::number(closest_tl3_id.get()));
            }
            if (closest_tl4_id) {
                LOG_ID("TL in start + duration time: " + QString::number(closest_tl4_id.get()));
            }
            return slowDownToCrossTrafficLights(vs1, gIt, vs2, closest_tl1, start, interval);
        }
    }

    for (auto *a : predicted_actions3) {
        free(a);
    }
    for (auto *a : predicted_actions4) {
        free(a);
    }
    if (!acc_action) { return nullptr; }

    CompositeAction *result_action = new CompositeAction();
    result_action->addAction(acc_action);
    auto move_action = new MoveForwardAction();
    move_action->setInterval(interval);
    result_action->addAction(move_action);
    return result_action;
}

Maybe<IDTy> ObeyTheRulesDesire::getClosestTrafficLights(const VehicleState &vs, const Goal &g) const
{
    FuzzyPosition goal_pos = g.pos();
    Direction dir_to_goal = getAlongDirection(vs.pos, goal_pos);
    auto dist_to_goal = getDistanceAlong(vs.pos, goal_pos, dir_to_goal);
    assert(dist_to_goal && "The vehicle cannot be in the right position");

    // find the lane of the vehicle
    auto laneID = getVehicleLane(vs);
    const auto &lane = SE::instance().data().grid()->getLane(laneID);

    // consider all traffic lights along the lane
    Maybe<IDTy> closest_tl;
    FuzzyPosition::DistTy min_dist_to_tl = dist_to_goal.get();
    for (auto tl_id : lane.trafficLights()) {
        const TrafficLights &tl = SE::instance().data().getTrafficLights(tl_id);
        Direction dir_to_tl = getAlongDirection(vs.pos, tl.pos());
        if (dir_to_tl != dir_to_goal) { continue; }
        auto dist_to_tl = getDistanceAlong(vs.pos, tl.pos(), dir_to_tl);
        if (dist_to_tl.get() > dist_to_goal.get()) { continue; }
        if (min_dist_to_tl >= dist_to_tl.get()) {
            closest_tl.set(tl.id());
            min_dist_to_tl = dist_to_tl.get();
        }
    }
    return closest_tl;
}

Maybe<IDTy> ObeyTheRulesDesire::getTurnLane(
        const TrafficLights &tl, GoalCItTy gIt) const
{
    assert(gIt != m_vehicle.getGoalEnd());

    Maybe<IDTy> result;
    if (!tl.pos().intersects(gIt->pos())) { return result; }

    auto turn_goal = gIt + 1;
    if (turn_goal == m_vehicle.getGoalEnd()) { return result; }

    auto turn_pos = turn_goal->pos();
    if (!tl.pos().intersects(turn_pos)) { return result; }

    if (!turn_goal->velocityDirection()) { return result; }
    Direction turn_dir = turn_goal->velocityDirection().get();

    if (isVerticalDir(turn_dir)) {
        return SE::instance().data().grid()->whichLaneVertical(turn_pos.getCentrePos(), turn_dir);
    }
    return SE::instance().data().grid()->whichLaneHorizontal(turn_pos.getCentrePos(), turn_dir);
}

IDTy ObeyTheRulesDesire::getVehicleLane(const VehicleState &vs) const
{
    assert(vs.ornt != Direction::INVALID);
    if (isVerticalDir(vs.ornt)) {
        auto laneV = SE::instance().data().grid()->whichLaneVertical(vs.pos, vs.ornt);
        assert(laneV);
        return laneV.get();
    }
    auto laneH = SE::instance().data().grid()->whichLaneHorizontal(vs.pos, vs.ornt);
    assert(laneH);
    return laneH.get();
}

} // namespace Charlcar
