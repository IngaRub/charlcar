#include "gridmodel.h"

#include <cassert>

namespace Charlcar {

LaneModel::LaneModel(IDTy id, unsigned coordV, unsigned from, unsigned to, Direction dirTo, QObject *parent)
    : QObject(parent),
      m_id(id), m_coord(coordV),
      m_from_coord(from), m_to_coord(to),
      m_dirTo(dirTo)
{
    switch(dirTo) {
    case Direction::EAST:
        m_area = FuzzyPosition(fromCoord() * LANE_SIZE, coord() * LANE_SIZE,
                             toCoord() * LANE_SIZE, (coord() + 1) * LANE_SIZE);
        break;
    case Direction::SOUTH:
        m_area = FuzzyPosition(coord() * LANE_SIZE, fromCoord() * LANE_SIZE,
                             (coord() + 1) * LANE_SIZE, toCoord() * LANE_SIZE);
        break;
    case Direction::WEST:
        m_area = FuzzyPosition(toCoord() * LANE_SIZE, coord() * LANE_SIZE,
                             fromCoord() * LANE_SIZE, (coord() + 1) * LANE_SIZE);
        break;
    case Direction::NORTH:
        m_area = FuzzyPosition(coord() * LANE_SIZE, toCoord() * LANE_SIZE,
                             (coord() + 1) * LANE_SIZE, fromCoord() * LANE_SIZE);
        break;
    default:
        assert(false && "Invalid direction for a lane.");
    }
}

IDTy LaneModel::id() const
{
    return m_id;
}

unsigned LaneModel::coord() const
{
    return m_coord;
}

unsigned LaneModel::fromCoord() const
{
    return m_from_coord;
}

unsigned LaneModel::toCoord() const
{
    return m_to_coord;
}

Direction LaneModel::dirTo() const
{
    return m_dirTo;
}

FuzzyPosition LaneModel::area() const
{
    return m_area;
}

bool LaneModel::isHorizontal() const
{
    return isHorizontalDir(dirTo());
}

bool LaneModel::isVertical() const
{
    return isVerticalDir(dirTo());
}

unsigned LaneModel::length() const
{
    return std::abs(toCoord() - fromCoord());
}

bool LaneModel::doesIntersect(const LaneModel &other) const
{
    if (isHorizontalDir(dirTo())) {
        return isVerticalDir(other.dirTo());
    } else {
        return isHorizontalDir(other.dirTo());
    }
}

FuzzyPosition LaneModel::getIntersection(const LaneModel &other) const
{
    assert(doesIntersect(other));
    if (isVerticalDir(dirTo())) {
        assert(isHorizontalDir(other.dirTo()));
        FuzzyPosition::FuzzyCoordTy fx(
                    coord() * LANE_SIZE, (coord() + 1) * LANE_SIZE);
        FuzzyPosition::FuzzyCoordTy fy(
                    other.coord() * LANE_SIZE, (other.coord() + 1) * LANE_SIZE);
        return FuzzyPosition(fx, fy);
    } else {
        assert(isVerticalDir(other.dirTo()));
        FuzzyPosition::FuzzyCoordTy fx(
                    other.coord() * LANE_SIZE, (other.coord() + 1) * LANE_SIZE);
        FuzzyPosition::FuzzyCoordTy fy(
                    coord() * LANE_SIZE, (coord() + 1) * LANE_SIZE);
        return FuzzyPosition(fx, fy);
    }
}

void LaneModel::addTrafficLights(IDTy tl)
{
    m_traffic_lights.insert(tl);
}

range<std::set<IDTy>::const_iterator> LaneModel::trafficLights() const
{
    return make_range(m_traffic_lights.cbegin(), m_traffic_lights.cend());
}

GridModel::GridModel(size_t rows, size_t columns, QObject *parent)
    : QObject(parent), m_rows(rows), m_columns(columns)
{

}

GridModel::~GridModel()
{

}

unsigned GridModel::getRows() const
{
    return m_rows;
}

unsigned GridModel::getColumns() const
{
    return m_columns;
}

unsigned GridModel::getGridWidth() const
{
    return LANE_SIZE * m_columns;
}

unsigned GridModel::getGridHeight() const
{
    return LANE_SIZE * m_rows;
}

range<GridModel::LanesVec::const_iterator>
GridModel::lanes() const
{
    return make_range(m_lanes.cbegin(), m_lanes.cend());
}

LaneModel *GridModel::addLane(
        unsigned coord, unsigned from, unsigned to, Direction dirTo)
{
    return addLane(m_lane_id, coord, from, to, dirTo);
}

LaneModel *GridModel::addLane(IDTy id,
        unsigned coord, unsigned from, unsigned to, Direction dirTo)
{
    assert(m_id_to_lane.find(id) == m_id_to_lane.end());
    m_lane_id = std::max(m_lane_id + 1, id + 1);

    switch (dirTo) {
    case Direction::NORTH:
    case Direction::SOUTH:
        return addLaneVertical(id, coord, from, to, dirTo); break;
    case Direction::WEST:
    case Direction::EAST:
        return addLaneHorizontal(id, coord, from, to, dirTo); break;
    default:
        assert(false && "Invalid direction");
        return nullptr;
    }
}

bool GridModel::deleteLane(IDTy id)
{
    if (m_id_to_lane.find(id) == m_id_to_lane.end()) {
        return false;
    }
    LaneModel *lane_ptr = m_id_to_lane.at(id);
    m_id_to_lane.erase(id);
    auto predicate_ptr = [lane_ptr](LaneModel *lm) { return lm == lane_ptr; };
    m_lanes.erase(std::remove_if(m_lanes.begin(), m_lanes.end(), predicate_ptr), m_lanes.end());

    auto predicate_id = [id](IDTy i) { return id == i; };
    auto iter = m_vertical_lanes.begin();
    auto iterEnd = m_vertical_lanes.end();
    for (; iter != iterEnd; ++iter) {
        iter->second.erase(std::remove_if(iter->second.begin(), iter->second.end(),
                                          predicate_id), iter->second.end());
        if (!iter->second.size()) {
            m_vertical_lanes.erase(iter);
        }
    }

    iter = m_horizontal_lanes.begin();
    iterEnd = m_horizontal_lanes.end();
    for (; iter != iterEnd; ++iter) {
        iter->second.erase(std::remove_if(iter->second.begin(), iter->second.end(),
                                          predicate_id), iter->second.end());
        if (!iter->second.size()) {
            m_horizontal_lanes.erase(iter);
        }
    }
    return true;
}

const LaneModel &GridModel::getLane(IDTy id) const
{
    return getLaneRef(id);
}

LaneModel &GridModel::getLaneRef(IDTy id) const
{
    assert(m_id_to_lane.find(id) != m_id_to_lane.end());
    auto lane = m_id_to_lane.at(id);
    assert(lane);
    return *(lane);
}

std::vector<IDTy> GridModel::whichLaneHorizontal(const Position &pos) const
{
    std::vector<IDTy> result;
    auto hlanes = m_horizontal_lanes.find(pos.y() / LANE_SIZE);
    if (hlanes == m_horizontal_lanes.end()) {
        return result;
    }
    for (auto hlaneID : hlanes->second) {
        const auto &hlane = getLane(hlaneID);
        if (hlane.area().includes(pos)) {
            result.push_back(hlaneID);
        }
    }
    return result;
}

std::vector<IDTy> GridModel::whichLaneVertical(const Position &pos) const
{
    std::vector<IDTy> result;
    auto vlanes = m_vertical_lanes.find(pos.x() / LANE_SIZE);
    if (vlanes == m_vertical_lanes.end()) {
        return result;
    }
    for (auto vlaneID : vlanes->second) {
        const auto &vlane = getLane(vlaneID);
        if (vlane.area().includes(pos)) {
            result.push_back(vlaneID);
        }
    }
    return result;
}

Maybe<IDTy> GridModel::whichLaneHorizontal(const Position &pos, const Direction &dir) const
{
    auto hlanes = m_horizontal_lanes.find(pos.y() / LANE_SIZE);
    if (hlanes == m_horizontal_lanes.end()) {
        return Maybe<IDTy>();
    }
    for (auto hlaneID : hlanes->second) {
        const auto &hlane = getLane(hlaneID);
        if (hlane.area().includes(pos)) {
            if (dir == hlane.dirTo()) {
                return Maybe<IDTy>(hlane.id());
            }
        }
    }
    return Maybe<IDTy>();
}

Maybe<IDTy> GridModel::whichLaneVertical(const Position &pos, const Direction &dir) const
{
    auto vlanes = m_vertical_lanes.find(pos.x() / LANE_SIZE);
    if (vlanes == m_vertical_lanes.end()) {
        return Maybe<IDTy>();
    }
    for (auto vlaneID : vlanes->second) {
        const auto &vlane = getLane(vlaneID);
        if (vlane.area().includes(pos)) {
            if (dir == vlane.dirTo()) {
                return Maybe<IDTy>(vlane.id());
            }
        }
    }
    return Maybe<IDTy>();
}

bool GridModel::isVerticalLaneOverlapping(unsigned coord, unsigned from, unsigned to) const
{
    auto vlanes = m_vertical_lanes.find(coord);
    if (vlanes == m_vertical_lanes.end()) {
        return false;
    }
    uint min = std::min(from, to);
    uint max = std::max(from, to);
    FuzzyPosition area(coord * LANE_SIZE, min * LANE_SIZE,
                      (coord + 1) * LANE_SIZE, max * LANE_SIZE);
    for (auto vlaneID : vlanes->second) {
        const auto &vlane = getLane(vlaneID);
        if (vlane.area().getIntersection(area).rect().height() > LANE_SIZE) {
            return true;
        }
    }
    return false;
}

bool GridModel::isHorizontalLaneOverlapping(unsigned coord, unsigned from, unsigned to) const
{
    auto hlanes = m_horizontal_lanes.find(coord);
    if (hlanes == m_horizontal_lanes.end()) {
        return false;
    }
    uint min = std::min(from, to);
    uint max = std::max(from, to);
    FuzzyPosition area(min * LANE_SIZE, coord * LANE_SIZE,
                       max * LANE_SIZE, (coord + 1) * LANE_SIZE);
    for (auto hlaneID : hlanes->second) {
        const auto &hlane = getLane(hlaneID);
        if (hlane.area().getIntersection(area).rect().width() > LANE_SIZE) {
            return true;
        }
    }
    return false;
}

LaneModel *GridModel::addLaneHorizontal(IDTy id,
        unsigned coord, unsigned from, unsigned to, Direction dirTo)
{
    assert(dirTo == Direction::WEST || dirTo == Direction::EAST);
    assert(coord < m_rows);

    LaneModel *lane = new LaneModel(id, coord, from, to, dirTo, this);
    m_lanes.push_back(lane);
    m_id_to_lane[id] = lane;

    if (m_horizontal_lanes.find(coord) != m_horizontal_lanes.end()) {
        m_horizontal_lanes.at(coord).push_back(id);
    } else {
        std::vector<IDTy> horizontal_lanes;
        horizontal_lanes.push_back(id);
        m_horizontal_lanes.insert(std::make_pair(coord, horizontal_lanes));
    }
    changed();
    return lane;
}

LaneModel *GridModel::addLaneVertical(IDTy id,
        unsigned coord, unsigned from, unsigned to, Direction dirTo)
{
    assert(dirTo == Direction::NORTH || dirTo == Direction::SOUTH);
    assert(coord < m_columns);

    LaneModel *lane = new LaneModel(id, coord, from, to, dirTo, this);
    m_lanes.push_back(lane);
    m_id_to_lane[id] = lane;

    if (m_vertical_lanes.find(coord) != m_vertical_lanes.end()) {
        m_vertical_lanes.at(coord).push_back(id);
    } else {
        std::vector<IDTy> vertical_lanes;
        vertical_lanes.push_back(id);
        m_vertical_lanes.insert(std::make_pair(coord, vertical_lanes));
    }
    changed();
    return lane;
}

} // namespace Charlcar
