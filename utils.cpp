#include "utils.h"

#include "simulationexecutor.h"

#include <cmath>

namespace Charlcar {

bool areOpposite(Direction d1, Direction d2)
{
    if (d1 == Direction::WEST && d2 == Direction::EAST)
        return true;
    if (d2 == Direction::WEST && d1 == Direction::EAST)
        return true;
    if (d1 == Direction::SOUTH && d2 == Direction::NORTH)
        return true;
    if (d2 == Direction::SOUTH && d1 == Direction::NORTH)
        return true;
    return false;
}

Direction getOppositeDirection(Direction dir)
{
    switch (dir) {
    case Direction::NORTH:
        return Direction::SOUTH;
    case Direction::SOUTH:
        return Direction::NORTH;
    case Direction::EAST:
        return Direction::WEST;
    case Direction::WEST:
        return Direction::EAST;
    default:
        assert(false && "Invalid direction");
    }
}

bool isVerticalDir(Direction d)
{
    return d == Direction::NORTH || d == Direction::SOUTH;
}

bool isHorizontalDir(Direction d)
{
    return d == Direction::EAST || d == Direction::WEST;
}

Maybe<double> solveQuadraticEq(double A, double B, double C)
{
    double delta = (B * B) - (4 * A * C);

    if (delta < 0) {
        return Maybe<double>();
    }

    double cand1 = (-B + sqrt(delta)) / (2 * A);
    double cand2 = (-B - sqrt(delta)) / (2 * A);

    if (cand1 >= 0 && cand2 >= 0) {
        double result = cand1 > cand2 ? cand2 : cand1;
        return Maybe<double>(result);
    }
    if (cand1 >= 0) {
        return Maybe<double>(cand1);
    }
    if (cand2 >= 0) {
        return Maybe<double>(cand2);
    }
    return Maybe<double>();
}

std::string dirToStr(Direction dir)
{
    switch (dir) {
    case Direction::NORTH:
        return "NORTH";
    case Direction::SOUTH:
        return "SOUTH";
    case Direction::EAST:
        return "EAST";
    case Direction::WEST:
        return "WEST";
    default:
        return "INVALID";
    }
}

QString dirToQStr(Direction dir)
{
    return QString(dirToStr(dir).c_str());
}

int abs(int a)
{
    return a < 0 ? -a : a;
}

bool areCollinear(uint x1, uint y1, uint x2, uint y2)
{
    return x1 == x2 || y1 == y2;
}

DirTy dirToDirTy(Direction dir)
{
    return static_cast<DirTy>(dir);
}

Direction dirTyToDir(DirTy d)
{
    return static_cast<Direction>(d);
}

} // namespace Charlcar
