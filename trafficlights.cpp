#include "trafficlights.h"

#include "parameters.h"
#include "simulationexecutor.h"

#define LOG(text) _LOG(DBG_TF, text)
#define LOG_LINE _LOG_LINE(DBG_TF)
#define LOG_BEGIN _LOG_BEGIN(DBG_TF)
#define LOG_END _LOG_END(DBG_TF)

namespace Charlcar {

using SE = SimulationExecutor;

Phase::Phase()
{

}

Phase::Phase(TimeTy interval) : m_interval(interval)
{

}

TimeTy Phase::interval() const
{
    return m_interval;
}

void Phase::allow(IDTy from, IDTy to)
{
  m_allowed.insert(AllowedTy(from, to));
}

bool Phase::isAllowed(IDTy from, IDTy to) const
{
  return m_allowed.find(AllowedTy(from, to)) != m_allowed.end();
}

range<std::set<Phase::AllowedTy>::const_iterator>
Phase::allowed() const
{
    return make_range(m_allowed.cbegin(), m_allowed.cend());
}

unsigned Phase::howManyAllowed() const
{
    return m_allowed.size();
}

TrafficLights::TrafficLights(IDTy id, FuzzyPosition pos, QObject *parent)
    : QObject(parent), m_id(id), m_pos(pos), m_current_phase(m_phases.end())
{
    m_wait_time = Parameters::instance().waitPhase();
}

TrafficLights::~TrafficLights()
{

}

void TrafficLights::addLane(SimulationData &data, IDTy lane)
{
    if (m_positions_from.find(lane) != m_positions_from.end()) {
        return;
    }
    LaneModel &lref = data.gridRef()->getLaneRef(lane);
    lref.addTrafficLights(m_id);

    const LaneModel &l = data.grid()->getLane(lane);
    FuzzyPosition posFrom(0, 0, 0, 0);
    FuzzyPosition posTo(0, 0, 0, 0);
    if (l.isHorizontal()) {
        posFrom.setY(l.area().y());
        posTo.setY(l.area().y());
    } else {
        posFrom.setX(l.area().x());
        posTo.setX(l.area().x());
    }
    FuzzyPosition incross = pos().getIntersection(l.area());
    switch (l.dirTo()) {
    case Direction::EAST:
        posFrom.setX(FuzzyCoordTy(incross.x().getMin()));
        posTo.setX(FuzzyCoordTy(incross.x().getMax()));
        break;
    case Direction::WEST:
        posFrom.setX(FuzzyCoordTy(incross.x().getMax()));
        posTo.setX(FuzzyCoordTy(incross.x().getMin()));
        break;
    case Direction::SOUTH:
        posFrom.setY(FuzzyCoordTy(incross.y().getMin()));
        posTo.setY(FuzzyCoordTy(incross.y().getMax()));
        break;
    case Direction::NORTH:
        posFrom.setY(FuzzyCoordTy(incross.y().getMax()));
        posTo.setY(FuzzyCoordTy(incross.y().getMin()));
        break;
    default:
        assert(false && "Invalid lane direction");
    }
    m_positions_from.insert(std::make_pair(lane, posFrom));
    m_positions_to.insert(std::make_pair(lane, posTo));
}

void TrafficLights::addPhase(Phase p)
{
    m_phases.push_back(Phase(m_wait_time));
    m_phases.push_back(p);
    m_current_phase = m_phases.begin();
}

void TrafficLights::setPrepareTime(TimeTy prepare_time)
{
    m_prepare_time = prepare_time;
}

void TrafficLights::setWaitTime(TimeTy wait_time)
{
    m_wait_time = wait_time;
}

IDTy TrafficLights::id() const
{
    return m_id;
}

FuzzyPosition TrafficLights::pos() const
{
    return m_pos;
}

const Phase& TrafficLights::currentPhase() const
{
    return *m_current_phase;
}

FuzzyPosition TrafficLights::getPosFrom(IDTy lane) const
{
    return m_positions_from.at(lane);
}

FuzzyPosition TrafficLights::getPosTo(IDTy lane) const
{
    return m_positions_to.at(lane);
}

void TrafficLights::makeStep(TimeTy interval)
{
    LOG_BEGIN;
    if (interval < m_timer) {
        m_timer -= interval;
        return;
    }
    interval -= m_timer;
    TimeTy timer = 0;
    while (timer <= interval) {
        ++m_current_phase;
        if (m_current_phase == m_phases.end()) {
            m_current_phase = m_phases.begin();
        }
        timer += m_current_phase->interval();
    }
    m_timer = timer - interval;
}

bool TrafficLights::getWhenAllowed(IDTy from, IDTy to, TimeTy &start, TimeTy &duration) const
{
    start = 0;
    std::vector<Phase>::const_iterator phase = m_current_phase;
    if (phase->isAllowed(from, to)) {
        duration = m_timer;
        return true;
    }
    start += m_timer;
    ++phase;
    while (phase != m_current_phase) {
        if (phase == m_phases.end()) {
            phase = m_phases.begin();
        }
        if (phase->isAllowed(from, to)) {
            duration = phase->interval();
            return true;
        }
        start += phase->interval();
        ++phase;
    }
    return false;
}

}
