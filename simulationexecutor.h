#ifndef SIMULATIONEXECUTOR_H
#define SIMULATIONEXECUTOR_H

#include "constants.h"
#include "concepts/position.h"
#include "simulationdata.h"
#include "utils.h"

#include <cmath>
#include <QObject>
#include <QTimer>
#include <map>
#include <type_traits>

namespace Charlcar {

class GridModel;
class VehicleModel;

class SimulationExecutor : public QObject
{
    Q_OBJECT

private:
    explicit SimulationExecutor(QObject *parent = 0);

    SimulationExecutor(SimulationExecutor const&) = delete;
    void operator=(SimulationExecutor const&)  = delete;

public:
    static SimulationExecutor& instance() {
        static SimulationExecutor instance;
        return instance;
    }

public:
    ~SimulationExecutor();

    void setData(SimulationData *data);
    SimulationData &data() const;

    IDTy generateActionID();

    TimeTy interval() const;

    void run();

public:
    std::vector<const VehicleModel *> getNeighbours(FuzzyPosition area,
                                                    Direction from);

public: // utilities
    template<class T, typename std::enable_if<std::is_integral<T>::value, int>::type = 0>
    inline T scaleWithTime(const T &t) const {
        T result = t * (double) interval() / SECOND;
        return result;
    }

    template<class T, typename std::enable_if<!std::is_integral<T>::value, int>::type = 0>
    inline T scaleWithTime(const T& t) const {
        assert(false && "Only integrals can be scaled with time.");
    }

    inline int timeToSteps(TimeTy t) const {
        return ceil((t * (double) SECOND) / interval());
    }

private:
    SimulationData *m_data = nullptr;
    QTimer *m_timer = nullptr;
    unsigned m_step_counter = 0;
    IDTy m_action_id = 0;

public slots:
    void makeStep();

signals:
    void changedData();
    void madeStep();
};

inline TimeTy simInterval() {
    return SimulationExecutor::instance().interval();
}

} // namespace Charlcar

#endif // SIMULATIONEXECUTOR_H
