#ifndef KINEMATICS_H
#define KINEMATICS_H

#include "concepts/acceleration.h"
#include "concepts/position.h"
#include "concepts/velocity.h"

namespace Charlcar {

class Kinematics
{
public:
    Kinematics();
    ~Kinematics();

    static int roundUp(double val);
    static int computeAccVal(int delta_z, int v0z, int vkz);
    static Acceleration computeAccOverDist(
            const Velocity &v0, const Velocity &vk,
            const FuzzyPosition &from, const FuzzyPosition &to);
    static Acceleration computeAccOverSpeed(
            const Velocity &v0, const Velocity &vk, TimeTy interval);
    static Acceleration computeAccOverTime(const Velocity &v0, TimeTy t,
            const FuzzyPosition &from, const FuzzyPosition &to);

    static Maybe<TimeTy> computeTime(const Acceleration &a, const Velocity &v,
                                     const FuzzyPosition &from, const FuzzyPosition &to);
    static Maybe<TimeTy> computeTime(const Velocity &v,
                                     const FuzzyPosition &from, const FuzzyPosition &to);
    static Maybe<FuzzyValue<TimeTy>> computeTime(const Acceleration &a, const Velocity &v,
                                     const FuzzyValue<Velocity::Value> &speed);

    static Position computePosition(const Position &pos, const Velocity &v,
                                    const Acceleration &a, TimeTy t);
    static Position computePosition(const Position &pos, const Velocity &v0,
                                    const Velocity &vk, const Acceleration &a);
};
} // namespace Charlcar

#endif // KINEMATICS_H
