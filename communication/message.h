#ifndef MESSAGE_H
#define MESSAGE_H

#include "actions/action.h"
#include "utils.h"
#include "vehiclemodel.h"

#include <set>

namespace Charlcar {

class Message
{

using ActionPtrRef = Action::ActionPtrRef;

public:
    Message(IDTy receiver, IDTy sender);
    ~Message();

    IDTy id() const;
    void setId(const IDTy &id);

    TimeTy timestamp() const;
    void setTimestamp(const TimeTy &timestamp);

    IDTy receiver() const;
    IDTy sender() const;

private:
    IDTy m_id;
    TimeTy m_timestamp;
    IDTy m_receiver;
    IDTy m_sender;

    std::unique_ptr<const VehicleModel::State> m_state;
    std::set<ActionPtrRef> m_executable_actions;

};

} // namespace Charlcar

#endif // MESSAGE_H
