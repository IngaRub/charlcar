#include "message.h"

namespace Charlcar {

Message::Message(IDTy receiver, IDTy sender)
    : m_receiver(receiver), m_sender(sender)
{

}

Message::~Message()
{

}
IDTy Message::id() const
{
    return m_id;
}

void Message::setId(const IDTy &id)
{
    m_id = id;
}
TimeTy Message::timestamp() const
{
    return m_timestamp;
}

void Message::setTimestamp(const TimeTy &timestamp)
{
    m_timestamp = timestamp;
}
IDTy Message::receiver() const
{
    return m_receiver;
}

IDTy Message::sender() const
{
    return m_sender;
}

} // namespace Charlcar
