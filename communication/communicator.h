#ifndef COMMUNICATOR_H
#define COMMUNICATOR_H

#include "message.h"

#include <QObject>

namespace Charlcar {

class Communicator : public QObject
{
    Q_OBJECT
public:
    explicit Communicator(QObject *parent = 0);
    ~Communicator();

    void receiveMessage(Communicator *sender, Message *msg);
    void sendMessage(Communicator *receiver, Message *msg);

signals:

public slots:
};

} // namespace Charlcar

#endif // COMMUNICATOR_H
