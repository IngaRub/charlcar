#ifndef UTILS_H
#define UTILS_H

#include "concepts/maybe.h"

#include <type_traits>

#include <bitset>
#include <cassert>
#include <QtGlobal>
#include <QtDebug>

/// to -> from

namespace Charlcar {

enum class Direction : unsigned {
    NORTH = 0,
    EAST = 1,
    SOUTH = 2,
    WEST = 3,
    INVALID = 4
};

typedef std::underlying_type<Direction>::type DirTy;

using IDTy = unsigned;
using TimeTy = int;

extern bool areOpposite(Direction d1, Direction d2);
extern Direction getOppositeDirection(Direction dir);
extern bool isVerticalDir(Direction d);
extern bool isHorizontalDir(Direction d);

extern bool areCollinear(uint x1, uint y1, uint x2, uint y2);

extern Maybe<double> solveQuadraticEq(double A, double B, double C);
extern int abs(int a);

extern std::string dirToStr(Direction dir);
extern QString dirToQStr(Direction dir);
extern DirTy dirToDirTy(Direction dir);
extern Direction dirTyToDir(DirTy d);

constexpr unsigned DIRECTION_COUNT = static_cast<unsigned>(Direction::INVALID);
using DirectionSet = std::bitset<DIRECTION_COUNT>;

template <typename E>
constexpr typename std::underlying_type<E>::type enumCast(E e) {
    return static_cast<typename std::underlying_type<E>::type>(e);
}

namespace Architect {
    enum class Mode: unsigned {
        START,
        SELECT_ACTIVE_TILE,
        START_LANE,
        SELECT_LANE,
        ADD_TL,
        EDIT_TL
    };
    typedef std::underlying_type<Mode>::type ModeTy;
} // namespace Architect

#define DBG_VS 1 // DEBUG VEHICLE-STATE
#define DBG_DFD 0 // DEBUG DRIVE-FAST-DESIRE
#define DBG_FTRD 0 // DEBUG FOLLOW-THE-ROUTE-DESIRE
#define DBG_OBTR 1 // DEBUG OBEY-THE-RULES-DESIRE
#define DBG_DNCD 1 // DEBUG DO-NOT-CRASH-DESIRE
#define DBG_TF 1 // DEBUG TRAFFIC-LIGHTS
#define DBG_GR 1 // DEBUG GRIND-RENDERING
#define DBG_ARCH 1 // DEBUG ARCHITECT

#define SEC(d) ((double) d / SECOND)

#define _LOG(flag, text) do { \
    if (flag) \
        qDebug() << text << " "; \
} while (0)

#define _LOG_LINE(flag) do { \
    if (flag) \
        qDebug() << "FUNC: " << __FUNCTION__ << "LINE: " << __LINE__ << "\n"; \
} while (0)

#define _LOG_BEGIN(flag) do { \
    if(flag) \
    qDebug() << Q_FUNC_INFO << ": BEGIN \n"; \
} while (0)

#define _LOG_END(flag) do { \
    if (flag) \
        qDebug() << Q_FUNC_INFO << ": END \n"; \
} while (0)

} // namespace Charlcar

#endif // UTILS_H

