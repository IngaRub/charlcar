#ifndef MAINVIEW_H
#define MAINVIEW_H

#include <QWidget>

/* Show animation and statistics or any monitored critical values. */

namespace Charlcar {

class RenderArea;

class MainView : public QWidget
{
    Q_OBJECT
public:
    explicit MainView(QWidget *parent = 0);
    ~MainView();

private:
    RenderArea *m_renderArea;

signals:

public slots:
};

} // namespace Charlcar

#endif // MAINVIEW_H
