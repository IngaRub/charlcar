#include "architectview.h"

#include "ui/architectwindow.h"
#include "renderarea.h"
#include "simulationdata.h"

#include <QGridLayout>

#define LOG(text) _LOG(DBG_ARCH, text)
#define LOG_LINE _LOG_LINE(DBG_ARCH)
#define LOG_BEGIN _LOG_BEGIN(DBG_ARCH)
#define LOG_END _LOG_END(DBG_ARCH)

namespace Charlcar {

ArchitectView::ArchitectView(SimulationData *data,
                             ArchitectWindow *parent) :
    QWidget(parent), m_parent(parent),
    m_main_layout(new QGridLayout()),
    m_render_area(new RenderArea(this)),
    m_overlay(new ArchitectOverlay(this))
{
    setData(data);
    m_render_area->installEventFilter(this);
    m_overlay->installEventFilter(this);

    m_main_layout->addWidget(m_render_area, 0, 0);
    m_main_layout->addWidget(m_overlay, 0, 0);
    m_overlay->show();

    setLayout(m_main_layout);

    /* signals from ArchitectWindow */
    QObject::connect(m_parent, SIGNAL(sSwitchMode(Mode)),
                     this, SLOT(switchMode(Mode)));
    QObject::connect(m_parent, SIGNAL(sAddLane()), this, SLOT(addLane()));
    QObject::connect(m_parent, SIGNAL(sAddTL()), this, SLOT(addTL()));
    QObject::connect(m_parent, SIGNAL(sDeleteLane()), this, SLOT(deleteLane()));
    QObject::connect(m_parent, SIGNAL(sToggleLaneSelection(IDTy,bool)),
                     this, SLOT(toggleLaneSelection(IDTy, bool)));

    /* signals to ArchitectWindow */
    QObject::connect(this, SIGNAL(sActiveTile(uint,uint,std::bitset<4>)),
                     m_parent, SLOT(showActiveTile(uint,uint,std::bitset<4>)));
    QObject::connect(this, SIGNAL(sHoverTile(uint,uint)),
                     m_parent, SLOT(showHoverTile(uint, uint)));
    QObject::connect(this, SIGNAL(sEndedLane(uint,uint,Direction)),
                     m_parent, SLOT(showEndedLane(uint,uint,Direction)));
    QObject::connect(this, SIGNAL(sEnableDeleteLane(bool)),
                     m_parent, SLOT(enableDeleteLane(bool)));
}

ArchitectView::~ArchitectView()
{

}

void ArchitectView::setData(SimulationData *data)
{
    m_render_area->setData(data);
    m_overlay->setGeometry(m_render_area->geometry());
    if (m_data && m_data != data) { m_data->deleteLater(); }
    m_data = data;
    QObject::connect(m_data, SIGNAL(changed()), m_parent, SLOT(informUnsaved()));
}

SimulationData *ArchitectView::data() const
{
    return m_data;
}

RenderArea *ArchitectView::renderArea()
{
    return m_render_area;
}

Maybe<QRect> ArchitectView::activeTile() const
{
    return m_active_tile;
}

Maybe<QRect> ArchitectView::hoverTile() const
{
    return m_hover_tile;
}

Maybe<QRect> ArchitectView::endedLane() const
{
    return m_ended_lane;
}

Maybe<QRect> ArchitectView::hoverLane() const
{
    return m_hover_lane;
}

std::map<IDTy, Direction> ArchitectView::selectedLanes() const
{
    return m_selected_lanes;
}

std::map<IDTy, Direction> ArchitectView::getPreselectedLanes() const
{
    std::map<IDTy, Direction> info;
    if (!m_active_tile) {
        return info;
    }
    Position active_pos = FuzzyPosition(m_active_tile.get()).getCentrePos();
    std::vector<IDTy> hlanes = m_data->grid()->whichLaneHorizontal(
                m_render_area->fromDisplay(active_pos));
    for (auto hlane_id : hlanes) {
        const auto &hlane = m_data->grid()->getLane(hlane_id);
        info[hlane_id] = hlane.dirTo();
    }
    std::vector<IDTy> vlanes = m_data->grid()->whichLaneVertical(
                m_render_area->fromDisplay(active_pos));
    for (auto vlane_id : vlanes) {
        const auto &vlane = m_data->grid()->getLane(vlane_id);
        info[vlane_id] = vlane.dirTo();
    }
    return info;
}

void ArchitectView::getFromToPreselectedLanes(std::map<IDTy, Direction> &from,
                                              std::map<IDTy, Direction> &to) const
{
    if (!m_active_tile) {
        return;
    }
    FuzzyPosition active_pos =
            m_render_area->fromDisplay(FuzzyPosition(m_active_tile.get()));
    Position active_pos_centre = active_pos.getCentrePos();
    std::vector<IDTy> hlanes =
            m_data->grid()->whichLaneHorizontal(active_pos_centre);

    for (auto hlane_id : hlanes) {
        const auto &hlane = m_data->grid()->getLane(hlane_id);
        if (hlane.dirTo() == Direction::EAST) {
            if (hlane.area().x().getMin() < active_pos.x().getMin()) {
                from[hlane_id] = getOppositeDirection(hlane.dirTo());
            }
            if (hlane.area().x().getMax() > active_pos.x().getMax()) {
                to[hlane_id] = hlane.dirTo();
            }
        } else if (hlane.dirTo() == Direction::WEST) {
            if (hlane.area().x().getMin() < active_pos.x().getMin()) {
                to[hlane_id] = hlane.dirTo();
            }
            if (hlane.area().x().getMax() > active_pos.x().getMax()) {
                from[hlane_id] = getOppositeDirection(hlane.dirTo());
            }
        }
    }
    std::vector<IDTy> vlanes =
            m_data->grid()->whichLaneVertical(active_pos_centre);
    for (auto vlane_id : vlanes) {
        const auto &vlane = m_data->grid()->getLane(vlane_id);
        if (vlane.dirTo() == Direction::NORTH) {
            if (vlane.area().y().getMin() < active_pos.y().getMin()) {
                to[vlane_id] = vlane.dirTo();
            }
            if (vlane.area().y().getMax() > active_pos.y().getMax()) {
                from[vlane_id] = getOppositeDirection(vlane.dirTo());
            }
        } else if (vlane.dirTo() == Direction::SOUTH) {
            if (vlane.area().y().getMin() < active_pos.y().getMin()) {
                from[vlane_id] = getOppositeDirection(vlane.dirTo());
            }
            if (vlane.area().y().getMax() > active_pos.y().getMax()) {
                to[vlane_id] = vlane.dirTo();
            }
        }
    }
}

Direction ArchitectView::getLaneDirection(const QRect &lane, const QRect &tile) const
{
    if (lane.bottom() > tile.bottom()) return Direction::SOUTH;
    if (lane.top() < tile.top()) return Direction::NORTH;
    if (lane.left() < tile.left()) return Direction::WEST;
    if (lane.right() > tile.right()) return Direction::EAST;
    return Direction::INVALID;
}

QRect ArchitectView::getLaneRect(IDTy id) const
{
    const auto &lane = m_data->grid()->getLane(id);
    return m_render_area->toDisplay(lane.area()).rect();
}

void ArchitectView::laneToCoords(const QRect &lane, Direction dir,
                                 uint &coord, uint &from, uint &to) const
{
    uint ls = m_render_area->getDisplayLaneSize();

    switch (dir) {
    case Direction::NORTH:
        coord = lane.left() / ls;
        from = lane.bottom() / ls + 1;
        to = lane.top() / ls;
        break;
    case Direction::EAST:
        coord = lane.top() / ls;
        from = lane.left() / ls;
        to = lane.right() / ls + 1;
        break;
    case Direction::SOUTH:
        coord = lane.left() / ls;
        from = lane.top() / ls;
        to = (lane.bottom() / ls) + 1;
        break;
    case Direction::WEST:
        coord = lane.top() / ls;
        from = lane.right() / ls + 1;
        to = (lane.left() / ls);
        break;
    default:
        return;
    }
}

IDTy ArchitectView::addPhase(const Phase &p)
{
    IDTy id = m_phases.size();
    m_phases.insert(std::make_pair(id, p));
    m_phase_order.push_back(id);
    return id;
}

bool ArchitectView::eventFilter(QObject *obj, QEvent *ev)
{
    if (!obj->isWidgetType()) {
        return false;
    } else if (ev->type() == QEvent::Resize) {
        m_overlay->setGeometry(m_render_area->geometry());
    } else if (ev->type() == QEvent::MouseButtonPress) {
        QMouseEvent *mouse_ev = static_cast<QMouseEvent*>(ev);
        switch (m_mode) {
        case Mode::SELECT_ACTIVE_TILE:
            setActiveTile(mouse_ev->pos()); break;
        case Mode::START_LANE:
            setEndedLane(mouse_ev->pos()); break;
        default: break;
        }
    } else if (ev->type() == QEvent::MouseMove) {
        QMouseEvent *mouse_ev = static_cast<QMouseEvent*>(ev);
        switch (m_mode) {
        case Mode::SELECT_ACTIVE_TILE:
            setHoverTile(mouse_ev->pos()); break;
        case Mode::START_LANE:
            setHoverLane(mouse_ev->pos()); break;
        default: break;
        }
    }
    return false;
}

void ArchitectView::setActiveTile(QPoint pos)
{
    unsigned ls = m_render_area->getDisplayLaneSize();
    unsigned coord_x = pos.x() / ls;
    unsigned coord_y = pos.y() / ls;
    m_active_tile = Maybe<QRect>(QRect(coord_x * ls, coord_y * ls,
                                       ls, ls));
    m_overlay->update();
    std::bitset<4> b;
    b.set(0);
    if (m_data->grid()->whichLaneHorizontal(
                m_render_area->fromDisplay(pos)).size()
            || m_data->grid()->whichLaneVertical(
                m_render_area->fromDisplay(pos)).size()) {
        b.set(1);
        b.set(2);
    }
    sActiveTile(coord_x, coord_y, b);
}

void ArchitectView::setHoverTile(QPoint pos)
{
    unsigned ls = m_render_area->getDisplayLaneSize();
    unsigned coord_x = pos.x() / ls;
    unsigned coord_y = pos.y() / ls;
    m_hover_lane = Maybe<QRect>();
    m_hover_tile = Maybe<QRect>(QRect(coord_x * ls, coord_y * ls,
                                      ls, ls));
    m_overlay->update();
    sHoverTile(coord_x, coord_y);
}

void ArchitectView::setHoverLane(QPoint pos)
{
    setHoverTile(pos);
    assert(m_active_tile);
    assert(m_hover_tile);

    unsigned ls = m_render_area->getDisplayLaneSize();
    unsigned coord_x = pos.x() / ls;
    unsigned coord_y = pos.y() / ls;
    unsigned active_x = m_active_tile.get().x() / ls;
    unsigned active_y = m_active_tile.get().y() / ls;

    if (!areCollinear(coord_x, coord_y,
                      active_x, active_y)) {
        return;
    }
    FuzzyPosition fpos1(m_hover_tile.get());
    FuzzyPosition fpos2(m_active_tile.get());
    auto lane = fpos1.getUnion(fpos2).rect();

    Direction dir = getLaneDirection(lane, m_active_tile.get());
    if (dir == Direction::INVALID) { return; }

    uint coord, from, to;
    laneToCoords(lane, dir, coord, from, to);
    if (isVerticalDir(dir)) {
        if (m_data->grid()->isVerticalLaneOverlapping(coord, from, to)) {
            return;
        }
    } else {
        if (m_data->grid()->isHorizontalLaneOverlapping(coord, from, to)) {
            return;
        }
    }
    m_hover_lane = Maybe<QRect>(lane);
    m_overlay->update();
}

void ArchitectView::setEndedLane(QPoint pos)
{
    if (!m_hover_lane) { return; }
    m_ended_lane = Maybe<QRect>(m_hover_lane.get());

    unsigned ls = m_render_area->getDisplayLaneSize();
    unsigned coord_x = pos.x() / ls;
    unsigned coord_y = pos.y() / ls;

    assert(m_active_tile);
    Direction dir = getLaneDirection(m_ended_lane.get(),
                                     m_active_tile.get());

    m_overlay->update();
    sEndedLane(coord_x, coord_y, dir);
}

void ArchitectView::switchMode(Mode m)
{
    m_mode = m;
    if (m_mode == Mode::START) {
        m_hover_tile = Maybe<QRect>();
        m_active_tile = Maybe<QRect>();
        m_hover_lane = Maybe<QRect>();
        m_ended_lane = Maybe<QRect>();
        m_selected_lanes = std::map<IDTy, Direction>();
        switchMode(Mode::SELECT_ACTIVE_TILE);
    }
    if (m_mode == Mode::SELECT_ACTIVE_TILE) {
        m_selected_lanes = std::map<IDTy, Direction>();
        m_hover_lane = Maybe<QRect>();
        m_ended_lane = Maybe<QRect>();
        m_active_tile = Maybe<QRect>();
    }
    if (m_mode == Mode::SELECT_LANE) {
        m_hover_tile = Maybe<QRect>();
        m_selected_lanes = getPreselectedLanes();
    }
    m_render_area->update();
}

void ArchitectView::addLane()
{
    assert(m_ended_lane);
    assert(m_active_tile);
    QRect ended_lane = m_ended_lane.get();
    QRect active_tile = m_active_tile.get();
    Direction dir = getLaneDirection(ended_lane, active_tile);
    uint coord, from, to;
    laneToCoords(ended_lane, dir, coord, from, to);
    m_data->gridRef()->addLane(coord, from, to, dir);
    m_render_area->update();
}

void ArchitectView::addTL()
{
    assert(m_active_tile);
    TrafficLights *tl = m_data->addTrafficLights(m_active_tile.get());
    for (IDTy id : m_phase_order) {
        tl->addPhase(m_phases.at(id));
    }
}

void ArchitectView::deleteLane()
{
    for (auto id_dir : m_selected_lanes) {
        m_data->gridRef()->deleteLane(id_dir.first);
    }
    m_selected_lanes = std::map<IDTy, Direction>();
    m_render_area->update();
}

void ArchitectView::toggleLaneSelection(IDTy id, bool b)
{
    if (b) {
        m_selected_lanes[id] = m_data->grid()->getLane(id).dirTo();
    } else {
        assert(m_selected_lanes.find(id) != m_selected_lanes.end());
        m_selected_lanes.erase(id);
    }
    sEnableDeleteLane(m_selected_lanes.size());
    m_render_area->update();
}

} // namespace Charlcar
