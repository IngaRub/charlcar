#ifndef ARCHITECTVIEW_H
#define ARCHITECTVIEW_H

#include "ui/architectoverlay.h"
#include "concepts/maybe.h"
#include "trafficlights.h"
#include "utils.h"

#include <QWidget>
#include <map>

class QGridLayout;

namespace Charlcar {

class ArchitectWindow;
class RenderArea;
class SimulationData;

class ArchitectView : public QWidget
{
    Q_OBJECT

    using Mode = Architect::Mode;

public:
    explicit ArchitectView(SimulationData *data,
                           ArchitectWindow *parent);
    ~ArchitectView();
    void setData(SimulationData *data);
    SimulationData *data() const;
    RenderArea *renderArea();
    Maybe<QRect> activeTile() const;
    Maybe<QRect> hoverTile() const;
    Maybe<QRect> endedLane() const;
    Maybe<QRect> hoverLane() const;
    std::map<IDTy, Direction> selectedLanes() const;
    std::map<IDTy, Direction> getPreselectedLanes() const;
    void getFromToPreselectedLanes(std::map<IDTy, Direction> &from, std::map<IDTy, Direction> &to) const;
    Direction getLaneDirection(const QRect &lane, const QRect &tile) const;
    QRect getLaneRect(IDTy id) const;
    void laneToCoords(const QRect &lane, Direction dir,
                      uint &coord, uint &from, uint &to) const;
    IDTy addPhase(const Phase &p);

protected:
    bool eventFilter(QObject *obj, QEvent *ev) override;

private:
    void setActiveTile(QPoint pos);
    void setHoverTile(QPoint pos);
    void setHoverLane(QPoint pos);
    void setEndedLane(QPoint pos);

private:
    ArchitectWindow *m_parent;
    Mode m_mode = Mode::SELECT_ACTIVE_TILE;

    QGridLayout *m_main_layout = nullptr;
    RenderArea *m_render_area = nullptr;
    ArchitectOverlay *m_overlay = nullptr;
    SimulationData *m_data = nullptr;

    Maybe<QRect> m_hover_tile;
    Maybe<QRect> m_active_tile;
    Maybe<QRect> m_hover_lane;
    Maybe<QRect> m_ended_lane;
    std::map<IDTy, Direction> m_selected_lanes;
    std::map<IDTy, Phase> m_phases;
    std::list<IDTy> m_phase_order;

signals:
    void sActiveTile(unsigned coord_x, unsigned coord_y, std::bitset<4> b);
    void sHoverTile(unsigned coord_x, unsigned coord_y);
    void sEndedLane(unsigned coord_x, unsigned coord_y, Direction dir);
    void sEnableDeleteLane(bool);

public slots:
    void switchMode(Mode m);
    void addLane();
    void addTL();
    void deleteLane();
    void toggleLaneSelection(IDTy id, bool b);
};

} // namespace Charlcar

#endif // ARCHITECTVIEW_H
