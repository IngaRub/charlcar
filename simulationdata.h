#ifndef SIMULATIONDATA_H
#define SIMULATIONDATA_H

#include "concepts/range.h"
#include "vehiclemodel.h"

#include <QObject>
#include <vector>

namespace Charlcar {

class GridModel;
class TrafficLights;
class VehicleSource;

class SimulationData : public QObject
{
    Q_OBJECT
public:
    using VehiclesVec = std::vector<VehicleModel *>;
    using VehicleSourcesVec = std::vector<VehicleSource *>;
    using TrafficLightsVec = std::vector<TrafficLights *>;

    using VehiclesMap = std::map<IDTy, VehicleModel *>;
    using VehicleSourcesMap = std::map<IDTy, VehicleSource *>;
    using TrafficLightsMap = std::map<IDTy, TrafficLights *>;

public:
    explicit SimulationData(QObject *parent = 0);
    ~SimulationData();

    range<VehiclesVec::const_iterator> vehicles() const;
    range<VehicleSourcesVec::const_iterator> vehicleSources() const;
    range<TrafficLightsVec::const_iterator> trafficLights() const;

    void setGrid(unsigned rows, unsigned cols);
    const GridModel *grid() const;
    GridModel *gridRef();

    VehicleModel *addVehicle(Position pos, Velocity vel, Direction ornt,
                             VehicleModel::SizeTy length, VehicleModel::SizeTy width); 
    VehicleModel *addVehicle(IDTy id, Position pos, Velocity vel, Direction ornt,
                             VehicleModel::SizeTy length, VehicleModel::SizeTy width);

    const VehicleModel &getVehicle(IDTy id) const;
    VehicleModel &getVehicleRef(IDTy id) const;
    /**
    * Adds an initial place for vehicles.
    * Vehicles will be created every time interval.
    * @param pos position of a vehicle source
    * @param interval timer interval in miliseconds
    */
    VehicleSource *addVehicleSource(Position pos, int interval);
    VehicleSource *addVehicleSource(IDTy id, Position pos, int interval);

    const VehicleSource &getVehicleSource(IDTy id) const;
    VehicleSource &getVehicleSourceRef(IDTy id) const;

    TrafficLights *addTrafficLights(FuzzyPosition pos);
    TrafficLights *addTrafficLights(IDTy id, FuzzyPosition pos);

    const TrafficLights &getTrafficLights(IDTy id) const;
    TrafficLights &getTrafficLightsRef(IDTy id) const;

private:
    IDTy vehicleID();
    IDTy vehicleSourceID();
    IDTy trafficLightsID();

private:
    GridModel *m_grid = nullptr;
    VehiclesVec m_vehicles;
    VehicleSourcesVec m_vehicle_sources;
    TrafficLightsVec m_traffic_lights;

    VehiclesMap m_id_to_vehicle;
    VehicleSourcesMap m_id_to_vehicle_source;
    TrafficLightsMap m_id_to_tl;

    IDTy m_vehicle_id = 0;
    IDTy m_vehicle_source_id = 0;
    IDTy m_tl_id = 0;

signals:
    void changed();

public slots:
};

} // namespace Charlcar

#endif // SIMULATIONDATA_H
