#ifndef CREATETLPHASEDIALOG_H
#define CREATETLPHASEDIALOG_H

#include <QCheckBox>
#include <QDialog>
#include <QRadioButton>

#include "trafficlights.h"

namespace Ui {
class CreateTLPhaseDialog;
}

namespace Charlcar {

class CreateTLPhaseDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreateTLPhaseDialog(QWidget *parent,
                                 std::map<IDTy, Direction> from,
                                 std::map<IDTy, Direction> to);
    ~CreateTLPhaseDialog();

    Phase phase() const;

private:
    Ui::CreateTLPhaseDialog *ui;
    std::map<Direction, IDTy> m_dir_to_lane_from;
    std::map<Direction, IDTy> m_dir_to_lane_to;
    std::map<Direction, QRadioButton *> m_buttons_from;
    std::map<Direction, QCheckBox *> m_buttons_to;
    Phase m_phase;
    Direction m_allowed_from = Direction::INVALID;
    std::bitset<4> m_allowed_to;

private slots:
    void createPhase();
    void fromStateChanged(Direction dir, bool state);
    void toStateChanged(Direction dir, bool state);
    void duration_slider_changed(int);
    void duration_spin_changed(int);
};

} // namespace Charlcar

#endif // CREATETLPHASEDIALOG_H
