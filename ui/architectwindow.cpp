#include "architectwindow.h"
#include "ui_architectwindow.h"

#include "architectview.h"
#include "dataparser.h"
#include "renderarea.h"
#include "simulationdata.h"
#include "utils.h"

#include <QAction>
#include <QCheckBox>
#include <QDir>
#include <QFileDialog>
#include <QGridLayout>
#include <QMenu>
#include <QSettings>

namespace Charlcar {

ArchitectWindow::ArchitectWindow(SimulationData *data,
                                 QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ArchitectWindow)
{
    ui->setupUi(this);
    setWindowTitle("Untitled");

    m_ask_on_exit = new QMessageBox(this);
    m_ask_on_exit->setInformativeText("Do you want to save your changes?");
    m_ask_on_exit->setStandardButtons(QMessageBox::Cancel | QMessageBox::No | QMessageBox::Yes);

    m_main_layout = new QGridLayout;
    ui->centralwidget->setLayout(m_main_layout);

    m_view = new ArchitectView(data, this);
    m_main_layout->addWidget(m_view, 0, 0, 3, 1);

    m_lane_size_label = new QLabel(this);
    m_lane_size_label->setText("Lane size: " +
                QString::number(m_view->renderArea()->getDisplayLaneSize()));
    m_main_layout->addWidget(m_lane_size_label, 0, 1, 1, 1);

    m_info_stack = new QStackedLayout;
    m_main_layout->addLayout(m_info_stack, 1, 1, 1, 1);
    m_choice_stack = new QStackedLayout;
    m_main_layout->addLayout(m_choice_stack, 2, 1, 1, 1);

    m_view->setToolTip(tr("Click on the plane to activate a tile"));

    m_blank_info_box = new QGroupBox(this);
    m_info_stack->addWidget(m_blank_info_box);
    m_blank_choice_box = new QGroupBox(this);
    m_choice_stack->addWidget(m_blank_choice_box);

    createActiveTileInfo();
    createActiveTileChoice();

    createStartLaneInfo();
    createStartLaneChoice();

    createSelectLaneInfo();
    createSelectLaneChoice();

    createAddTLInfo();
    createAddTLChoice();

    ui->menuCreator->addAction(
                "Open...", this, SLOT(openProject()),
                QKeySequence(Qt::CTRL + Qt::Key_O));

    ui->menuCreator->addSeparator();

    ui->menuCreator->addAction(
                "Save", this, SLOT(saveProject()),
                QKeySequence(Qt::CTRL + Qt::Key_S));

    ui->menuCreator->addAction(
                "Save As...", this, SLOT(saveProjectAs()),
                QKeySequence(Qt::SHIFT + Qt::CTRL + Qt::Key_S));
}

ArchitectWindow::~ArchitectWindow()
{
    delete ui;
}

void ArchitectWindow::closeEvent(QCloseEvent *event)
{
    while (!m_saved) {
        int ret = m_ask_on_exit->exec();
        switch (ret) {
        case QMessageBox::Yes: {
            saveProject();
            break;
        }
        case QMessageBox::No: {
            event->accept();
            return;
        }
        case QMessageBox::Cancel: {
            event->ignore();
            return;
        }
        }
    }
    event->accept();
}

void ArchitectWindow::setFilePath(const QString &file_path)
{
    m_file_path = file_path;
}

void ArchitectWindow::createActiveTileInfo()
{
    m_active_tile_info_box = new QGroupBox(this);
    m_active_tile_info_box->setTitle(tr("Active tile"));

    QVBoxLayout *active_tile_info_layout = new QVBoxLayout;
    QLabel *active_tile_label = new QLabel(m_active_tile_info_box);
    m_active_tile_x_label = new QLabel(m_active_tile_info_box);
    m_active_tile_y_label = new QLabel(m_active_tile_info_box);
    active_tile_label->setText(tr("Coordinates") + ": ");
    m_active_tile_x_label->setText("x: --");
    m_active_tile_y_label->setText("y: --");
    active_tile_info_layout->addWidget(active_tile_label);
    active_tile_info_layout->addWidget(m_active_tile_x_label);
    active_tile_info_layout->addWidget(m_active_tile_y_label);
    active_tile_info_layout->addStretch(1);
    m_active_tile_info_box->setLayout(active_tile_info_layout);
    m_info_stack->addWidget(m_active_tile_info_box);
}

void ArchitectWindow::createActiveTileChoice()
{
    m_active_tile_choice_box = new QGroupBox(this);
    m_active_tile_choice_box->setTitle(tr("Choose action"));

    m_start_lane_choice =
            new QPushButton(tr("Start lane"), m_active_tile_choice_box);
    m_select_lane_choice =
            new QPushButton(tr("Select lane"), m_active_tile_choice_box);
    m_add_traffic_lights_choice =
            new QPushButton(tr("Add traffic lights"), m_active_tile_choice_box);
    m_edit_traffic_lights_choice =
            new QPushButton(tr("Edit traffic lights"), m_active_tile_choice_box);
    QObject::connect(m_start_lane_choice, SIGNAL(clicked()),
                     this, SLOT(startLane()));
    QObject::connect(m_select_lane_choice, SIGNAL(clicked()),
                     this, SLOT(selectLane()));
    QObject::connect(m_add_traffic_lights_choice, SIGNAL(clicked()),
                     this, SLOT(addTL()));

    QGroupBox *lane_box = new QGroupBox(m_active_tile_choice_box);
    lane_box->setTitle("Lanes");
    QVBoxLayout *lane_box_choice_layout = new QVBoxLayout;
    lane_box_choice_layout->addWidget(m_start_lane_choice);
    lane_box_choice_layout->addWidget(m_select_lane_choice);
    lane_box->setLayout(lane_box_choice_layout);

    QGroupBox *traffic_lights_box = new QGroupBox(m_active_tile_choice_box);
    traffic_lights_box->setTitle("Traffic lights");
    QVBoxLayout *traffic_lights_box_choice_layout = new QVBoxLayout;
    traffic_lights_box_choice_layout->addWidget(m_add_traffic_lights_choice);
    traffic_lights_box_choice_layout->addWidget(m_edit_traffic_lights_choice);
    traffic_lights_box->setLayout(traffic_lights_box_choice_layout);

    QVBoxLayout *active_tile_choice_layout = new QVBoxLayout;
    active_tile_choice_layout->addWidget(lane_box);
    active_tile_choice_layout->addWidget(traffic_lights_box);
    active_tile_choice_layout->addStretch(1);
    m_active_tile_choice_box->setLayout(active_tile_choice_layout);
    m_choice_stack->addWidget(m_active_tile_choice_box);
}

void ArchitectWindow::createStartLaneInfo()
{
    m_start_lane_info_box = new QGroupBox(this);
    m_start_lane_info_box->setTitle(tr("Start lane"));

    QVBoxLayout *start_lane_info_layout = new QVBoxLayout;
    QLabel *start_lane_from_label = new QLabel(tr("From:"), m_start_lane_info_box);
    QFont font = start_lane_from_label->font();
    font.setBold(true);
    start_lane_from_label->setFont(font);
    m_start_lane_from_x_label = new QLabel("x: --", m_active_tile_info_box);
    m_start_lane_from_y_label = new QLabel("y: --", m_active_tile_info_box);
    QLabel *start_lane_to_label = new QLabel(tr("To") + ":", m_start_lane_info_box);
    start_lane_to_label->setFont(font);
    m_start_lane_to_x_label = new QLabel("x: --", m_active_tile_info_box);
    m_start_lane_to_y_label = new QLabel("y: --", m_active_tile_info_box);
    QLabel *start_lane_dir_label = new QLabel(tr("Towards:"), m_start_lane_info_box);
    start_lane_dir_label->setFont(font);
    m_start_lane_dir_label = new QLabel("--", m_start_lane_info_box);

    start_lane_info_layout->addWidget(start_lane_from_label);
    start_lane_info_layout->addWidget(m_start_lane_from_x_label);
    start_lane_info_layout->addWidget(m_start_lane_from_y_label);
    start_lane_info_layout->addWidget(start_lane_to_label);
    start_lane_info_layout->addWidget(m_start_lane_to_x_label);
    start_lane_info_layout->addWidget(m_start_lane_to_y_label);
    start_lane_info_layout->addWidget(start_lane_dir_label);
    start_lane_info_layout->addWidget(m_start_lane_dir_label);

    start_lane_info_layout->addStretch(1);
    m_start_lane_info_box->setLayout(start_lane_info_layout);
    m_info_stack->addWidget(m_start_lane_info_box);
}

void ArchitectWindow::createStartLaneChoice()
{
    m_start_lane_choice_box = new QGroupBox(this);
    m_start_lane_choice_box->setTitle(tr("Choose action"));

    QVBoxLayout *start_lane_choice_layout = new QVBoxLayout;
    m_add_lane_ok = new QPushButton(tr("Add"), m_start_lane_choice_box);
    m_add_lane_return = new QPushButton(tr("Return"), m_start_lane_choice_box);
    m_add_lane_ok->setDisabled(true);
    QObject::connect(m_add_lane_return, SIGNAL(clicked()),
                     this, SLOT(addLaneCancel()));
    QObject::connect(m_add_lane_ok, SIGNAL(clicked()),
                     this, SLOT(addLane()));

    start_lane_choice_layout->addWidget(m_add_lane_ok);
    start_lane_choice_layout->addWidget(m_add_lane_return);

    start_lane_choice_layout->addStretch(1);
    m_start_lane_choice_box->setLayout(start_lane_choice_layout);
    m_choice_stack->addWidget(m_start_lane_choice_box);
}

void ArchitectWindow::createSelectLaneInfo()
{
    m_select_lane_info_box = new QGroupBox(this);
    m_select_lane_info_box->setTitle(tr("Selected lanes"));

    m_info_stack->addWidget(m_select_lane_info_box);
}

void ArchitectWindow::createSelectLaneChoice()
{
    m_select_lane_choice_box = new QGroupBox(this);
    m_select_lane_choice_box->setTitle(tr("Choose action"));

    QVBoxLayout *select_lane_choice_layout = new QVBoxLayout;

    m_select_lane_del = new QPushButton(tr("Delete"), m_start_lane_choice_box);
    auto select_lane_return = new QPushButton(tr("Return"), m_start_lane_choice_box);
    m_select_lane_del->setDisabled(true);
    QObject::connect(select_lane_return, SIGNAL(clicked()),
                     this, SLOT(selectLaneCancel()));
    QObject::connect(m_select_lane_del, SIGNAL(clicked()),
                     this, SLOT(deleteLane()));
    QObject::connect(select_lane_return, SIGNAL(clicked()),
        this, SIGNAL(sLeftSelectLaneMode()));
    QObject::connect(m_select_lane_del, SIGNAL(clicked()),
        this, SIGNAL(sLeftSelectLaneMode()));

    select_lane_choice_layout->addWidget(m_select_lane_del);
    select_lane_choice_layout->addWidget(select_lane_return);

    select_lane_choice_layout->addStretch(1);
    m_select_lane_choice_box->setLayout(select_lane_choice_layout);
    m_choice_stack->addWidget(m_select_lane_choice_box);
}

void ArchitectWindow::createAddTLInfo()
{
    m_add_tl_info_box = new QGroupBox(this);
    m_add_tl_info_box->setTitle(tr("Related Lanes"));

    m_info_stack->addWidget(m_add_tl_info_box);
}

void ArchitectWindow::createAddTLChoice()
{
    m_add_tl_choice_box = new QGroupBox(this);
    m_add_tl_choice_box->setTitle(tr("Create Traffic Lights"));

    QVBoxLayout *add_tl_choice_layout = new QVBoxLayout;

    // PHASES
    QGroupBox *phases_box = new QGroupBox(m_add_tl_choice_box);
    phases_box->setTitle("Create Phases");
    QVBoxLayout *phases_layout = new QVBoxLayout();
    m_phases_list = new QListWidget(phases_box);
    m_phases_list->setDragDropMode(QAbstractItemView::InternalMove);
    m_phases_list->setSelectionMode(QAbstractItemView::SingleSelection);

    auto add_phase = new QPushButton(tr("New phase"), phases_box);
    m_add_tl_phase_edit = new QPushButton(tr("Edit phase"), phases_box);
    m_add_tl_phase_edit->setEnabled(false);
    m_add_tl_phase_del = new QPushButton(tr("Delete phase"), phases_box);
    m_add_tl_phase_del->setEnabled(false);
    QObject::connect(add_phase, SIGNAL(clicked()),
                     this, SLOT(openPhaseCreator()));
    QObject::connect(m_add_tl_phase_del, SIGNAL(clicked()),
                     this, SLOT(deletePhase()));
    phases_layout->addWidget(m_phases_list);
    phases_layout->addWidget(add_phase);
    phases_layout->addWidget(m_add_tl_phase_edit);
    phases_layout->addWidget(m_add_tl_phase_del);
    phases_box->setLayout(phases_layout);
    add_tl_choice_layout->addWidget(phases_box);

    // ACTION
    QGroupBox *action_box = new QGroupBox(m_add_tl_choice_box);
    action_box->setTitle("Choose action");
    QVBoxLayout *action_layout = new QVBoxLayout();
    m_add_tl_edit = new QPushButton(tr("Add"), action_box);
    auto add_tl_return = new QPushButton(tr("Return"), action_box);
    QObject::connect(m_add_tl_edit, SIGNAL(clicked()),
                     this, SLOT(addTLAdd()));
    QObject::connect(add_tl_return, SIGNAL(clicked()),
                     this, SLOT(addTLCancel()));
    action_layout->addWidget(m_add_tl_edit);
    action_layout->addWidget(add_tl_return);
    action_box->setLayout(action_layout);
    add_tl_choice_layout->addWidget(action_box);

    add_tl_choice_layout->addStretch(1);
    m_add_tl_choice_box->setLayout(add_tl_choice_layout);
    m_choice_stack->addWidget(m_add_tl_choice_box);
}

void ArchitectWindow::addTL()
{
    m_add_tl_info_layout = new QVBoxLayout;
    m_add_tl_info_box->setLayout(m_add_tl_info_layout);
    m_add_tl_edit->setText("Add");

    std::map<IDTy, Direction> selected_lanes = m_view->getPreselectedLanes();
    for (auto id_dir : selected_lanes) {
        QHBoxLayout *add_tl_single_info_layout = new QHBoxLayout();
        QLabel *add_tl_label = new QLabel(tr("Lane "));
        QLabel *add_tl_id_label = new QLabel(QString::number(id_dir.first));
        QLabel *add_tl_id_sep_label = new QLabel(" ");
        QLabel *add_tl_arrow_label = new QLabel(">");
        QLabel *add_tl_to_dir_label = new QLabel(dirToQStr(id_dir.second));

        add_tl_single_info_layout->addWidget(add_tl_label);
        add_tl_single_info_layout->addWidget(add_tl_id_label);
        add_tl_single_info_layout->addWidget(add_tl_id_sep_label);
        add_tl_single_info_layout->addWidget(add_tl_arrow_label);
        add_tl_single_info_layout->addWidget(add_tl_to_dir_label);
        add_tl_single_info_layout->addStretch();
        m_add_tl_info_layout->addLayout(add_tl_single_info_layout);

        QObject::connect(this, SIGNAL(sLeftSelectLaneMode()),
            add_tl_single_info_layout, SLOT(deleteLater()));
        QObject::connect(this, SIGNAL(sLeftSelectLaneMode()),
            add_tl_label, SLOT(deleteLater()));
        QObject::connect(this, SIGNAL(sLeftSelectLaneMode()),
            add_tl_id_label, SLOT(deleteLater()));
        QObject::connect(this, SIGNAL(sLeftSelectLaneMode()),
            add_tl_id_sep_label, SLOT(deleteLater()));
        QObject::connect(this, SIGNAL(sLeftSelectLaneMode()),
            add_tl_arrow_label, SLOT(deleteLater()));
        QObject::connect(this, SIGNAL(sLeftSelectLaneMode()),
            add_tl_to_dir_label, SLOT(deleteLater()));
    }

    m_add_tl_info_layout->addStretch(1);
    QObject::connect(this, SIGNAL(sLeftSelectLaneMode()),
            m_add_tl_info_layout, SLOT(deleteLater()));

    m_info_stack->setCurrentIndex(
                static_cast<Architect::ModeTy>(Mode::ADD_TL));
    m_choice_stack->setCurrentIndex(
                static_cast<Architect::ModeTy>(Mode::ADD_TL));
    sSwitchMode(Mode::ADD_TL);
}

void ArchitectWindow::addTLAdd()
{
    sAddTL();
    m_add_tl_edit->setText("Edit");
}

void ArchitectWindow::addTLCancel()
{
    m_info_stack->setCurrentIndex(
                static_cast<Architect::ModeTy>(Mode::START));
    m_choice_stack->setCurrentIndex(
                static_cast<Architect::ModeTy>(Mode::START));
    sSwitchMode(Mode::SELECT_ACTIVE_TILE);
}

void ArchitectWindow::startLane()
{
    m_start_lane_from_x_label->setText(m_active_tile_x_label->text());
    m_start_lane_from_y_label->setText(m_active_tile_y_label->text());
    m_start_lane_to_x_label->setText("x: --");
    m_start_lane_to_y_label->setText("y: --");
    m_start_lane_dir_label->setText("--");
    m_add_lane_ok->setDisabled(true);
    m_info_stack->setCurrentIndex(
                static_cast<Architect::ModeTy>(Mode::START_LANE));
    m_choice_stack->setCurrentIndex(
                static_cast<Architect::ModeTy>(Mode::START_LANE));
    sSwitchMode(Mode::START_LANE);
}

void ArchitectWindow::selectLane()
{
    m_select_lane_info_layout = new QVBoxLayout;
    m_select_lane_info_box->setLayout(m_select_lane_info_layout);

    std::map<IDTy, Direction> selected_lanes = m_view->getPreselectedLanes();
    for (auto id_dir : selected_lanes) {
        QHBoxLayout *select_single_lane_info_layout = new QHBoxLayout();
        QCheckBox *select_lane_box = new QCheckBox();
        select_lane_box->setChecked(true);
        connect(select_lane_box, &QCheckBox::toggled,
                [this, id_dir](bool b) { this->toggleLaneSelection(id_dir.first, b); });
        QLabel *select_lane_label = new QLabel(tr("Lane "));
        QLabel *select_lane_id_label = new QLabel(QString::number(id_dir.first));
        QLabel *select_lane_id_sep_label = new QLabel(" ");
        QLabel *select_lane_arrow_label = new QLabel(">");
        QLabel *select_lane_to_dir_label = new QLabel(dirToQStr(id_dir.second));

        select_single_lane_info_layout->addWidget(select_lane_box);
        select_single_lane_info_layout->addWidget(select_lane_label);
        select_single_lane_info_layout->addWidget(select_lane_id_label);
        select_single_lane_info_layout->addWidget(select_lane_id_sep_label);
        select_single_lane_info_layout->addWidget(select_lane_arrow_label);
        select_single_lane_info_layout->addWidget(select_lane_to_dir_label);
        select_single_lane_info_layout->addStretch();
        m_select_lane_info_layout->addLayout(select_single_lane_info_layout);

        QObject::connect(this, SIGNAL(sLeftSelectLaneMode()),
            select_single_lane_info_layout, SLOT(deleteLater()));
        QObject::connect(this, SIGNAL(sLeftSelectLaneMode()),
            select_lane_box, SLOT(deleteLater()));
        QObject::connect(this, SIGNAL(sLeftSelectLaneMode()),
            select_lane_label, SLOT(deleteLater()));
        QObject::connect(this, SIGNAL(sLeftSelectLaneMode()),
            select_lane_id_label, SLOT(deleteLater()));
        QObject::connect(this, SIGNAL(sLeftSelectLaneMode()),
            select_lane_id_sep_label, SLOT(deleteLater()));
        QObject::connect(this, SIGNAL(sLeftSelectLaneMode()),
            select_lane_arrow_label, SLOT(deleteLater()));
        QObject::connect(this, SIGNAL(sLeftSelectLaneMode()),
            select_lane_to_dir_label, SLOT(deleteLater()));
    }

    m_select_lane_info_layout->addStretch(1);
    QObject::connect(this, SIGNAL(sLeftSelectLaneMode()),
            m_select_lane_info_layout, SLOT(deleteLater()));

    m_select_lane_del->setEnabled(true);

    m_info_stack->setCurrentIndex(
                static_cast<Architect::ModeTy>(Mode::SELECT_LANE));
    m_choice_stack->setCurrentIndex(
                static_cast<Architect::ModeTy>(Mode::SELECT_LANE));
    sSwitchMode(Mode::SELECT_LANE);
}

void ArchitectWindow::addLane()
{

    sAddLane();
}

void ArchitectWindow::addLaneCancel()
{
    m_info_stack->setCurrentIndex(
                static_cast<Architect::ModeTy>(Mode::START));
    m_choice_stack->setCurrentIndex(
                static_cast<Architect::ModeTy>(Mode::START));
    sSwitchMode(Mode::SELECT_ACTIVE_TILE);
}

void ArchitectWindow::deleteLane()
{
    sDeleteLane();

    m_info_stack->setCurrentIndex(
                static_cast<Architect::ModeTy>(Mode::START));
    m_choice_stack->setCurrentIndex(
                static_cast<Architect::ModeTy>(Mode::START));
    sSwitchMode(Mode::SELECT_ACTIVE_TILE);
}

void ArchitectWindow::selectLaneCancel()
{
    m_info_stack->setCurrentIndex(
                static_cast<Architect::ModeTy>(Mode::START));
    m_choice_stack->setCurrentIndex(
                static_cast<Architect::ModeTy>(Mode::START));
    sSwitchMode(Mode::SELECT_ACTIVE_TILE);
}

void ArchitectWindow::toggleLaneSelection(IDTy id, bool state)
{
    sToggleLaneSelection(id, state);
}

void ArchitectWindow::openPhaseCreator()
{
    std::map<IDTy, Direction> from, to;
    m_view->getFromToPreselectedLanes(from, to);
    CreateTLPhaseDialog dialog(this, from, to);
    dialog.exec();
    Phase p = dialog.phase();
    IDTy id = m_view->addPhase(p);
    QListWidgetItem *item = new QListWidgetItem(
                "Phase" + QString::number(id), m_phases_list);
    item->setData(Qt::UserRole, id);
}

void ArchitectWindow::deletePhase()
{

}

void ArchitectWindow::informSaved()
{
    setWindowTitle(m_file_path.split("/").back());
    m_saved = true;
}

void ArchitectWindow::informUnsaved()
{
    if (!m_file_path.isEmpty()) {
        setWindowTitle(m_file_path.split("/").back() + "*");
    } else {
        setWindowTitle("Untitled*");
    }
    m_saved = false;
}

void ArchitectWindow::inform()
{
    qDebug() << "Inform!";
}

void ArchitectWindow::showActiveTile(uint x, uint y, std::bitset<4> b)
{
    m_info_stack->setCurrentIndex(
                static_cast<Architect::ModeTy>(Mode::SELECT_ACTIVE_TILE));
    m_choice_stack->setCurrentIndex(
                static_cast<Architect::ModeTy>(Mode::SELECT_ACTIVE_TILE));
    m_active_tile_x_label->setText("x: " + QString::number(x));
    m_active_tile_y_label->setText("y: " + QString::number(y));
    m_start_lane_choice->setDisabled(!b.test(0));
    m_select_lane_choice->setDisabled(!b.test(1));
    m_add_traffic_lights_choice->setDisabled(!b.test(2));
    m_edit_traffic_lights_choice->setDisabled(!b.test(3));
}

void ArchitectWindow::showHoverTile(uint x, uint y)
{
    this->statusBar()->showMessage(tr("Coordinates") + ": (" +
                                   QString::number(x) + ", " +
                                   QString::number(y) + ")");
}

void ArchitectWindow::showEndedLane(uint x, uint y, Direction dir)
{
    m_start_lane_to_x_label->setText("x: " + QString::number(x));
    m_start_lane_to_y_label->setText("y: " + QString::number(y));
    m_start_lane_dir_label->setText(dirToQStr(dir));
    m_add_lane_ok->setDisabled(false);
}

void ArchitectWindow::enableDeleteLane(bool b)
{
    m_select_lane_del->setEnabled(b);
}

void ArchitectWindow::openProject()
{
    const QString DEFAULT_DIR_KEY("default_dir");

    QSettings settings;

    auto file_path = QFileDialog::getOpenFileName(this, tr("Open Simulation Data"),
                                             settings.value(DEFAULT_DIR_KEY).toString(),
                                             tr("Data Files (*.json)"));

    if (!file_path.isEmpty()) {
        QDir dir;
        settings.setValue(DEFAULT_DIR_KEY, dir.absoluteFilePath(file_path));

        SimulationData *data = DataParser::load(file_path);
        if (!data) {
            QMessageBox::information(this, "Parsing error",
                                     "File " + file_path + " cannot be parsed properly.");
        } else {
            m_view->setData(data);
            setFilePath(file_path);
            sSwitchMode(Mode::START);
        }
        informSaved();
    }
}

void ArchitectWindow::saveProject()
{
    if (m_file_path.isEmpty()) {
        saveProjectAs();
    } else {
        DataParser::save(m_view->data(), m_file_path);
        informSaved();
    }
}

void ArchitectWindow::saveProjectAs()
{
    const QString DEFAULT_DIR_KEY("default_dir");

    QSettings settings;

    auto file_path = QFileDialog::getSaveFileName(this,
                                                  tr("Save Simulation Data"),
                                                  settings.value(DEFAULT_DIR_KEY).toString(),
                                                  tr("Data Files (*.json)"));
    if (!file_path.isEmpty()) {
        QDir dir;
        settings.setValue(DEFAULT_DIR_KEY, dir.absoluteFilePath(file_path));
        DataParser::save(m_view->data(), file_path);
        setFilePath(file_path);
        informSaved();
    }
}

}
