#include "dataparser.h"

#include "simulationexecutor.h"
#include "trafficlights.h"

#include <QFile>
#include <QJsonArray>

namespace Charlcar {

using SE = SimulationExecutor;

SimulationData *Charlcar::DataParser::load(const QString &filename)
{
    QFile loadFile(filename);

    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Cannot open the file.");
        return nullptr;
    }

    QByteArray saveData = loadFile.readAll();

    QJsonDocument doc(QJsonDocument::fromJson(saveData));
    return load(doc.object());
}

SimulationData *Charlcar::DataParser::load(const QJsonObject &json)
{
    SimulationData *data = new SimulationData(&SE::instance());

    if (!loadGrid(*data, json)) {
        qWarning("Parsing error: grid");
        return nullptr;
    }
    if (!loadLanes(*data, json)) {
        qWarning("Parsing errors: lanes");
        return nullptr;
    }
    if (!loadTrafficLights(*data, json)) {
        qWarning("Parsing errors: traffic lights");
        return nullptr;
    }

    if (!loadVehicles(*data, json)) {
        qWarning("Parsing errors: vehicles");
        return nullptr;
    }
    return data;
}

bool Charlcar::DataParser::loadGrid(SimulationData &data, const QJsonObject &json)
{
    int cols = json["cols"].toInt();
    int rows = json["rows"].toInt();

    data.setGrid(rows, cols);

    return true;
}

bool Charlcar::DataParser::loadLanes(SimulationData &data, const QJsonObject &json)
{
    if (!json["lanes"].isArray()) {
        return false;
    }

    for (const QJsonValue &lane_val : json["lanes"].toArray()) {
        auto lane = lane_val.toObject();
        IDTy id = toUnsigned(lane["id"]);
        unsigned coord = toUnsigned(lane["coord"]);
        unsigned from = toUnsigned(lane["from"]);
        unsigned to = toUnsigned(lane["to"]);
        Direction dir = toDirection(lane["dir"]);
        data.gridRef()->addLane(id, coord, from, to, dir);
    }

    return true;
}

bool Charlcar::DataParser::loadTrafficLights(SimulationData &data, const QJsonObject &json)
{
    if (!json["traffic_lights"].isArray()) {
        return true; // optional
    }

    for (const QJsonValue &tl_val : json["traffic_lights"].toArray()) {
        auto tl_obj = tl_val.toObject();
        IDTy lane1 = tl_obj["lane1"].toInt();
        IDTy lane2 = tl_obj["lane2"].toInt();
        auto grid = data.grid();
        TrafficLights *tl = data.addTrafficLights(
                    grid->getLane(lane1).area().getIntersection(
                        grid->getLane(lane2).area()));
        if (!tl_obj["phases"].isArray()) {
            qWarning("Parsing errors: phases");
            return false;
        }
        for (const QJsonValue &phase_val : tl_obj["phases"].toArray()) {
            auto phase_obj = phase_val.toObject();
            IDTy from = phase_obj["from"].toInt();
            IDTy to = phase_obj["to"].toInt();
            TimeTy time = phase_obj["time"].toInt();
            Phase phase(time);
            phase.allow(from, to);
            tl->addPhase(phase);
            tl->addLane(data, from);
            tl->addLane(data, to);
        }
    }
    return true;
}

bool DataParser::loadVehicles(SimulationData &data, const QJsonObject &json)
{
    if (!json["vehicles"].isArray()) {
        return true; // optional
    }
    for (const QJsonValue &v1_val : json["vehicles"].toArray()) {
        auto v1_obj = v1_val.toObject();

        auto pos_obj = v1_obj["pos"].toObject();
        Position pos(pos_obj["x"].toInt(), pos_obj["y"].toInt());

        Direction dir = toDirection(v1_obj["ornt"]);

        assert(v1_obj["length"].toInt() > 0);
        assert(v1_obj["width"].toInt() > 0);
        VehicleModel::SizeTy length = v1_obj["length"].toInt();
        VehicleModel::SizeTy width = v1_obj["width"].toInt();

        Velocity vel(toVectorValue(v1_obj["vel"]));
        VehicleModel *vm = data.addVehicle(pos, vel, dir, length, width);

        Route route;
        if (!loadRoute(data, route, v1_obj)) {
            return false;
        }
        vm->assignRoute(route);
    }
    return true;
}

bool DataParser::loadRoute(SimulationData &data, Route &route, const QJsonObject &json)
{
    assert(json["route"].isArray());

    for (const QJsonValue &p_val : json["route"].toArray()) {
        if (!p_val.isObject()) { return false; }
        auto x_obj = (p_val.toObject()["x"]).toObject();
        auto y_obj = (p_val.toObject()["y"]).toObject();

        if (exists(x_obj, "lane")) {
            IDTy lane_x = x_obj["lane"].toInt();
            if (exists(y_obj, "lane")) {
                IDTy lane_y = y_obj["lane"].toInt();
                route.pushLaneXY(data, lane_x, lane_y);
            } else {
                if (!exists(y_obj, "min")) { return false; }
                if (!exists(y_obj, "max")) { return false; }
                int y_min = y_obj["min"].toInt();
                int y_max = y_obj["max"].toInt();
                route.pushLaneX(data, lane_x, FuzzyCoordTy(y_min, y_max));
            }
        } else {
            if (!exists(x_obj, "min")) { return false; }
            if (!exists(x_obj, "max")) { return false; }
            int x_min = x_obj["min"].toInt();
            int x_max = x_obj["max"].toInt();
            if (exists(y_obj, "lane")) {
                IDTy lane_y = y_obj["lane"].toInt();
                route.pushLaneY(data, lane_y, FuzzyCoordTy(x_min, x_max));
            } else {
                if (!exists(y_obj, "min")) { return false; }
                if (!exists(y_obj, "max")) { return false; }
                int y_min = y_obj["min"].toInt();
                int y_max = y_obj["max"].toInt();
                route.pushPosition(FuzzyCoordTy(x_min, x_max),
                                   FuzzyCoordTy(y_min, y_max));
            }
        }
    }
    return true;
}

void DataParser::save(const SimulationData *data, const QString &filename)
{
    QFile createFile(filename);
    if (!createFile.open(QIODevice::WriteOnly)) {
        qWarning("Cannot create the file.");
        return;
    }
    QJsonObject json;
    save(data, json);

    QJsonDocument doc;
    doc.setObject(json);

    createFile.write(doc.toJson());
}

void DataParser::save(const SimulationData *data, QJsonObject &json)
{
    saveGrid(data, json);
    saveLanes(data, json);
    saveTrafficLights(data, json);
    saveVehicles(data, json);
}

void DataParser::saveGrid(const SimulationData *data, QJsonObject &json)
{
    QJsonValue r(static_cast<int>(data->grid()->getRows()));
    QJsonValue c(static_cast<int>(data->grid()->getColumns()));
    json.insert("rows", r);
    json.insert("cols", c);
}

void DataParser::saveLanes(const SimulationData *data, QJsonObject &json)
{
    QJsonArray lane_array;
    for (const LaneModel *lane : data->grid()->lanes()) {
        QJsonObject lane_obj;
        lane_obj.insert("id", QJsonValue(static_cast<int>(lane->id())));
        lane_obj.insert("coord", QJsonValue(static_cast<int>(lane->coord())));
        lane_obj.insert("from", QJsonValue(static_cast<int>(lane->fromCoord())));
        lane_obj.insert("to", QJsonValue(static_cast<int>(lane->toCoord())));
        lane_obj.insert("dir", fromDirection(lane->dirTo()));
        lane_array.push_back(QJsonValue(lane_obj));
    }
    json.insert("lanes", QJsonValue(lane_array));
}

void DataParser::saveTrafficLights(const SimulationData *data, QJsonObject &json)
{

}

void DataParser::saveVehicles(const SimulationData *data, QJsonObject &json)
{

}

unsigned Charlcar::DataParser::toUnsigned(const QJsonValue &value)
{
    int vInt = value.toInt();
    assert(vInt >= 0);
    return vInt;
}

Charlcar::Direction Charlcar::DataParser::toDirection(const QJsonValue &value)
{
    assert(value.isString());
    auto str = value.toString("I");
    if (str == "N") {
        return Direction::NORTH;
    }
    if (str == "S") {
        return Direction::SOUTH;
    }
    if (str == "E") {
        return Direction::EAST;
    }
    if (str == "W") {
        return Direction::WEST;
    }
    return Direction::INVALID;
}

VectorValue DataParser::toVectorValue(const QJsonValue &value)
{
    auto v_obj = value.toObject();
    int x = v_obj["x"].toInt();
    int y = v_obj["y"].toInt();
    return VectorValue(x, y);
}

bool DataParser::exists(const QJsonObject &obj, QString name)
{
    if (obj[name].isUndefined()) { return false; }
    return obj[name].type() != 0;
}

QJsonValue DataParser::fromDirection(const Direction &dir)
{
    if (dir == Direction::NORTH) {
        return QJsonValue("N");
    }
    if (dir == Direction::SOUTH) {
        return QJsonValue("S");
    }
    if (dir == Direction::EAST) {
        return QJsonValue("E");
    }
    if (dir == Direction::WEST) {
        return QJsonValue("W");
    }
    return QJsonValue("I"); // Direction:;INVALID
}

} // namespace Charlcar
