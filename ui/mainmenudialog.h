#ifndef MAINMENUDIALOG_H
#define MAINMENUDIALOG_H

#include <QDialog>


namespace Ui {
class MainMenuDialog;
}

namespace Charlcar {

class MainMenuDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MainMenuDialog(QWidget *parent = 0);
    ~MainMenuDialog();

private slots:
    void on_loadInputButton_clicked();

    void on_startButton_clicked();

    void on_setParamsButton_clicked();

    void on_createInputButton_clicked();

private:
    Ui::MainMenuDialog *ui;
};

} // namespace Charlcar

#endif // MAINMENUDIALOG_H
