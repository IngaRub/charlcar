#include "ui/mainmenudialog.h"

#include "simulationexecutor.h"
#include "ui_mainmenudialog.h"
#include "ui/createinputdialog.h"
#include "ui/dataparser.h"
#include "ui/setparametersdialog.h"

#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>
#include <QSettings>

namespace Charlcar {

using SE = SimulationExecutor;

MainMenuDialog::MainMenuDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MainMenuDialog)
{
    ui->setupUi(this);
    ui->loadInputButton->setFocus();
    ui->startButton->setEnabled(false);
}

MainMenuDialog::~MainMenuDialog()
{
    delete ui;
}

void MainMenuDialog::on_loadInputButton_clicked()
{
    const QString DEFAULT_DIR_KEY("default_dir");

    QSettings settings;

    auto file_path = QFileDialog::getOpenFileName(this, tr("Open Simulation Data"),
                                             settings.value(DEFAULT_DIR_KEY).toString(),
                                             tr("Data Files (*.json)"));

    if (!file_path.isEmpty()) {
        QDir dir;
        settings.setValue(DEFAULT_DIR_KEY, dir.absoluteFilePath(file_path));

        SimulationData *data = DataParser::load(file_path);
        if (!data) {
            QMessageBox::information(this, "Parsing error",
                                     "File " + file_path + " cannot be parsed properly.");
        } else {
            SE::instance().setData(data);
            ui->fileNameLabel->setText("Loaded data: " + file_path.section("/", -1, -1));
            ui->startButton->setEnabled(true);
            ui->startButton->setFocus();
        }
    }
}

} // namespace Charlcar

void Charlcar::MainMenuDialog::on_startButton_clicked()
{
    SimulationExecutor::instance().run();
    accept();
}

void Charlcar::MainMenuDialog::on_setParamsButton_clicked()
{
    SetParametersDialog set_parameters_dialog;
    set_parameters_dialog.exec();
}

void Charlcar::MainMenuDialog::on_createInputButton_clicked()
{
    CreateInputDialog create_input_dialog;
    create_input_dialog.exec();
}
