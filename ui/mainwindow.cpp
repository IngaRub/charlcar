#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "gridmodel.h"
#include "mainview.h"

#include <QDebug>
#include <QGridLayout>

namespace Charlcar {

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle("CharlCar");

    m_view = new MainView(this);
    QGridLayout *mainLayout = new QGridLayout;
    mainLayout->addWidget(m_view, 0, 0, 1, 1);
    ui->centralWidget->setLayout(mainLayout);
}

MainWindow::~MainWindow()
{
    delete ui;
}

} // namespace Charlcar
