#ifndef SETPARAMETERSDIALOG_H
#define SETPARAMETERSDIALOG_H

#include <QDialog>

namespace Ui {
class SetParametersDialog;
}

namespace Charlcar {

class SetParametersDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SetParametersDialog(QWidget *parent = 0);
    ~SetParametersDialog();

private slots:
    void on_SliderStepInterval_valueChanged(int value);

    void on_SliderDesiredSpeed_valueChanged(int value);

    void on_SliderWaitPhase_valueChanged(int value);

    void on_ButtonOK_clicked();

    void on_ButtonCancel_clicked();

private:
    Ui::SetParametersDialog *ui;
};

} // namespace Charlcar

#endif // SETPARAMETERSDIALOG_H
