#ifndef DATAPARSER_H
#define DATAPARSER_H

#include "gridmodel.h"
#include "concepts/route.h"
#include "concepts/vectorvalue.h"
#include "simulationdata.h"

#include <QJsonDocument>
#include <QJsonObject>

namespace Charlcar {

class DataParser {
public:
    static SimulationData *load(const QString &filename);
    static SimulationData *load(const QJsonObject &json);
    static bool loadGrid(SimulationData &data, const QJsonObject &json);
    static bool loadLanes(SimulationData &data, const QJsonObject &json);
    static bool loadTrafficLights(SimulationData &data, const QJsonObject &json);
    static bool loadVehicles(SimulationData &data, const QJsonObject &json);
    static bool loadRoute(SimulationData &data, Route &route, const QJsonObject &json);

public:
    static void save(const SimulationData *data, const QString &filename);
    static void save(const SimulationData *data, QJsonObject &json);
    static void saveGrid(const SimulationData *data, QJsonObject &json);
    static void saveLanes(const SimulationData *data, QJsonObject &json);
    static void saveTrafficLights(const SimulationData *data, QJsonObject &json);
    static void saveVehicles(const SimulationData *data, QJsonObject &json);

private:
    DataParser();
    static unsigned toUnsigned(const QJsonValue &value);
    static Direction toDirection(const QJsonValue &value);
    static VectorValue toVectorValue(const QJsonValue &value);
    static bool exists(const QJsonObject &obj, QString name);

    static QJsonValue fromDirection(const Direction &dir);
};

} // namespace Charlcar

#endif
