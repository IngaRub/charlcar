#include "createinputdialog.h"
#include "ui_createinputdialog.h"

#include "ui/architectwindow.h"
#include "simulationdata.h"

namespace Charlcar {

CreateInputDialog::CreateInputDialog(QWidget *parent) :
    QDialog(parent),
    m_parent(parent),
    ui(new Ui::CreateInputDialog)
{
    ui->setupUi(this);
}

CreateInputDialog::~CreateInputDialog()
{
    delete ui;
}

void CreateInputDialog::on_sliderCols_valueChanged(int value)
{
    ui->spinBoxCols->setValue(value);
}

void CreateInputDialog::on_sliderRows_valueChanged(int value)
{
    ui->spinBoxRows->setValue(value);
}

void CreateInputDialog::on_sliderLaneWidth_valueChanged(int value)
{
    ui->spinBoxLaneWidth->setValue(value);
}

void CreateInputDialog::on_spinBoxCols_valueChanged(int arg1)
{
    ui->sliderCols->setValue(arg1);
}

void CreateInputDialog::on_spinBoxRows_valueChanged(int arg1)
{
    ui->sliderRows->setValue(arg1);
}

void CreateInputDialog::on_spinBoxLaneWidth_valueChanged(int arg1)
{
    ui->sliderLaneWidth->setValue(arg1);
}

void CreateInputDialog::on_pushButton_clicked()
{
    SimulationData *data = new SimulationData(m_parent);
    data->setGrid(ui->sliderRows->value(), ui->sliderCols->value());
    ArchitectWindow *arch_win = new ArchitectWindow(data, m_parent);
    arch_win->setWindowModality(Qt::ApplicationModal);
    arch_win->show();
    close();
}

} // namespace Charlcar
