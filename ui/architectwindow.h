#ifndef ARCHITECTWINDOW_H
#define ARCHITECTWINDOW_H

#include "ui/createtlphasedialog.h"
#include "utils.h"

#include <QMainWindow>

#include <QGroupBox>
#include <QLabel>
#include <QListWidget>
#include <QListWidgetItem>
#include <QMessageBox>
#include <QPushButton>
#include <QRadioButton>
#include <QStackedLayout>

namespace Ui {
class ArchitectWindow;
}

class QGridLayout;

namespace Charlcar {

class ArchitectView;
class SimulationData;

class ArchitectWindow : public QMainWindow
{
    Q_OBJECT

    using Mode = Architect::Mode;

public:
    explicit ArchitectWindow(SimulationData *data,
                             QWidget *parent = 0);
    ~ArchitectWindow();

protected:
    void closeEvent(QCloseEvent *event) Q_DECL_OVERRIDE;

private:
    void setFilePath(const QString &file_path);

private:
    void createActiveTileInfo();
    void createActiveTileChoice();

    void createStartLaneInfo();
    void createStartLaneChoice();

    void createSelectLaneInfo();
    void createSelectLaneChoice();

    void createAddTLInfo();
    void createAddTLChoice();

private:
    Ui::ArchitectWindow *ui = nullptr;
    QGridLayout *m_main_layout = nullptr;
    ArchitectView *m_view = nullptr;
    QMessageBox *m_ask_on_exit = nullptr;
    QString m_file_path = "";
    bool m_saved = false;

private:
    QAction *m_open_project = nullptr;
    QAction *m_save_project = nullptr;
    QAction *m_save_project_as = nullptr;

private:
    QLabel *m_lane_size_label = nullptr;

    QStackedLayout *m_info_stack = nullptr;
    QStackedLayout *m_choice_stack = nullptr;

    QGroupBox *m_blank_info_box = nullptr;
    QGroupBox *m_blank_choice_box = nullptr;

    QGroupBox *m_active_tile_info_box = nullptr;
    QLabel *m_active_tile_x_label = nullptr;
    QLabel *m_active_tile_y_label = nullptr;

    QGroupBox *m_start_lane_info_box = nullptr;
    QLabel *m_start_lane_from_x_label = nullptr;
    QLabel *m_start_lane_from_y_label = nullptr;
    QLabel *m_start_lane_to_x_label = nullptr;
    QLabel *m_start_lane_to_y_label = nullptr;
    QLabel *m_start_lane_dir_label = nullptr;

    QGroupBox *m_select_lane_info_box = nullptr;
    QVBoxLayout *m_select_lane_info_layout = nullptr;

    QGroupBox *m_add_tl_info_box = nullptr;
    QVBoxLayout *m_add_tl_info_layout = nullptr;

    QGroupBox *m_active_tile_choice_box = nullptr;
    QPushButton *m_start_lane_choice = nullptr;
    QPushButton *m_select_lane_choice = nullptr;
    QPushButton *m_add_traffic_lights_choice = nullptr;
    QPushButton *m_edit_traffic_lights_choice = nullptr;

    QGroupBox *m_start_lane_choice_box = nullptr;
    QPushButton *m_add_lane_ok = nullptr;
    QPushButton *m_add_lane_return = nullptr;

    QGroupBox *m_select_lane_choice_box = nullptr;
    QPushButton *m_select_lane_del = nullptr;

    QGroupBox *m_add_tl_choice_box = nullptr;
    QListWidget *m_phases_list = nullptr;
    QPushButton *m_add_tl_edit = nullptr;
    QPushButton *m_add_tl_phase_edit = nullptr;
    QPushButton *m_add_tl_phase_del = nullptr;

signals:
    void sSwitchMode(Mode m);
    void sAddLane();
    void sAddTL();
    void sSelectLane();
    void sDeleteLane();
    void sLeftSelectLaneMode();
    void sToggleLaneSelection(IDTy id, bool b);

private slots:
    void startLane();
    void selectLane();
    void addLane();
    void addLaneCancel();
    void deleteLane();
    void selectLaneCancel();
    void toggleLaneSelection(IDTy id, bool state);
    void openPhaseCreator();
    void deletePhase();
    void addTL();
    void addTLAdd();
    void addTLCancel();
    void informSaved();
    void informUnsaved();
    void inform();

public slots:
    void showActiveTile(uint x, uint y, std::bitset<4>);
    void showHoverTile(uint x, uint y);
    void showEndedLane(uint x, uint y, Direction dir);
    void enableDeleteLane(bool b);

public slots:
    void openProject();
    void saveProject();
    void saveProjectAs();
};

} // namespace Charlcar

#endif // ARCHITECTWINDOW_H
