#include "architectoverlay.h"

#include "architectview.h"
#include "concepts/position.h"

#include <QDebug>
#include <QPainter>

namespace Charlcar {

ArchitectOverlay::ArchitectOverlay(ArchitectView *parent) :
    QWidget(parent), m_parent(parent)
{
    setAttribute(Qt::WA_NoSystemBackground);
    setMouseTracking(true);
}

ArchitectOverlay::~ArchitectOverlay()
{
}

void ArchitectOverlay::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    p.fillRect(rect(), QColor(80, 80, 225, 128));
    if (m_parent->activeTile()) {
        p.fillRect(m_parent->activeTile().get(), QColor(100, 0, 0, 128));
    }
    if (m_parent->hoverTile()) {
        p.fillRect(m_parent->hoverTile().get(), QColor(255, 255, 255, 64));
    }
    for (auto selected_lane : m_parent->selectedLanes()) {
        p.fillRect(m_parent->getLaneRect(selected_lane.first),
                   QColor(255, 255, 0, 128));
    }
    if (m_parent->hoverLane()) {
        p.fillRect(m_parent->hoverLane().get(), QColor(255, 255, 255, 128));
        assert(m_parent->activeTile());
        QRect active_tile = m_parent->activeTile().get();
        QRect hover_lane = m_parent->hoverLane().get();
        Direction dir = m_parent->getLaneDirection(hover_lane, active_tile);
        drawLaneDirection(p, active_tile, dir, QColor(255, 100, 100, 128));
    }
    if (m_parent->endedLane()) {
        p.fillRect(m_parent->endedLane().get(), QColor(100, 0, 0, 128));
        assert(m_parent->activeTile());
        QRect active_tile = m_parent->activeTile().get();
        QRect ended_lane = m_parent->endedLane().get();
        Direction dir = m_parent->getLaneDirection(ended_lane, active_tile);
        drawLaneDirection(p, active_tile, dir, QColor(255, 100, 100, 128));
    }
}

void ArchitectOverlay::drawLaneDirection(QPainter &p, QRect tile, Direction dir, QColor c)
{
    p.save();
    uint ls = tile.width();

    QPainterPath path;
    path.moveTo(0, 0);
    path.lineTo(ls / 2, ls / 2);
    path.lineTo(0, ls);
    path.lineTo(0, 0);

    switch (dir) {
    case Direction::EAST: {
        p.translate(tile.topRight()); break;
    }
    case Direction::SOUTH: {
        p.translate(tile.bottomRight());
        p.rotate(90); break;
    }
    case Direction::WEST: {
        p.translate(tile.bottomLeft());
        p.rotate(180); break;
    }
    case Direction::NORTH: {
        p.translate(tile.topLeft());
        p.rotate(270); break;
    }
    default:
        return;
    }
    p.fillPath(path, c);
    p.restore();
}

} // namespace Charlcar

