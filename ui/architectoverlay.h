#ifndef ARCHITECTOVERLAY_H
#define ARCHITECTOVERLAY_H

#include "utils.h"

#include <QWidget>

#include <QEvent>
#include <QPaintEvent>

namespace Charlcar {

class ArchitectView;

/*
 * This is a widget layed upon the render area in order to show elements
 * specific to the Architect Mode, e.g. hovered tiles, selected tiles.
 */
class ArchitectOverlay : public QWidget
{
    Q_OBJECT
public:
    explicit ArchitectOverlay(ArchitectView *parent);
    ~ArchitectOverlay();

protected:
    void paintEvent(QPaintEvent *) override;
    void drawLaneDirection(QPainter &p, QRect tile, Direction dir, QColor c);

private:
    ArchitectView *m_parent;

signals:

public slots:

};

} // namespace Charlcar

#endif // ARCHITECTOVERLAY_H
