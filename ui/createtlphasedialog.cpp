#include "createtlphasedialog.h"
#include "ui_createtlphasedialog.h"

namespace Charlcar {

CreateTLPhaseDialog::CreateTLPhaseDialog(QWidget *parent,
                                         std::map<IDTy, Direction> from,
                                         std::map<IDTy, Direction> to) :
    QDialog(parent),
    ui(new Ui::CreateTLPhaseDialog)
{
    ui->setupUi(this);
    m_buttons_from[Direction::NORTH] = ui->radioButton_N;
    m_buttons_from[Direction::EAST] = ui->radioButton_E;
    m_buttons_from[Direction::SOUTH] = ui->radioButton_S;
    m_buttons_from[Direction::WEST] = ui->radioButton_W;

    m_buttons_to[Direction::NORTH] = ui->checkBox_N;
    m_buttons_to[Direction::EAST] = ui->checkBox_E;
    m_buttons_to[Direction::SOUTH] = ui->checkBox_S;
    m_buttons_to[Direction::WEST] = ui->checkBox_W;

    for (const auto &id_dir : from) {
        m_dir_to_lane_from[id_dir.second] = id_dir.first;
        if (id_dir.second != Direction::INVALID) {
            m_buttons_from[id_dir.second]->setEnabled(true);
        }
    }

    for (const auto &id_dir : to) {
        m_dir_to_lane_to[id_dir.second] = id_dir.first;
        if (id_dir.second != Direction::INVALID) {
            m_buttons_to[id_dir.second]->setEnabled(true);
        }
    }

    for (auto dir_rb : m_buttons_from) {
        connect(dir_rb.second, &QRadioButton::toggled,
                [this, dir_rb] (bool state) {
                    this->fromStateChanged(dir_rb.first, state); });
    }

    for (auto dir_cb : m_buttons_to) {
        connect(dir_cb.second, &QCheckBox::toggled,
                [this, dir_cb] (bool state) {
                    this->toStateChanged(dir_cb.first, state); });
    }

    ui->add_button->setEnabled(false);

    QObject::connect(ui->add_button, SIGNAL(clicked()),
                     this, SLOT(createPhase()));
    QObject::connect(ui->duration_slider, SIGNAL(valueChanged(int)),
                     this, SLOT(duration_slider_changed(int)));
    QObject::connect(ui->duration_spin, SIGNAL(valueChanged(int)),
                     this, SLOT(duration_spin_changed(int)));
}

CreateTLPhaseDialog::~CreateTLPhaseDialog()
{
    delete ui;
}

Phase CreateTLPhaseDialog::phase() const
{
    return m_phase;
}

void CreateTLPhaseDialog::createPhase()
{
    assert(m_allowed_from != Direction::INVALID);
    m_phase = Phase(ui->duration_slider->value());
    IDTy lane_from = m_dir_to_lane_from[m_allowed_from];
    for (int i = 0; i < 4; ++i) {
        if (m_allowed_to[i]) {
            Direction dir = dirTyToDir(i);
            m_phase.allow(lane_from, m_dir_to_lane_to[dir]);
        }
    }
    accept();
}

void CreateTLPhaseDialog::fromStateChanged(Direction dir, bool state)
{
    if (state) {
        m_allowed_from = dir;
        ui->add_button->setEnabled(m_allowed_to.any());
    }
}

void CreateTLPhaseDialog::toStateChanged(Direction dir, bool state)
{
    m_allowed_to.set(dirToDirTy(dir), state);
    ui->add_button->setEnabled(m_allowed_to.any());
}

void CreateTLPhaseDialog::duration_slider_changed(int val)
{
    ui->duration_spin->setValue(val);
}

void CreateTLPhaseDialog::duration_spin_changed(int val)
{
    ui->duration_slider->setValue(val);
}

}
