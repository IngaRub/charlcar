#include "setparametersdialog.h"

#include "parameters.h"
#include "ui_setparametersdialog.h"

#include <QDebug>

namespace Charlcar {

using PS = Parameters;

SetParametersDialog::SetParametersDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SetParametersDialog)
{
    ui->setupUi(this);
    ui->SliderStepInterval->setValue(PS::instance().stepInterval() / 100);
    ui->SliderDesiredSpeed->setValue(PS::instance().desiredSpeed());
    ui->SliderWaitPhase->setValue((PS::instance().waitPhase() - 2000) / 500);

    ui->SliderStepInterval->valueChanged(ui->SliderStepInterval->value());
    ui->SliderDesiredSpeed->valueChanged(ui->SliderDesiredSpeed->value());
    ui->SliderWaitPhase->valueChanged(ui->SliderWaitPhase->value());
}

SetParametersDialog::~SetParametersDialog()
{
    delete ui;
}

void SetParametersDialog::on_SliderStepInterval_valueChanged(int value)
{
    PS::instance().setStepInterval(value * 100);
    ui->LabelStepInterval->setText(
                QString::number(PS::instance().stepInterval()) + " ms");
}

void SetParametersDialog::on_SliderDesiredSpeed_valueChanged(int value)
{
    PS::instance().setDesiredSpeed(value);
    ui->LabelDesiredSpeed->setText(
                QString::number(PS::instance().desiredSpeed()) + " m/s");
}

void SetParametersDialog::on_SliderWaitPhase_valueChanged(int value)
{
    PS::instance().setWaitPhase((4 + value) * 500);
    ui->LabelWaitPhase->setText(
                QString::number(PS::instance().waitPhase()) + " ms");
}

void SetParametersDialog::on_ButtonOK_clicked()
{
    accept();
}

void SetParametersDialog::on_ButtonCancel_clicked()
{
    reject();
}

} // namespace Charlcar
