#ifndef CREATEINPUTDIALOG_H
#define CREATEINPUTDIALOG_H

#include <QDialog>

namespace Ui {
class CreateInputDialog;
}

namespace Charlcar {

class CreateInputDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreateInputDialog(QWidget *parent = 0);
    ~CreateInputDialog();

private slots:
    void on_sliderCols_valueChanged(int value);

    void on_sliderRows_valueChanged(int value);

    void on_sliderLaneWidth_valueChanged(int value);

    void on_spinBoxCols_valueChanged(int arg1);

    void on_spinBoxRows_valueChanged(int arg1);

    void on_spinBoxLaneWidth_valueChanged(int arg1);

    void on_pushButton_clicked();

private:
    QWidget *m_parent = nullptr;
    Ui::CreateInputDialog *ui;
};

} // namespace Charlcar

#endif // CREATEINPUTDIALOG_H
