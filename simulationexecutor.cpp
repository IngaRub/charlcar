#include "simulationexecutor.h"

#include "parameters.h"
#include "trafficlights.h"

namespace Charlcar {

SimulationExecutor::SimulationExecutor(QObject *parent) : QObject(parent)
{

}

SimulationExecutor::~SimulationExecutor()
{
}

void SimulationExecutor::setData(SimulationData *data)
{
    if (m_data) {
        delete m_data;
    }
    m_data = data;
    changedData();
}

SimulationData &SimulationExecutor::data() const
{
    assert(m_data);
    return *m_data;
}

TimeTy SimulationExecutor::interval() const
{
    return Parameters::instance().stepInterval();
}

IDTy SimulationExecutor::generateActionID()
{
    return ++m_action_id;
}

std::vector<const VehicleModel *> SimulationExecutor::getNeighbours(FuzzyPosition area, Direction from)
{
    assert(from != Direction::INVALID);

    std::vector<const VehicleModel *> result;
    for (auto *vehicle : data().vehicles()) {
        if (area.includes(vehicle->pos())) {
            result.push_back(vehicle);
        }
    }

    std::sort(result.begin(), result.end(),
              [from](const VehicleModel *a, const VehicleModel *b) {
        switch (from) {
        case Direction::WEST:
            return a->pos().x() < b->pos().x();
        case Direction::EAST:
            return a->pos().x() > b->pos().x();
        case Direction::NORTH:
            return a->pos().y() < b->pos().y();
        case Direction::SOUTH:
            return a->pos().y() > b->pos().y();
        default:
            return a < b;
        };
        return a < b;
    });

    return result;
}

void SimulationExecutor::run()
{
    m_timer = new QTimer(this);
    connect(m_timer, SIGNAL(timeout()), this, SLOT(makeStep()));
    m_timer->start(interval());
}

void SimulationExecutor::makeStep()
{
    for (auto *vehicle : data().vehicles()) {
        vehicle->makeStep();
    }
    for (auto *tl : data().trafficLights()) {
        tl->makeStep(interval());
    }
    ++m_step_counter;
    madeStep();
}

} // namespace Charlcar
