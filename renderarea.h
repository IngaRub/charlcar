#ifndef RENDERAREA_H
#define RENDERAREA_H

#include "concepts/position.h"
#include "trafficlights.h"
#include "utils.h"

#include <QWidget>

namespace Charlcar {

class SimulationData;

class RenderArea : public QWidget
{
protected:
    Q_OBJECT

    static constexpr int WIDTH = 800;
    static constexpr int HEIGHT = 600;

public:
    explicit RenderArea(QWidget *parent = 0);
    ~RenderArea();

    QSize minimumSizeHint() const Q_DECL_OVERRIDE;
    QSize sizeHint() const Q_DECL_OVERRIDE;
    const SimulationData *getData() const;
    void setData(const SimulationData *data);
    unsigned getDisplayLaneSize() const;
    double getScaleFactor() const;
    unsigned toDisplay(unsigned dist) const;
    Position toDisplay(const Position &pos) const;
    FuzzyPosition toDisplay(const FuzzyPosition &pos) const;
    unsigned fromDisplay(unsigned dist) const;
    Position fromDisplay(const Position &pos) const;
    FuzzyPosition fromDisplay(const FuzzyPosition &pos) const;

protected:
    void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;

private:
    void paintGrid();
    void paintLane(const LaneModel &lane);
    void paintDirection(const LaneModel &lane);
    void paintVehicles();
    void paintTrafficLights();
    void paintTrafficLightsArrow(IDTy from, IDTy to, const Charlcar::TrafficLights *TL);
    void paintTrafficLightsPass(IDTy from, IDTy to, const Charlcar::TrafficLights *TL);
    void paintTrafficLightsNoPass(const TrafficLights *TL);

private:
    const SimulationData *m_data = nullptr;
    unsigned m_rows = 0;
    unsigned m_columns = 0;
    unsigned m_lane_size = 0;

signals:

public slots:
    void setData();
};

} // namespace Charlcar

#endif // RENDERAREA_H
