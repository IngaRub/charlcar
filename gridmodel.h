#ifndef GRIDMODEL_H
#define GRIDMODEL_H

#include "constants.h"
#include "concepts/position.h"
#include "concepts/range.h"
#include "utils.h"

#include <QObject>

#include <map>
#include <set>
#include <vector>

namespace Charlcar {

class LaneModel : public QObject {

    Q_OBJECT
public:
    /**
     * Assumption: from < to in terms of coordinates
     */
    LaneModel(IDTy id, unsigned coord, unsigned from, unsigned to,
              Direction dirTo, QObject *parent = 0);

    IDTy id() const;
    unsigned coord() const;
    unsigned fromCoord() const;
    unsigned toCoord() const;
    Direction dirTo() const;
    FuzzyPosition area() const;

    bool isHorizontal() const;
    bool isVertical() const;
    unsigned length() const;

    bool doesIntersect(const LaneModel &other) const;
    FuzzyPosition getIntersection(const LaneModel &other) const;

    void addTrafficLights(IDTy tl);
    range<std::set<IDTy>::const_iterator> trafficLights() const;


private:
    IDTy m_id;
    unsigned m_coord;
    unsigned m_from_coord, m_to_coord;
    Direction m_dirTo;
    FuzzyPosition m_area;
    std::set<IDTy> m_traffic_lights;
};

class GridModel : public QObject
{
    Q_OBJECT
public:
  using LanesVec = std::vector<LaneModel *>;
  using LanesMap = std::map<IDTy, LaneModel *>;
  using PosLanesMap = std::map<Position::CoordTy, std::vector<IDTy>>;

public:
    explicit GridModel(size_t rows, size_t columns, QObject *parent = 0);
    ~GridModel();

    unsigned getRows() const;
    unsigned getColumns() const;
    unsigned getGridWidth() const;
    unsigned getGridHeight() const;

    range<LanesVec::const_iterator> lanes() const;

    LaneModel *addLane(
            unsigned coord, unsigned from, unsigned to, Direction dirTo);
    LaneModel *addLane(IDTy id, unsigned coord,
                 unsigned from, unsigned to, Direction dirTo);
    bool deleteLane(IDTy id);

    const LaneModel &getLane(IDTy id) const;
    LaneModel &getLaneRef(IDTy id) const;
    std::vector<IDTy> whichLaneHorizontal(const Position &pos) const;
    std::vector<IDTy> whichLaneVertical(const Position &pos) const;
    Maybe<IDTy> whichLaneHorizontal(const Position &pos, const Direction &dir) const;
    Maybe<IDTy> whichLaneVertical(const Position &pos, const Direction &dir) const;
    bool isVerticalLaneOverlapping(unsigned coord, unsigned from, unsigned to) const;
    bool isHorizontalLaneOverlapping(unsigned coord, unsigned from, unsigned to) const;

private:
    LaneModel *addLaneVertical(IDTy id, unsigned coord,
                         unsigned from, unsigned to, Direction dirTo);
    LaneModel *addLaneHorizontal(IDTy id, unsigned coord, unsigned from, unsigned to, Direction dirTo);

private:
    unsigned m_rows;
    unsigned m_columns;

    IDTy m_lane_id = 1;
    LanesVec m_lanes;
    LanesMap m_id_to_lane;
    PosLanesMap m_vertical_lanes;
    PosLanesMap m_horizontal_lanes;

signals:
    void changed();

public slots:
};

} // namespace Charlcar

#endif // GRIDMODEL_H
