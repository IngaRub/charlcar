#include "simulationdata.h"

#include "trafficlights.h"
#include "vehiclesource.h"

#include <cmath>

namespace Charlcar {

SimulationData::SimulationData(QObject *parent) : QObject(parent)
{

}

SimulationData::~SimulationData()
{

}

range<SimulationData::VehiclesVec::const_iterator>
SimulationData::vehicles() const
{
    return make_range(m_vehicles.cbegin(), m_vehicles.cend());
}

range<SimulationData::VehicleSourcesVec::const_iterator>
SimulationData::vehicleSources() const
{
    return make_range(m_vehicle_sources.cbegin(), m_vehicle_sources.end());
}

range<SimulationData::TrafficLightsVec::const_iterator>
SimulationData::trafficLights() const
{
    return make_range(m_traffic_lights.cbegin(), m_traffic_lights.cend());
}

void SimulationData::setGrid(unsigned rows, unsigned cols)
{
    if (m_grid) {
        delete m_grid;
    }
    m_grid = new GridModel(rows, cols, this);
    QObject::connect(m_grid, SIGNAL(changed()), this, SIGNAL(changed()));
    changed();
}

const GridModel *SimulationData::grid() const
{
    assert(m_grid);
    return m_grid;
}

GridModel *SimulationData::gridRef()
{
    assert(m_grid);
    return m_grid;
}

VehicleModel *SimulationData::addVehicle(
        Position pos, Velocity vel, Direction ornt,
        VehicleModel::SizeTy length, VehicleModel::SizeTy width)
{
    return addVehicle(vehicleID(), pos, vel, ornt, length, width);
}

VehicleModel *SimulationData::addVehicle(
        IDTy id, Position pos, Velocity vel, Direction ornt,
        VehicleModel::SizeTy length, VehicleModel::SizeTy width)
{
    assert(m_id_to_vehicle.find(id) == m_id_to_vehicle.end());
    m_vehicle_id = std::max(m_vehicle_id + 1, id + 1);

    VehicleModel *V = new VehicleModel(id, length, width, this);
    V->setPos(pos);
    V->setVel(vel);
    V->setOrnt(ornt);
    m_vehicles.push_back(V);
    m_id_to_vehicle[id] = V;

    changed();
    return V;
}

const VehicleModel &SimulationData::getVehicle(IDTy id) const
{
    return getVehicleRef(id);
}

VehicleModel &SimulationData::getVehicleRef(IDTy id) const
{ 
    assert(m_id_to_vehicle.find(id) != m_id_to_vehicle.end());
    auto *v = m_id_to_vehicle.at(id);
    assert(v);
    return *v;
}

VehicleSource *SimulationData::addVehicleSource(Position pos, int interval)
{
    return addVehicleSource(vehicleSourceID(), pos, interval);
}

VehicleSource *SimulationData::addVehicleSource(
        IDTy id, Position pos, int interval)
{
    assert(m_id_to_vehicle_source.find(id) == m_id_to_vehicle_source.end());
    m_vehicle_source_id = std::max(m_vehicle_source_id + 1, id + 1);

    VehicleSource *vs = new VehicleSource(id, pos, interval, this);
    m_vehicle_sources.push_back(vs);
    m_id_to_vehicle_source[id] = vs;

    changed();
    return vs;
}

const VehicleSource &SimulationData::getVehicleSource(IDTy id) const
{
    return getVehicleSourceRef(id);
}

VehicleSource &SimulationData::getVehicleSourceRef(IDTy id) const
{
    assert(m_id_to_vehicle_source.find(id) != m_id_to_vehicle_source.end());
    auto *vs = m_id_to_vehicle_source.at(id);
    assert(vs);
    return *vs;
}

TrafficLights *SimulationData::addTrafficLights(FuzzyPosition pos)
{
    return addTrafficLights(trafficLightsID(), pos);
}

TrafficLights *SimulationData::addTrafficLights(IDTy id, FuzzyPosition pos)
{
    assert(m_id_to_tl.find(id) == m_id_to_tl.end());
    m_tl_id = std::max(m_tl_id + 1, id + 1);

    TrafficLights *tl = new TrafficLights(id, pos, this);
    m_traffic_lights.push_back(tl);
    m_id_to_tl[id] = tl;

    changed();
    return tl;
}

const TrafficLights &SimulationData::getTrafficLights(IDTy id) const
{
    return getTrafficLightsRef(id);
}

TrafficLights &SimulationData::getTrafficLightsRef(IDTy id) const
{
    assert(m_id_to_tl.find(id) != m_id_to_tl.end());
    auto *tl = m_id_to_tl.at(id);
    assert(tl);
    return *tl;
}

IDTy SimulationData::vehicleID()
{
    return m_vehicle_id;
}

IDTy SimulationData::vehicleSourceID()
{
   return m_vehicle_source_id;
}

IDTy SimulationData::trafficLightsID()
{
   return m_tl_id;
}

} // namespace Charlcar
