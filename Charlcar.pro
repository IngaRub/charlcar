#-------------------------------------------------
#
# Project created by QtCreator 2015-05-04T21:09:05
#
#-------------------------------------------------

QT       += core gui
QMAKE_CXXFLAGS += -std=c++11
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Charlcar
TEMPLATE = app

SOURCES += main.cpp\
    actions/accelerateaction.cpp \
    actions/action.cpp \
    actions/compositeaction.cpp \
    actions/moveforwardaction.cpp \
    actions/turnaction.cpp \
    actions/waitaction.cpp \
    actions/wrapperaction.cpp \
    communication/communicator.cpp \
    communication/message.cpp \
    concepts/acceleration.cpp \
    concepts/fuzzyvalue.cpp \
    concepts/maybe.cpp \
    concepts/position.cpp \
    concepts/route.cpp \
    concepts/vectorvalue.cpp \
    concepts/velocity.cpp \
    desires/desire.cpp \
    desires/donotcrashdesire.cpp \
    desires/drivefastdesire.cpp \
    desires/followtheroutedesire.cpp \
    desires/goal.cpp \
    desires/obeytherulesdesire.cpp \
    gridmodel.cpp \
    kinematics.cpp \
    mainview.cpp \
    parameters.cpp \
    renderarea.cpp \
    settingsmodel.cpp \
    simulationdata.cpp \
    simulationexecutor.cpp \
    trafficlights.cpp \
    ui/architectwindow.cpp \
    ui/createinputdialog.cpp \
    ui/dataparser.cpp \
    ui/mainmenudialog.cpp \
    ui/mainwindow.cpp \
    ui/setparametersdialog.cpp \
    utils.cpp \
    vehiclemodel.cpp \
    vehiclesource.cpp \
    architectview.cpp \
    ui/architectoverlay.cpp \
    ui/createtlphasedialog.cpp

HEADERS  += mainwindow.h \
    actions/accelerateaction.h \
    actions/action.h \
    actions/compositeaction.h \
    actions/moveforwardaction.h \
    actions/turnaction.h \
    actions/waitaction.h \
    actions/wrapperaction.h \
    communication/communicator.h \
    communication/message.h \
    concepts/acceleration.h \
    concepts/condition.h \
    concepts/fuzzyvalue.h \
    concepts/maybe.h \
    concepts/position.h \
    concepts/range.h \
    concepts/route.h \
    concepts/vectorvalue.h \
    concepts/velocity.h \
    constants.h \
    desires/desire.h \
    desires/donotcrashdesire.h \
    desires/drivefastdesire.h \
    desires/followtheroutedesire.h \
    desires/goal.h \
    desires/obeytherulesdesire.h \
    gridmodel.h \
    kinematics.h \
    mainview.h \
    parameters.h \
    renderarea.h \
    settingsmodel.h \
    simulationdata.h \
    simulationexecutor.h \
    trafficlights.h \
    ui/architectwindow.h \
    ui/createinputdialog.h \
    ui/dataparser.h \
    ui/mainmenudialog.h \
    ui/mainwindow.h \
    ui/setparametersdialog.h \
    utils.h \
    vehiclemodel.h \
    vehiclesource.h \
    architectview.h \
    ui/architectoverlay.h \
    ui/createtlphasedialog.h

FORMS    += ui/mainwindow.ui \
    ui/setparametersdialog.ui \
    ui/mainmenudialog.ui \
    ui/createinputdialog.ui \
    ui/architectwindow.ui \
    ui/createtlphasedialog.ui
