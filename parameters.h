#ifndef PARAMETERS_H
#define PARAMETERS_H

#include "concepts/vectorvalue.h"
#include "constants.h"
#include "utils.h"

#include <QObject>

namespace Charlcar {

class Parameters : public QObject
{
    Q_OBJECT

public:
    static constexpr unsigned DESIRED_SPEED_DEFAULT = 20;
    static constexpr unsigned STEP_INTERVAL_DEFAULT = SECOND;
    static constexpr unsigned WAIT_PHASE_DEFAULT = 2 * SECOND;

private:
    explicit Parameters(QObject *parent = 0);
    Parameters(Parameters const&) = delete;
    void operator=(Parameters const&) = delete;

public:
    ~Parameters();
    static Parameters& instance() {
        static Parameters instance;
        return instance;
    }

    TimeTy stepInterval() const;
    void setStepInterval(const TimeTy &step_interval);

    VectorValue::Value desiredSpeed() const;
    void setDesiredSpeed(const VectorValue::Value &desired_speed);

    TimeTy waitPhase() const;
    void setWaitPhase(const TimeTy &wait_phase);

private:
    TimeTy m_step_interval = STEP_INTERVAL_DEFAULT;
    VectorValue::Value m_desired_speed = DESIRED_SPEED_DEFAULT;
    TimeTy m_wait_phase = WAIT_PHASE_DEFAULT;
};

} // namespace Charlcar

#endif // PARAMETERS_H
