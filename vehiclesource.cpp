#include "vehiclesource.h"

#include "concepts/route.h"
#include "simulationexecutor.h"

namespace Charlcar {

VehicleSource::VehicleSource(IDTy id, Position pos, int interval, QObject *parent) :
    QObject(parent), m_id(id), m_pos(pos), m_interval(interval)
{
    m_timer = new QTimer(this);
    connect(m_timer, SIGNAL(timeout()), this, SLOT(createVehicle()));
    m_timer->start(m_interval);
}

void VehicleSource::createVehicle()
{
    // TODO
    Velocity vel(10, 0);
    SimulationExecutor::instance().data().addVehicle(
                m_pos, vel, Direction::EAST, 20, 5);
}

VehicleSource::~VehicleSource()
{

}

} // namespace Charlcar
