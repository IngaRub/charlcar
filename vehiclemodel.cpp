#include "vehiclemodel.h"

#include "actions/moveforwardaction.h"
#include "concepts/route.h"
#include "constants.h"
#include "desires/donotcrashdesire.h"
#include "desires/drivefastdesire.h"
#include "desires/followtheroutedesire.h"
#include "desires/obeytherulesdesire.h"
#include "kinematics.h"
#include "parameters.h"
#include "simulationexecutor.h"
#include "utils.h"

#include <functional>

#define LOG(text) _LOG(DBG_VS, text)
#define LOG_LINE _LOG_LINE(DBG_VS)
#define LOG_BEGIN _LOG_BEGIN(DBG_VS)
#define LOG_END _LOG_END(DBG_VS)

#define LOG_ID(text) do { \
    LOG("[" + QString::number(m_id) + "]"); \
    LOG(text); \
} while(0)

namespace Charlcar {

using SE = SimulationExecutor;

VehicleState::VehicleState()
{
    pos = Position();
    v = Velocity();
    a = Acceleration();
    ornt = Direction::INVALID;
}

VehicleState::VehicleState(Position vPos, Velocity vV, Acceleration vA, Direction vOrnt)
    : pos(vPos), v(vV), a(vA), ornt(vOrnt)
{

}

QString VehicleState::str() const
{
    return QString("POS: %1\nVEL: %2\nACC: %3\n").arg(pos.str()).arg(v.str()).arg(a.str());
}

VehicleModel::VehicleModel(IDTy id, SizeTy size_l, SizeTy size_w, QObject *parent)
    : QObject(parent), m_id(id), m_size_l(size_l), m_size_w(size_w)
{
    m_desired_speed = Parameters::instance().desiredSpeed();
    m_desires.push_back(DesirePtr(new DriveFastDesire(*this)));
    m_desires.push_back(DesirePtr(new FollowTheRouteDesire(*this)));
    m_desires.push_back(DesirePtr(new ObeyTheRulesDesire(*this)));
    m_desires.push_back(DesirePtr(new DoNotCrashDesire(*this)));
}

VehicleModel::~VehicleModel()
{

}

IDTy VehicleModel::id() const
{
    return m_id;
}

Position VehicleModel::pos() const
{
    return m_state.pos;
}

void VehicleModel::setPos(Position pos)
{
    m_state.pos = pos;
}

Velocity VehicleModel::vel() const
{
    return m_state.v;
}

void VehicleModel::setVel(Velocity velocity)
{
    m_state.v = velocity;
}

Acceleration VehicleModel::acc() const
{
    return m_state.a;
}

void VehicleModel::setAcc(Acceleration a)
{
    m_state.a = a;
}

VehicleModel::State VehicleModel::state() const
{
    return m_state;
}

void VehicleModel::setState(const State &state)
{
    m_state = state;
}

VehicleModel::SizeTy VehicleModel::sizeL() const
{
    return m_size_l;
}

VehicleModel::SizeTy VehicleModel::sizeW() const
{
    return m_size_w;
}

VectorValue::Value VehicleModel::desiredSpeed() const
{
    return m_desired_speed;
}

void VehicleModel::setDesiredSpeed(VectorValue::Value val)
{
    m_desired_speed = val;
}

void VehicleModel::makeStep()
{
    while (checkIfGoalCompleted());

    m_planned_action = nullptr;
    Action *action = new MoveForwardAction();
    action->setInterval(simInterval());

    for (auto &desire : m_desires) {
        action = desire->planAction(action);
        assert(action);
    }

    m_planned_action = action;
    action->execute(*this);
}

Direction VehicleModel::ornt() const {
  return m_state.ornt;
}

void VehicleModel::setOrnt(Direction ornt) {
  m_state.ornt = ornt;
}

void VehicleModel::assignRoute(const Route &route)
{
    FuzzyPosition last = route.getFirstPosition();
    VectorValue::Value turn_speed_min = TURN_SPEED_MIN;
    VectorValue::Value turn_speed_max = TURN_SPEED_MAX;
    Goal::FuzzyVecValue turn_speed(turn_speed_min, turn_speed_max);
    FuzzyPosition::DistTy x_marg = 0, y_marg = 0;
    FuzzyPosition::DistTy marg = (2 * sizeW() - 1) / 2;
    for (const auto &pos : route.get()) {
        Direction dir = getAlongDirection(last, pos);
        if (dir == Direction::INVALID) continue;
        if (isHorizontalDir(dir)) {
            x_marg = 0; y_marg = marg;
        } else {
            x_marg = marg; y_marg = 0;
        }
        FuzzyCoordTy x1(last.x().getMin() + x_marg, last.x().getMax() - x_marg);
        FuzzyCoordTy y1(last.y().getMin() + y_marg, last.y().getMax() - y_marg);
        Goal g1(*this, x1, y1);
        g1.setVelocityDirection(dir);
        m_goals.push_back(g1);
        FuzzyCoordTy x2(pos.x().getMin() + x_marg, pos.x().getMax() - x_marg);
        FuzzyCoordTy y2(pos.y().getMin() + y_marg, pos.y().getMax() - y_marg);
        Goal g2(*this, x2, y2);
        g2.setSpeed(turn_speed);
        g2.setVelocityDirection(dir);
        m_goals.push_back(g2);
        last = pos;
    }
}

void VehicleModel::accelerateSimulate(VehicleState &state, Acceleration acc) const
{
    state.a = acc;
}

void VehicleModel::moveForwardSimulate(VehicleState &vs, TimeTy interval) const
{
    double intervalSecs = SEC(interval);

    double a_xt = vs.a.x() * intervalSecs;
    double a_yt = vs.a.y() * intervalSecs;

    double v_xt = vs.v.x() * intervalSecs;
    double a_xt2 = a_xt * intervalSecs;

    double v_yt = vs.v.y() * intervalSecs;
    double a_yt2 = a_yt * intervalSecs;

    // dist_x = dx/dt * v + d^2x/dt^2 * t^2 / 2
    VectorValue::Value dist_x = (VectorValue::Value) (v_xt + a_xt2 / 2);
    VectorValue::Value dist_y = (VectorValue::Value) (v_yt + a_yt2 / 2);

    // update position
    vs.pos.setX(vs.pos.x() + dist_x);
    vs.pos.setY(vs.pos.y() + dist_y);

    // update velocity
    vs.v.setX(vs.v.x() + (VectorValue::Value) a_xt);
    vs.v.setY(vs.v.y() + (VectorValue::Value) a_yt);
}

void VehicleModel::turnSimulate(VehicleState &state, Direction direction) const
{
    Velocity::Value v_val = state.v.value();
    Acceleration::Value a_val = state.a.value();
    Velocity vel;
    Acceleration acc;
    state.v = vel; // (0, 0)
    state.v.setValueUp(v_val, direction);
    state.a = acc; // (0, 0)
    state.a.setValueUp(a_val, direction);
    state.ornt = direction;
}

void VehicleModel::waitSimulate(VehicleModel::State &vs) const
{
    vs.v.subFrom(vs.v);
    vs.a.subFrom(vs.a);
}


void VehicleModel::accelerateExecute(Acceleration acc)
{
    State s = state();
    accelerateSimulate(s, acc);
    setState(s);
}

void VehicleModel::moveForwardExecute(TimeTy interval)
{
    State s = state();
    moveForwardSimulate(s, interval);
    setState(s);
}

void VehicleModel::turnExecute(Direction direction)
{
    State s = state();
    turnSimulate(s, direction);
    setState(s);
}

void VehicleModel::waitExecute()
{
    State s = state();
    waitSimulate(s);
    setState(s);
}

std::vector<Action *> VehicleModel::plan(VehicleState &vs, GoalCItTy &gIt,
                                         TimeTy interval) const
{
    while (checkIfGoalCompleted(vs, gIt)) { ++gIt; }
    std::vector<Action *> result;
    while (interval > 0) {
        TimeTy time = interval > simInterval() ? simInterval() : interval;
        interval -= time;
        Action *action = new MoveForwardAction();
        action->setInterval(simInterval());

        for (auto &desire : m_desires) {
            if (desire->isPredicting()) { break; }
            action = desire->planAction(vs, gIt, time, action);
            assert(action);
        }

        action->simulate(*this, vs, gIt);
        result.push_back(action);
    }

    return result;
}

void VehicleModel::printDebug()
{
    LOG_ID("STATE: " << m_state.str());
    for (const auto &goal : m_goals) {
        LOG_ID(goal);
    }
}

unsigned VehicleModel::accMax() const
{
    return m_acc_max;
}

void VehicleModel::setAccMax(const unsigned &acc_max)
{
    m_acc_max = acc_max;
}

const std::deque<Goal>& VehicleModel::goals() const
{
    return m_goals;
}

void VehicleModel::setGoals(const std::deque<Goal> &goals)
{
    for (const auto &g : goals) {
        m_goals.push_back(g);
    }
}

const Action *VehicleModel::getPlannedAction() const
{
    return m_planned_action;
}

FuzzyPosition VehicleModel::getNeighbourArea(
        const VehicleState &vs, TimeTy interval) const
{
    Position max_pos = Kinematics::computePosition(vs.pos, vs.v,
                Acceleration(accMax(), vs.ornt), interval);
    Velocity max_vel(desiredSpeed(), vs.ornt);
    Velocity zero_vel(0, Direction::INVALID);
    max_pos = Kinematics::computePosition(max_pos, max_vel, zero_vel,
                Acceleration(accMax(), getOppositeDirection(vs.ornt)));

    if (isHorizontalDir(vs.ornt)) {
        auto laneID = SE::instance().data().grid()->whichLaneHorizontal(vs.pos, vs.ornt);
        assert(laneID);
        const auto &lane = SE::instance().data().grid()->getLane(laneID.get());
        FuzzyCoordTy cx(vs.pos.x(), max_pos.x());
        FuzzyCoordTy cy = lane.area().y();
        return FuzzyPosition(cx, cy);
    }

    assert(isVerticalDir(vs.ornt));
    auto laneID = SE::instance().data().grid()->whichLaneVertical(vs.pos, vs.ornt);
    assert(laneID);
    const auto &lane = SE::instance().data().grid()->getLane(laneID.get());

    FuzzyCoordTy cx = lane.area().x();
    FuzzyCoordTy cy(vs.pos.y(), max_pos.y());
    return FuzzyPosition(cx, cy);
}

Maybe<Goal> VehicleModel::getGoal() const
{
    if (m_goals.empty()) {
        return Maybe<Goal>();
    }
    return Maybe<Goal>(m_goals.front());
}

VehicleModel::GoalCItTy VehicleModel::getGoalBegin() const {
    return m_goals.cbegin();
}

VehicleModel::GoalCItTy VehicleModel::getGoalEnd() const {
    return m_goals.cend();
}

void VehicleModel::completeGoal()
{
    assert(!m_goals.empty());
    m_goals.pop_front();
}

bool VehicleModel::checkIfGoalCompleted()
{
    if (!getGoal()) { return false; }

    Goal g = getGoal().get();
    if (g.isAchieved()) {
        LOG_ID("ACHIEVED THE GOAL: " << g << "\n");
        completeGoal();
        return true;
    }

    if (!g.approvesWay()) {
        LOG_ID("FAILED AT ACHIEVING THE GOAL" << g << "\n");
        completeGoal();
        return true;
    }
    return false;
}

bool VehicleModel::checkIfGoalCompleted(const VehicleState &vs, GoalCItTy gIt) const
{
    if ( gIt == getGoalEnd()) { return false; }

    if (gIt->isAchieved(vs)) {
        return true;
    }

    if (!gIt->approvesWay(vs)) {
        return true;
    }
    return false;
}

} // namespace Charlcar
