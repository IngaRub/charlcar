#include "wrapperaction.h"

#include "vehiclemodel.h"

namespace Charlcar {

WrapperAction::WrapperAction(ExeFnTy action_fn, SimFnTy simulate_fn)
    : m_action_fn(action_fn), m_simulate_fn(simulate_fn)
{

}

void WrapperAction::executeFor(VehicleModel &vehicle, TimeTy) const
{
    m_action_fn(vehicle);
}

void WrapperAction::simulateFor(const VehicleModel &vehicle, VehicleState &vs, TimeTy) const
{
    m_simulate_fn(vehicle, vs);
}

WrapperAction::~WrapperAction()
{

}

} // namespace Charlcar
