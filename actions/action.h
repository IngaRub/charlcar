#ifndef ACTION_H
#define ACTION_H

#include "concepts/condition.h"
#include "concepts/position.h"
#include "concepts/velocity.h"
#include "desires/goal.h"
#include "utils.h"

#include <deque>
#include <functional>
#include <memory>
#include <set>
#include <utility>
#include <vector>

namespace Charlcar {

class VehicleState;
class VehicleModel;

/* Represents a single action
 * taken by a driver in accordance with the plan. */
class Action
{

friend class VehicleModel;

public:
using ActionPtrRef =
    std::reference_wrapper<std::unique_ptr<Action>>;
using ActionPtrCRef =
    std::reference_wrapper<const std::unique_ptr<Action>>;
using GoalCItTy = std::deque<Goal>::const_iterator;

public:
    Action();
    virtual ~Action();

    IDTy id() const;
    TimeTy interval() const;
    virtual std::vector<TimeTy> getIntervals() const;
    virtual void setInterval(TimeTy interval);

    virtual void executeFor(VehicleModel &vehicle, TimeTy time) const = 0;
    void execute(VehicleModel &vehicle) const;

    virtual void simulateFor(
            const VehicleModel &vehicle, VehicleState &vs, TimeTy time) const = 0;
    virtual void simulateFor(
            const VehicleModel &vehicle, VehicleState &vs, GoalCItTy &gIt, TimeTy time) const;

    void simulate(const VehicleModel &vehicle, VehicleState &vs, GoalCItTy &gIt) const;
    void simulate(const VehicleModel &vehicle, VehicleState &vs) const;
    VehicleState simulate(const VehicleModel &vehicle) const;


    void addConditionOnPosX(Position::CoordTy targetX, bool gte = true);
    void addConditionOnPosY(Position::CoordTy targetY, bool gte = true);
    void addConditionOnTime(TimeTy interval);

    bool verifyConditions(const VehicleModel &vehicle) const;

protected:
    IDTy m_id;
    TimeTy m_interval;
    std::vector<Condition> m_conditions = {};
};

} // namespace Charlcar

#endif // ACTION_H
