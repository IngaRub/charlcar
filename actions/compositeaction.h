#ifndef COMPOSITEACTION_H
#define COMPOSITEACTION_H

#include "action.h"

#include <vector>

namespace Charlcar {

class CompositeAction : public Action
{
public:
    CompositeAction();
    ~CompositeAction();
    void addAction(Action *action);
    std::vector<TimeTy> getIntervals() const override;
    void setInterval(TimeTy interval) override;
    void executeFor(VehicleModel &vehicle, TimeTy time) const override;
    void simulateFor(const VehicleModel &vehicle, VehicleState &vs, TimeTy time) const override;
    void simulateFor(const VehicleModel &vehicle, VehicleState &vs, GoalCItTy &gIt, TimeTy time) const override;
    bool empty() const;
    unsigned size() const;

private:
    std::vector<Action *> m_actions;
};

} // namespace Charlcar

#endif // COMPOSITEACTION_H
