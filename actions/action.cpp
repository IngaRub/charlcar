#include "action.h"
#include "simulationexecutor.h"
#include "vehiclemodel.h"

#include <QTime>

namespace Charlcar {

Action::Action()
{
    m_id = SimulationExecutor::instance().generateActionID();
    m_interval = 0;
}

Action::~Action()
{
}

IDTy Action::id() const
{
    return m_id;
}

TimeTy Action::interval() const
{
    return m_interval;
}

std::vector<TimeTy> Action::getIntervals() const
{
    std::vector<TimeTy> result;
    result.push_back(m_interval);
    return result;
}

void Action::setInterval(TimeTy interval)
{
    m_interval = interval;
}

void Action::execute(VehicleModel &vehicle) const
{
    executeFor(vehicle, interval());
}

bool Action::verifyConditions(const VehicleModel &vehicle) const
{
    for (auto &cond : m_conditions) {
        if (!cond.check(vehicle)) return false;
    }
    return true;
}

void Action::simulateFor(const VehicleModel &vehicle, VehicleState &vs,
                      GoalCItTy &gIt, TimeTy time) const
{
    simulateFor(vehicle, vs, time);
    if (gIt == vehicle.getGoalEnd()) {
        return;
    }
    while (gIt->isAchieved(vs)) {
        ++gIt;
    }
}

void Action::simulate(const VehicleModel &vehicle, VehicleState &vs) const
{
    simulateFor(vehicle, vs, interval());
}

void Action::simulate(const VehicleModel &vehicle, VehicleState &vs,
                      Action::GoalCItTy &gIt) const
{
    simulateFor(vehicle, vs, gIt, interval());
}

VehicleState Action::simulate(const VehicleModel &vm) const {
    VehicleState state = vm.state();
    simulate(vm, state);
    return state;
}

void Action::addConditionOnPosX(Position::CoordTy targetX, bool gte)
{
    if (gte) {
        m_conditions.emplace_back([targetX](const VehicleModel &vehicle) {
            return vehicle.pos().x() >= targetX;
        });
    } else {
        m_conditions.emplace_back([targetX](const VehicleModel &vehicle) {
            return vehicle.pos().x() <= targetX;
        });
    }
}

void Action::addConditionOnPosY(Position::CoordTy targetY, bool gte)
{
    if (gte) {
        m_conditions.emplace_back([targetY](const VehicleModel &vehicle) {
            return vehicle.pos().y() >= targetY;
        });
    } else {
        m_conditions.emplace_back([targetY](const VehicleModel &vehicle) {
            return vehicle.pos().y() <= targetY;
        });
    }
}

void Action::addConditionOnTime(TimeTy interval)
{
    QTime start = QTime::currentTime();
    m_conditions.emplace_back([interval, start](const VehicleModel &) {
        return start.msecsTo(QTime::currentTime()) > interval;
    });
}

} // namespace Charlcar
