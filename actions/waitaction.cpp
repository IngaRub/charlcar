#include "waitaction.h"
#include "simulationexecutor.h"

namespace Charlcar {

WaitAction::WaitAction()
{

}

WaitAction::~WaitAction()
{

}

void WaitAction::executeFor(VehicleModel &vehicle, TimeTy) const
{
    vehicle.waitExecute();
}

void WaitAction::simulateFor(const VehicleModel &vehicle, VehicleState &vs, TimeTy) const
{
    vehicle.waitSimulate(vs);
}

} // namespace Charlcar
