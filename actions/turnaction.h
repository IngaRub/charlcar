#ifndef TURNACTION_H
#define TURNACTION_H

#include "action.h"
#include "utils.h"

namespace Charlcar {

class TurnAction : public Action
{
public:
    TurnAction(Direction directionV);
    ~TurnAction();

    void setInterval(TimeTy t) override;

    void executeFor(VehicleModel &vehicle, TimeTy time) const override;
    void simulateFor(const VehicleModel &vehicle, VehicleState &vs, TimeTy time) const override;

private:
    Direction m_direction;
};

} // namespace Charlcar

#endif // TURNACTION_H
