#include "compositeaction.h"

#include "vehiclemodel.h"

namespace Charlcar {

CompositeAction::CompositeAction()
{

}

CompositeAction::~CompositeAction()
{
    for (auto action : m_actions) {
        free(action);
    }
}

void CompositeAction::addAction(Action *action)
{
    m_actions.push_back(action);
    m_interval += action->interval();
}

std::vector<TimeTy> CompositeAction::getIntervals() const
{
    std::vector<TimeTy> result;
    TimeTy time = 0;
    for (auto action : m_actions) {
        time += action->interval();
        result.push_back(action->interval());
    }
    return result;
}

void CompositeAction::setInterval(TimeTy time)
{
    assert(time <= interval()); // TODO
    m_interval = time;
}

void CompositeAction::executeFor(VehicleModel &vehicle, TimeTy time) const
{
    assert(time <= interval());

    TimeTy sum = 0;
    auto IT = m_actions.begin();

    while (IT != m_actions.end()) {
        TimeTy delta_t = (*IT)->interval();

        sum += delta_t;

        if (sum > time) {
            delta_t -= sum - time;
        }

        (*IT)->executeFor(vehicle, delta_t);
        while (vehicle.checkIfGoalCompleted());

        if (sum > time) {
            break;
        }
        ++IT;
    }
}

void CompositeAction::simulateFor(const VehicleModel &vehicle,
                               VehicleState &vs, TimeTy time) const
{
    assert(time <= interval());

    TimeTy sum = 0;
    auto IT = m_actions.begin();

    while (IT != m_actions.end()) {
        TimeTy delta_t = (*IT)->interval();

        sum += delta_t;
        if (sum > time) {
            delta_t -= sum - time;
        }

        (*IT)->simulateFor(vehicle, vs, delta_t);

        if (sum > time) {
            break;
        }
        ++IT;
    }
}

void CompositeAction::simulateFor(const VehicleModel &vehicle, VehicleState &vs,
                               Action::GoalCItTy &gIt, TimeTy time) const
{
    assert(time <= interval());

    TimeTy sum = 0;
    auto IT = m_actions.begin();

    while (IT != m_actions.end()) {
        TimeTy delta_t = (*IT)->interval();

        sum += delta_t;
        if (sum > time) {
            delta_t -= sum - time;
        }

        (*IT)->simulateFor(vehicle, vs, gIt, delta_t);

        if (sum > time) {
            break;
        }
        ++IT;
    }
}

bool CompositeAction::empty() const
{
    return !m_actions.size();
}

unsigned CompositeAction::size() const
{
    return m_actions.size();
}

} // namespace Charlcar
