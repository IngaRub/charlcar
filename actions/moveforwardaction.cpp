#include "moveforwardaction.h"
#include "simulationexecutor.h"

namespace Charlcar {

MoveForwardAction::MoveForwardAction()
{

}

MoveForwardAction::~MoveForwardAction()
{

}

void MoveForwardAction::executeFor(VehicleModel &vehicle, TimeTy time) const
{
    vehicle.moveForwardExecute(time);
}

void MoveForwardAction::simulateFor(const VehicleModel &vehicle, VehicleState &vs,
                                    TimeTy time) const
{
    vehicle.moveForwardSimulate(vs, time);
}

} // namespace Charlcar
