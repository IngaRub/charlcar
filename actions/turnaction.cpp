#include "turnaction.h"

#include "constants.h"
#include "vehiclemodel.h"

namespace Charlcar {

TurnAction::TurnAction(Direction directionV)
    : m_direction(directionV)
{
    setInterval(0);
}

void TurnAction::executeFor(VehicleModel &vehicle, TimeTy time) const
{
    assert(!time);
    vehicle.turnExecute(m_direction);
}

void TurnAction::simulateFor(const VehicleModel &vehicle,
                          VehicleState &vs, TimeTy time) const
{
    assert(!time);
    vehicle.turnSimulate(vs, m_direction);
}

TurnAction::~TurnAction()
{

}

void TurnAction::setInterval(TimeTy t)
{
    assert(!t);
}

} // namespace Charlcar
