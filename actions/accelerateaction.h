#ifndef ACCELERATEACTION_H
#define ACCELERATEACTION_H

#include "concepts/acceleration.h"
#include "action.h"

namespace Charlcar {

class AccelerateAction : public Action
{
public:
    AccelerateAction(const Acceleration &accV);

    ~AccelerateAction();
    void executeFor(VehicleModel &vehicle, TimeTy time) const override;
    void simulateFor(const VehicleModel &vehicle, VehicleState &vs, TimeTy time) const override;

private:
    Acceleration m_acc;
};

} // namespace Charlcar

#endif // ACCELERATEACTION_H
