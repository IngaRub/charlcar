#ifndef WRAPPERACTION_H
#define WRAPPERACTION_H

#include "action.h"

#include <functional>

namespace Charlcar {

class WrapperAction : public Action
{
public:
    using ExeFnTy = std::function<bool(VehicleModel &)>;
    using SimFnTy = std::function<VehicleState(const VehicleModel &, VehicleState &)>;

public:
    WrapperAction(ExeFnTy action_fn, SimFnTy simulate_fn);
    ~WrapperAction();

    void executeFor(VehicleModel &vehicle, TimeTy time) const override;
    void simulateFor(const VehicleModel &vehicle, VehicleState &vs, TimeTy time) const override;

private:
    ExeFnTy m_action_fn;
    SimFnTy m_simulate_fn;
};

} // namespace Charlcar


#endif // WRAPPERACTION_H
