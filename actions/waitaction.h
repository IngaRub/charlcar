#ifndef WAITACTION_H
#define WAITACTION_H

#include "action.h"

namespace Charlcar {

class WaitAction : public Action
{
public:
    WaitAction();

    ~WaitAction();
    void executeFor(VehicleModel &vehicle, TimeTy time) const override;
    void simulateFor(const VehicleModel &vehicle, VehicleState &vs, TimeTy time) const override;

};

} // namespace Charlcar

#endif // WAITACTION_H
