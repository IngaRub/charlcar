#include "accelerateaction.h"
#include "simulationexecutor.h"

namespace Charlcar {

AccelerateAction::AccelerateAction(const Acceleration &accV)
    : m_acc(accV)
{

}

AccelerateAction::~AccelerateAction()
{

}

void AccelerateAction::executeFor(VehicleModel &vehicle, TimeTy time) const
{
    assert(!time);
    vehicle.accelerateExecute(m_acc);
}

void AccelerateAction::simulateFor(const VehicleModel &vehicle, VehicleState &vs, TimeTy time) const
{
    assert(!time);
    vehicle.accelerateSimulate(vs, m_acc);
}

} // namespace Charlcar
