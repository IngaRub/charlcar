#ifndef MOVEFORWARDACTION_H
#define MOVEFORWARDACTION_H

#include "action.h"

namespace Charlcar {

class MoveForwardAction : public Action
{
public:
    MoveForwardAction();

    ~MoveForwardAction();
    void executeFor(VehicleModel &vehicle, TimeTy time) const override;
    void simulateFor(const VehicleModel &vehicle, VehicleState &vs, TimeTy time) const override;

};

} // namespace Charlcar

#endif // MOVEFORWARDACTION_H
