#ifndef TRAFFICLIGHTS_H
#define TRAFFICLIGHTS_H

#include "concepts/position.h"
#include "concepts/range.h"
#include "constants.h"
#include "gridmodel.h"
#include "utils.h"

#include <memory>
#include <map>
#include <set>
#include <vector>

#include <QColor>
#include <QObject>

namespace Charlcar {

class SimulationData;

class Phase {
public:
    using AllowedTy = std::pair<IDTy, IDTy>;

public:
    Phase();
    Phase(TimeTy interval);

    TimeTy interval() const;
    void allow(IDTy lane_from, IDTy lane_to);
    bool isAllowed(IDTy lane_from, IDTy lane_to) const;
    range<std::set<AllowedTy>::const_iterator> allowed() const;
    unsigned howManyAllowed() const;

private:
    std::set<AllowedTy> m_allowed;
    TimeTy m_interval;
};

class TrafficLights : public QObject
{
    Q_OBJECT
    using FuzzyCoordTy = FuzzyPosition::FuzzyCoordTy;

public:
    TrafficLights(IDTy id, FuzzyPosition pos, QObject *parent = 0);
    ~TrafficLights();
    void addPhase(Phase p);
    void addLane(SimulationData &data, IDTy lane);
    void setPrepareTime(TimeTy prepare_time);
    void setWaitTime(TimeTy wait_time);
    void makeStep(TimeTy interval);
    bool getWhenAllowed(IDTy from, IDTy to, TimeTy &start, TimeTy &duration) const;

    IDTy id() const;
    FuzzyPosition pos() const;
    const Phase & currentPhase() const;
    FuzzyPosition getPosFrom(IDTy lane) const;
    FuzzyPosition getPosTo(IDTy lane) const;

private:
    IDTy m_id;
    FuzzyPosition m_pos;

    std::vector<Phase> m_phases;
    std::vector<Phase>::iterator m_current_phase;

    std::map<IDTy, FuzzyPosition> m_positions_from;
    std::map<IDTy, FuzzyPosition> m_positions_to;
    TimeTy m_prepare_time = 2000; // yellow light - milliseconds
    TimeTy m_wait_time; // no traffic allowed inbetween phases - milliseconds
    TimeTy m_timer = 0; // how many milliseconds are left for the current phase
};

}

#endif // TRAFFICLIGHTS_H
